if (window.location.pathname !== '/login.html') {
	if (typeof $.cookie("clientId") === 'undefined' || typeof $.cookie("accessToken") === 'undefined')
		window.location.href="/login.html";
}

$(document).ready(function() {

	$("#fiscalKey").keydown(function (e) {

		var code = e.keyCode || e.which;

		//Blocking letters
		if (code != 13 && code != 9 && code != 8 && code != 0 && code != 116 && (code < 48 || code > 57)) {
			return false;
		}
	});
	
	$('#fiscalKey').focus();

	// labelsUrl = 'http://localhost:3001' // Local
	// labelsUrl = 'https://etiqueta-api.modalgrhomolog.be' // Dev
	// labelsUrl = 'https://etiqueta-api.alliedhomolog.com.br' // Hom
	labelsUrl = 'https://etiqueta-api.alliedapp.com.br' // Prod
	
	label = function() {

		var fiscalKey = $("#fiscalKey").val();

		if (event.keyCode === 13 || event.keyCode === 9) {
			
			event.preventDefault();
			
			if(fiscalKey.length != 44) {
				$('#target').val('\n');
				$('#target').css("color", "red");
				$('#target').val( ($('#target').val() + ('\n' + 'chave fiscal inválida')) );
				$('#fiscalKey').focus();
				return false;
			}

			let clientId = $.cookie("clientId");
			let accessToken = $.cookie("accessToken");
			
			$('#target').css("color", "black");   
			$('#target').val('Carregando...');
			   
			$.ajax({
	        	type: 'post',
				url: labelsUrl+'/label',
				headers: {
					'client-id': clientId,
					'x-access-token': accessToken
				},
	            data: JSON.stringify({
					fiscalkey: fiscalKey
				}),
				dataType: 'json',
				crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            success: function (data) {

					var content = data.content;

					$('#fiscalKey').val('');
					$('#target').css("color", "green");
					$('#target').val('\n');

					console.log('data.printed: ' + data.printed);

					if (data.printed == 1 || data.printed == true) {
						if(!confirm("Etiqueta já foi impressa. Deseja reimprimir?")) {
							$('#fiscalKey').focus();
							return false;
						}
					}

					$('#target').val( ($('#target').val() + ('\n' + 'Imprimindo...')) );
					data.type == 'zebra' ? writeToSelectedPrinter(content) : printJS({printable: content, type: 'pdf', base64: true});

					setTimeout(function(){
						$('#target').val('');
					},2000);

					$('#fiscalKey').focus();
	            },
	            error: function (data) {

					$('#fiscalKey').val('');
					$('#target').css("color", "red");
					$('#target').val('\n');

					if (typeof data.responseJSON !== 'undefined')
						$('#target').val( ($('#target').val() + ('\n' + data.responseJSON.message)) );
					else
						$('#target').val( ($('#target').val() + ('\n' + data.statusText)) );
	            }
	        });
		}
	}

	status = function() {

		var fiscalKey = $("#fiscalKey").val();

		if (event.keyCode === 13 || event.keyCode === 9) {
		
			event.preventDefault();
			
			$('#target').css("color", "black");   
			$('#target').val('Carregando...');
			
			$.ajax({
	        	type: 'post',
				url: labelsUrl+'/status',
				headers: {
					'client-id': $.cookie("clientId"),
					'x-access-token': $.cookie("accessToken")
				},
	            data: JSON.stringify({
					fiscalkey: fiscalKey
				}),
				dataType: 'json',
				crossDomain: true,
	            contentType: 'application/json; charset=utf-8',
	            success: function (data) {

					var content = data.content;

					$('#fiscalKey').val('');
					$('#target').css("color", "green");
					$('#target').val('\n');
					$('#target').val( ($('#target').val() + ('\n' + data.orderStatus)) );

					console.log('data.printed: ' + data.printed);
					
					$('#fiscalKey').focus();
	            },
	            error: function (data) {

					$('#fiscalKey').val('');
					$('#target').css("color", "red");
					$('#target').val('\n');

					if (typeof data.responseJSON !== 'undefined')
						$('#target').val( ($('#target').val() + ('\n' + data.responseJSON.message)) );
					else
						$('#target').val( ($('#target').val() + ('\n' + data.statusText)) );
	            }
	        });
		}
	}

	login = function() {

		event.preventDefault();
		var user = $("#user").val();
		var password = $("#password").val();

		if (!user) {
			alert('Insira o usuário');
			return false;
		}

		if (!password) {
			alert('Insira a senha');
			return false;
		}

		$('#target').html('logging in...');
		
		$.ajax({
			type: 'post',
			url: labelsUrl+'/ad-auth',
			data: JSON.stringify({user: user, password: password}),
			dataType: 'json',
			crossDomain: true,
			contentType: 'application/json; charset=utf-8',
			success: function (data) {
				
				//Setting cookies from response
				var expirationtimeindays = data.expires_in / 86400;
				$.cookie('clientId', data.clientId, {
					expires: expirationtimeindays
				});
				$.cookie('accessToken', data.accessToken, {
					expires: expirationtimeindays
				});
				
				window.location.href="/gerar.html";
			},
			error: function (data) {

				$('#target').html('');
				alert(data.responseJSON.message);
			}
		});
	}

	logout = function() {
		
		var cookies = $.cookie();
		for(var cookie in cookies) {
			$.removeCookie(cookie);
		}
	}
});