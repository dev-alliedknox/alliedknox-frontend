export class StatusDispositivos {
    id: string = '';
    sku: string = '';
    nomeProduto: string = '';
    loja: string = '';
    notaFiscal: string = '';
    codigoImei: string = '';
    statusKDP: string = '';
    dtHoraKDP: string = '';
    statusKG: string = '';
    dtHoraKG: string = '';
    timeSpanKDP: Date = new Date();
    timeSpanKG: Date = new Date();

    constructor() {}

}

export const filterDateTimeKDPStatusDispositivos = (listaStatusDispositivos: Array<StatusDispositivos>): Array<StatusDispositivos> => {
    listaStatusDispositivos.reduce((previous, current, index, array) => {
      if(current.dtHoraKDP !== null && current.dtHoraKDP !== undefined && current.dtHoraKDP !== '') {
      const res = current.dtHoraKDP.split("T");
      const [year, month, day] = res[0].split("-");
      const date = day + "/" + month + "/" + year;
      const [hour, minute, second] = res[1].split(":");
      const time = hour + ":" + minute + ":" + second.split(".")[0];
      const strminute = second.split(".")[0];
      current.dtHoraKDP = date + " " + time;
      current.timeSpanKDP = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      }
      return array;
    }, {});
    return listaStatusDispositivos;
  }

  export const filterDateTimeKGStatusDispositivos = (listaStatusDispositivos: Array<StatusDispositivos>): Array<StatusDispositivos> => {
    listaStatusDispositivos.reduce((previous, current, index, array) => {
      if(current.dtHoraKG !== null && current.dtHoraKG !== undefined && current.dtHoraKG !== '') {
      const res = current.dtHoraKG.split("T");
      const [year, month, day] = res[0].split("-");
      const date = day + "/" + month + "/" + year;
      const [hour, minute, second] = res[1].split(":");
      const time = hour + ":" + minute + ":" + second.split(".")[0];
      const strminute = second.split(".")[0];
      current.dtHoraKG = date + " " + time;
      current.timeSpanKG = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      }
      return array;
    }, {});
    return listaStatusDispositivos;
  }