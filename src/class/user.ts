export class User {
    name: string;
    email: string;
    isActive: boolean;
    isActiveLabel: string;
    password: string;

    constructor() {}
}
