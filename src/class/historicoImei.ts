export class historicoImei {

    serial: string = '';
    cpf: string = '';
    dthora: string = '';
    acao: string = '';
    mensagem: string = '';
    usuario: string = '';
    timeSpan: Date = new Date();

    constructor() {}
}

export const filterDateTimeHistoricoImei = (listaHistoricoImei: Array<historicoImei>): Array<historicoImei> => {
    listaHistoricoImei.reduce((previous, current, index, array) => {
      if(current.dthora !== null && current.dthora !== undefined && current.dthora !== '') {
      const res = current.dthora.split(" ");
      const [day, month, year] = res[0].split("/");
      const date = day + "/" + month + "/" + year;
      const [hour, minute] = res[1].split(":");
      const time = hour + ":" + minute;
      current.dthora = date + " " + time;
      current.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), 0);
      }
      return array;
    }, {});

    listaHistoricoImei = listaHistoricoImei.sort((a, b) => {
        if(a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }

        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }

        return 0;
      });

    return listaHistoricoImei;
  }