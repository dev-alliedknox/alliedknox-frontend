export class UsageInfo {
    activationCount: number = 0;
    endDate: string = '';
    excessAcceptRate: number = 0;
    numberOfCurrent: number = 0;
    numberOfLicenses: number = 0;
    serviceToBeProvisioned: string = '';
    startDate: number = 0;

    constructor() {}
}
