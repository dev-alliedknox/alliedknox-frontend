import { PackageInfo } from "./package-info";
import { PartnerInfo } from "./partner-info";
import { UsageInfo } from "./usage-info";

export class Usage {
        generateType: string = '';
        licenseKey: "KLM09-MKVIF-HOU07-THEZJ-RITHU-90VIU";
        packageInfo: Array<PackageInfo> = new Array<PackageInfo>();
        partnerInfo: PartnerInfo = new PartnerInfo();
        resultCode: number = 0;
        resultMessage: string = '';
        slmCustomerId: string = '';
        status: string = '';
        usageInfo: Array<UsageInfo> = new Array<UsageInfo>();
        vendorId: string = '';

        constructor() {}
}
