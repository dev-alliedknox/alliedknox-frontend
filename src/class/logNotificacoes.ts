export class logNotificacoes {
    codigoImei: string = '';
    tipo: string = '';
    nomeCliente: string = '';
    dtHr: string = '';
    timeSpan: Date = new Date();
    mensagem: string = '';
    status: string = '';
    response: string = '';
    statusExecucao: string = '';

    constructor() {}
}

export const filterDateTimeLogNotificacoes = (listalogNotificacoes: Array<logNotificacoes>): Array<logNotificacoes> => {
    listalogNotificacoes.reduce((previous, current, index, array) => {
      if(current.dtHr !== null && current.dtHr !== undefined && current.dtHr !== '') {
      const res = current.dtHr.split("T");
      const [year, month, day] = res[0].split("-");
      const date = day + "/" + month + "/" + year;
      const [hour, minute, second] = res[1].split(":");
      const time = hour + ":" + minute + ":" + second.split(".")[0];
      const strminute = second.split(".")[0];
      current.dtHr = date + " " + time;
      current.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      }
      return array;
    }, {});
    return listalogNotificacoes;
  }