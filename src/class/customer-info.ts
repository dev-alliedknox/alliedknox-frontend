export class CustomerInfo {

    industryCode: string = '';
    scaleCode: string = '';
    zipCode: string = '';
    state: string = '';
    city: string = '';
    street: string = '';

    constructor() {}
    
}
