export class ApproveListUpdateInfo {

    approveComment: string = '';
    approveId: string = '';
    arrearsDays: 0;
    deviceUid: string = '';
    paidCount: 0;
    totalCount: 0;
    
    constructor() {}
}
