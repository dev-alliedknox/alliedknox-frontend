export class KdpDeleteDevices {

    customerId:string = '';
    resellerId:string = '';
    transactionId:string = '';
    devices: Array<string> = new Array<string>();
    // removeTransactionId:string = '';
    type:string = '';
    vendorId:string = '';

    constructor() {}
}
