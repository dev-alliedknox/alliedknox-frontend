import { ApproveListUpdateInfo } from "./approve-list-update-info";

export class UpdateInfo {

    approveList: Array<ApproveListUpdateInfo> = new Array<ApproveListUpdateInfo>();
    
    constructor() {}
}
