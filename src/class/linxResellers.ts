export class LinxResellers {
    id: number = 0;
    idempresa: number;
    nome: string = '';
    email: string = '';
    razao_social: string = '';
    cnpj: string = '';
    nome_fantasia: string = '';
    telefone: string = '';
    ativo: string;

    constructor() {}
}