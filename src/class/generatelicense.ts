import { ProductInfo } from "./product-info";
import { RecipientInfo } from "./recipient-info";
import { BbcAplications } from "./bbc-aplications";
import { PackageInfo } from "./package-info";
import { CustomerInfo } from "./customer-info";

export class GenerateLicense {
    
    customerId: string = '';
    customerName: string = '';
    contactemail: string = '';
    contactNumber: string = '';
    mdmId: string = '';
    startDate: string = '';
    endDate: string = '';
    poNumber: string = '';
    dcRate: number = 0 ;
    productInfo: Array<ProductInfo> = new Array<ProductInfo>();
    countryInfo: string = '';
    orderDate: string = '';
    recipientInfo: Array<RecipientInfo> = new Array<RecipientInfo>();
    bbcAplications: Array<BbcAplications> = new Array<BbcAplications>();
    packageInfo: Array<PackageInfo> = new Array<PackageInfo>();
    customerInfo: Array<CustomerInfo>= new Array<CustomerInfo>();

    constructor() {}

}
