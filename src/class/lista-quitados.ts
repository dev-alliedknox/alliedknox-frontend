export class listaQuitados {

    dthora: string = '';
    nome: string = '';
    cpf: string = '';
    imei: string = '';
    timeSpan: Date = new Date();
    
    constructor() {}
}

export const filterDateQuitados = (listaQuitados: Array<listaQuitados>): Array<listaQuitados> => {
    listaQuitados.reduce((previous, current, index, array) => {
      if(current.dthora !== null && current.dthora !== undefined && current.dthora !== '') {
        const res = current.dthora.split("T");
        const [year, month, day] = res[0].split("-");
        const date = day + "/" + month + "/" + year;
        const [hour, minute, second] = res[1].split(":");
        const time = hour + ":" + minute + ":" + second.split(".")[0];
        const strminute = second.split(".")[0];
        current.dthora = date + " " + time;
        current.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      }
      return array;
    }, {});
    return listaQuitados;
  }