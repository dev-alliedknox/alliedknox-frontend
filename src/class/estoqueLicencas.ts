export class EstoqueLicensas {
    id: string = '';
    qtdSeguranca: number = 0;
    idempresa: number;
    incrementoCompra: number = 0;
    descricaoLicenca: string = '';
    licRegistradas: number = 0;
    qtdEstoque: number = 0;
    licAtivadas: number = 0;
    qtdDisponivel: number = 0;

    constructor() {}
}