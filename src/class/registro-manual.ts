export class RegistroManual {

    cnpj: string = '';
    razaoSocial: string = '';
    nomeLoja: string = '';
    numeroLoja: string = '';
    nota: string = '';
    chaveNfe: string = '';
    modeloDocumento: string = '';
    numeroVenda: string = '';
    linhaNota: string = '';
    cpfCliente: string = '';
    dataEmissao: string = '';
    valor: string = '';
    formaPagamento: string = '';
    parcelas: string = '';
    tipoDispositivo: string = '';
    imeiSn: string = '';
    transacao: number = 0;
    nomeCliente: string = '';
    idempresa: string = '';

    constructor() { }

}