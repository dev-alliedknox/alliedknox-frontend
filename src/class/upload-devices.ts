export class UploadDevices {

    customerId: string = '';
    resellerId: string = '';
    transactionId: string = '';
    devices: Array<string> = new Array<string>();
    type: string = '';
    orderNo: string = '';
    orderTime: 0;
    vendorId: string = '';

    constructor() {}
}
