export class PackageInfo {
    packageName: string = '';
    publicKeyHash: string = '';
    companyName: string = '';
    applicationName: string = '';
    packageType: string = '';

    constructor() {}
}
