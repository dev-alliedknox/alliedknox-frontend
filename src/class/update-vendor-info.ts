import { ProductInfo } from "./product-info";

export class UpdateVendorInfo {
    
    licenseKey: string = '';
    packageInfo: Array<ProductInfo> = new Array<ProductInfo>();

    constructor() {}
}
