import { PackageRemoveVendorInfo } from "./package-remove-vendor-info";

export class RemoveVendorInfo {

    licenseKey: string = '';
    packageRemoveVendorInfo: Array<PackageRemoveVendorInfo> = new Array<PackageRemoveVendorInfo>();

    constructor() {}
}
