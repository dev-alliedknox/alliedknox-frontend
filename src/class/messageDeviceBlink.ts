export class MessageDeviceBlink {
    cpf: string = '';
    imei: string = '';
    dtHrAuth: string = '';
    valorCompra: number = 0;
    messageId: number = 0;
    dtVenc: string = '';
    parcelaAberta: number = 0;
    blinkInterval: number = 10;
    apiurl: string = '';
}