export class notificacaoMensagens {
    id: number = 0;
    varejista: string = '';
    // idlinxresellers: number = 0;
    idempresa: number = 0;
    tipo:string = '';
    dtHr: string = '';
    dtUpd: string = '';
    timeSpan: Date = new Date();
    timeSpanUpd: Date = new Date();
    texto: string = '';
    status: string = '';

    constructor() {}
}

export const filterDateTime = (listaNotificacaoMensagens: Array<notificacaoMensagens>): Array<notificacaoMensagens> => {
    listaNotificacaoMensagens.reduce((previous, current, index, array) => {
      if(current.dtHr !== null && current.dtHr !== undefined && current.dtHr !== '') {
      const res = current.dtHr.split(' ');
      const [day, month, year] = res[0].split('/');
      const date = day + '/' + month + '/' + year;
      const [hour, minute, second] = res[1].split(":");
      const time = hour + ':' + minute + ':' + second.split('.')[0];
      const strminute = second.split('.')[0];
      current.dtHr = date + ' ' + time;
      current.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      }
      
      // if(current.dtUpd) {
      //   const res = current.dtUpd.split('T');
      //   const [year, month, day] = res[0].split('-');
      //   const date = day + '/' + month + '/' + year;
      //   const [hour, minute, second] = res[1].split(":");
      //   const time = hour + ':' + minute + ':' + second.split('.')[0];
      //   const strminute = second.split('.')[0];
      //   current.dtUpd = date + ' ' + time;
      //   current.timeSpanUpd = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(strminute));
      // }

      return array;
    }, {});
    return listaNotificacaoMensagens;
  }