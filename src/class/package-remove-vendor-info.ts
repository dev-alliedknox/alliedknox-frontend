export class PackageRemoveVendorInfo {

    packageName: string = '';
    publicKeyHash: string = '';
    packageType: string = '';

    constructor() {}

}
