export class MessageDevice {
    cpf: string = '';
    imei: string = '';
    dtHrAuth: string = '';
    valorCompra: number = 0;
    messageId: number = 0;
    dtVenc: string = '';
    parcelaAberta: number = 0;
    apiurl: string = '';
}