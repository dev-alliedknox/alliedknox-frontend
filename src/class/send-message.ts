export class SendMessage {

    email: string = '';
    tel: string = '';
    message: string = '';
    messageType: number = 0;
    aprroveId: Array<string> = new Array<string>();
    deviceUid: Array<string> = new Array<string>();
    objectId: Array<string> = new Array<string>();
    
    constructor() {}
}
