export class BlinkDevices {

    email: string = '';
    tel: string = '';
    interval: number = 0;
    message: string = '';
    aprroveId: Array<string> = new Array<string>();
    deviceUid: Array<string> = new Array<string>();
    objectId: Array<string> = new Array<string>();
    
    constructor() {}
}
