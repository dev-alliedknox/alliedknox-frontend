export class KdpListDevicesStatus {

  resellerId: string;
  customerId: string;
  vendorId: string;
  transactionId: string;
  operationType: string;
  state: string;
  pageNum: number;
  pageSize: number;
  timestamp;
  sortBy: string;
  sortOrder: string;

    constructor() {}
}
