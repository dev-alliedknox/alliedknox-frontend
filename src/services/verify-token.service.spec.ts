import { TestBed, inject } from '@angular/core/testing';

import { VerifyTokenService } from './verify-token.service';

describe('VerifyTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VerifyTokenService]
    });
  });

  it('should be created', inject([VerifyTokenService], (service: VerifyTokenService) => {
    expect(service).toBeTruthy();
  }));
});
