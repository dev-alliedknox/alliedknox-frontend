import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../environments/environment';
import { JWTDecoderService } from './jwt-decoder.service';

@Injectable({
  providedIn: 'root'
})
export class VerifyTokenService {

  constructor(private cookieService: CookieService, private jwtDecoder: JWTDecoderService, private router: Router) { }

  public verifyToken = () => {
    if(this.cookieService.check('accessToken'))  {
      const decodedToken = this.jwtDecoder.decodedToken(this.cookieService.get('accessToken'));
      console.log(decodedToken);
      if(this.jwtDecoder.isExpired(this.cookieService.get('accessToken'))) {
        this.deleteCredentials();
        this.router.navigate(['/login']);
      } else {
        this.router.navigate(['/home']);
      }
    } else {
      this.deleteCredentials();
      this.router.navigate(['/login']);
    }
  }

  public getToken = (): string => {
    if(environment.access_token === '') {
      this.verifyToken();
      return environment.access_token;
    }

    return environment.access_token;
  }

  public logOut = () => {
    this.deleteCredentials();
    this.cookieService.delete('LoggedIn');
    this.router.navigate(['/login']);
  }

  public deleteCredentials = () => {
    this.cookieService.delete('accessToken');
  }
}
