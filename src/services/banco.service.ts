import { Injectable } from '@angular/core';
// import { Http, RequestOptions, Headers } from '@angular/http';
import { Http, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { EstoqueLicensas } from '../class/estoqueLicencas';
import { cadastroEmails } from 'src/class/cadastroEmails';
import { serialWhitelist } from 'src/class/serialWhitelist';
import { LinxResellers } from 'src/class/linxResellers';
import { ListaMensagem } from 'src/class/listaMensagens';
import { UnionMensagensTags } from 'src/class/unionMensagensTags';
import { MessageDevice } from 'src/class/messageDevice';
import { environment } from '../environments/environment';
import TransactionRequest from 'src/class/transactionRequest';
import { RegistroManual } from 'src/class/registro-manual';
import { RelacionamentoSoudi } from 'src/class/relacionamento-soudi';

@Injectable({
  providedIn: 'root'
})
export class BancoService {

  private headers: Headers = new Headers();

  constructor(private http: Http, private cookeService: CookieService) { }

  public idempresa;

  public getIdEmpresa() {
    this.idempresa = this.cookeService.get('idempresa');
    // console.log('idempresa', this.idempresa);
  }
  private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken').toString());

  public getHistoricoTransacao = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/historicoTransacao/?idempresa=${this.idempresa}`).toPromise();
  }

  public getStatusDispositivos = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/statusDispositivos?idempresa=${this.idempresa}`).toPromise();
  }

  public getNotificacaoMensagens = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/notificacaoMensagens?idempresa=${this.idempresa}`).toPromise();
  }

  public getLogNotificacoes = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/logNotificacoes?idempresa=${this.idempresa}`).toPromise();
  }

  public getAllEstoqueLicensa = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/estoqueLicencas?idempresa=${this.idempresa}`).toPromise();
  }

  public postEstoqueLicencas = async (prEstoqueSeguranca: EstoqueLicensas): Promise<any> => {

    return this.http.post(`${environment.API_URL_LOCAL}/estoqueLicencas`, prEstoqueSeguranca).toPromise();
  }

  public postImeiMensagens = async (data: any): Promise<any> => {

    return this.http.post(`${environment.API_URL_LOCAL}/imeiMensagens`, data).toPromise();
  }

  public getAllSerialWhitelist = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/serialWhitelist?idempresa=${this.idempresa}`).toPromise();
  }

  public postSerialWhitelist = async (whitelist: serialWhitelist): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/serialWhitelist`, whitelist).toPromise();
  }

  public deleteWhiteList = async (id: number): Promise<any> => {
    this.getIdEmpresa();
    return this.http.delete(`${environment.API_URL_LOCAL}/serialWhitelist?id=${id}&idempresa=${this.idempresa}`).toPromise();
  }

  public getAllLinxResellers = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/linxResellers?idempresa=${this.idempresa}`).toPromise();
  }
  public getAllCadastroEmail = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/cadastroEmail?idempresa=${this.idempresa}`).toPromise();
  }

  public getAllTransacaoVarejista = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/transacaoVarejista?idempresa=${this.idempresa}`).toPromise();
  }

  public getAllStatusCompraLicensa = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/StatusCompraLicensa?idempresa=${this.idempresa}`).toPromise();
  }

  public postCadastroEmail = async (cadastroEmail: cadastroEmails ): Promise<any> => {
    // this.getIdEmpresa();;
    return this.http.post(`${environment.API_URL_LOCAL}/CadastroEmail`, cadastroEmail).toPromise();
  }

  public putCadastroEmail = async (cadastroEmail: cadastroEmails ): Promise<any> => {
    // this.getIdEmpresa();;
    return this.http.put(`${environment.API_URL_LOCAL}/CadastroEmail`, cadastroEmail).toPromise();
  }

  public deleteCadastroEmail = async (id: number): Promise<any> => {
    this.getIdEmpresa();
    return this.http.delete(`${environment.API_URL_LOCAL}/cadastroEmail?id=${id}&idempresa=${this.idempresa}`).toPromise();
  }

  public getAllLinxMovimentoSerial = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/LinxMovimentoSerial?idempresa=${this.idempresa}`).toPromise();
  }

  public getAllTagsMensagens = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/tagsMensagens?idempresa=${this.idempresa}`).toPromise();
  }

  public postListaMensagens = async (listaMensagens: ListaMensagem): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/listaMensagens`, listaMensagens ).toPromise();
  }

  public putListaMensagens = async (listaMensagens: ListaMensagem): Promise<any> => {
    return this.http.put(`${environment.API_URL_LOCAL}/listaMensagens`, listaMensagens).toPromise();
  }

  public getAllListaMensagens = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaMensagens?idempresa=${this.idempresa}`).toPromise();
  }

  public postUnionMensagensTags = async (unionMensagensTags: UnionMensagensTags): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/unionMensagensTags`, unionMensagensTags).toPromise();
  }

  // public putUnionMensagensTags = async (unionMensagensTags: UnionMensagensTags): Promise<any> => {
  //   return this.http.put(`${environment.API_URL_LOCAL}/unionMensagensTags`, unionMensagensTags).toPromise();
  // }

  public getUnionMensagensTags = async (id): Promise<any> => {
    return this.http.get(`${environment.API_URL_LOCAL}/unionMensagensTags?id=${id}`).toPromise();
  }

  public getCpfFaturamento = async (message: MessageDevice, idempresa: any): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/faturamento?cpf=${message.cpf}&idempresa=${idempresa}`).toPromise();
  }

  public getListaBloqueados = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaBloqueados?idempresa=${this.idempresa}`).toPromise();
  }

  public getListaBlinkados = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaBlinkados?idempresa=${this.idempresa}`).toPromise();
  }

  public getListaAtivos = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaAtivos?idempresa=${this.idempresa}`).toPromise();
  }

  public getListaQuitados = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaQuitados?idempresa=${this.idempresa}`).toPromise();
  }

  public getListaImeiManual = async (): Promise<any> => {
    this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/listaImeiManual?idempresa=${this.idempresa}`).toPromise();
  }

  public postResellers = async (linxResellers: LinxResellers): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/resellerVarejista`, linxResellers).toPromise();
  }

  public putResellers = async (linxResellers: LinxResellers): Promise<any> => {
    return this.http.put(`${environment.API_URL_LOCAL}/resellerVarejista`, linxResellers).toPromise();
  }

  public getTransactionId = async (): Promise<any> => {
    return this.http.get(`${environment.API_URL_LOCAL}/transactionId?idempresa=${this.idempresa}`).toPromise();
  }

  public gravaRequest = async (transactionRequest: TransactionRequest): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/gravaRequest`, transactionRequest).toPromise();
  }

  public gravaResponse = async (transactionId: number, response: string): Promise<any> => {
    return this.http.post(`${environment.API_URL_LOCAL}/gravaResponse`, {'transactionId': transactionId, 'response': response}).toPromise();
  }

  public updateStatusKg = async (serial: string, status: string): Promise<any> => {
    return this.http.put(`${environment.API_URL_LOCAL}/updateStatusKg`, {'serial': serial, 'status': status}).toPromise();
  }

  public getVendaManual = async (): Promise<any> => {
    // this.createAuthHeader();
    this.getIdEmpresa();
    // return this.http.get(environment.API_URL_LOCAL + `/vendaManual?idempresa=${this.idempresa}`, { headers: this.headers}).toPromise();
    return this.http.get(environment.API_URL_LOCAL + `/getVendaManual?idempresa=${this.idempresa}`).toPromise();
  }

  public postVendaManual = async (vendaManual: RegistroManual[]): Promise<any> => {
    this.createAuthHeader();
    return this.http.post(environment.API_URL_LOCAL + '/vendaManual', vendaManual, { headers: this.headers}).toPromise();
  }

  public putVendaManual = async (vendaManual: RegistroManual[]): Promise<any> => {
    this.createAuthHeader();
    return this.http.put(environment.API_URL_LOCAL + '/vendaManual', vendaManual, { headers: this.headers}).toPromise();
  }

  public postVendaManualSpreadSheet = async (formVendaManual: FormData): Promise<any> => {
    this.createAuthHeader();
    return this.http.post(environment.API_URL_LOCAL + '/vendaManual/SpreadSheet', formVendaManual, { headers: this.headers}).toPromise();
  }

  public getRelacionamentoSoudi = async (): Promise<any> => {
    // this.getIdEmpresa();
    return this.http.get(`${environment.API_URL_LOCAL}/relacionamentoSoudi`).toPromise();
  }

  public putRelacionamentoSoudi = async (soudi: RelacionamentoSoudi): Promise<any> => {
    // this.getIdEmpresa();
    return this.http.put(`${environment.API_URL_LOCAL}/relacionamentoSoudi`, soudi).toPromise();
  }

  public removeMovSerial = async (imei: string): Promise<any> => {
    // this.getIdEmpresa();
    return this.http.put(`${environment.API_URL_LOCAL}/removeMovSerial`, { imei: imei } ).toPromise();
  }

}
