import { TestBed, inject } from '@angular/core/testing';

import { JWTDecoderService } from './jwt-decoder.service';

describe('JWTDecoderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JWTDecoderService]
    });
  });

  it('should be created', inject([JWTDecoderService], (service: JWTDecoderService) => {
    expect(service).toBeTruthy();
  }));
});
