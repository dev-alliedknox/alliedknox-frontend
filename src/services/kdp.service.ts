import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { UploadDevices } from 'src/class/upload-devices';
import { KdpDeleteDevices } from 'src/class/kdp-delete-devices';
import { KdpListDevicesStatus } from 'src/class/kdp-list-devices-status';
// import { devicesReseller } from 'src/class/';
// import { GetLicenseInfo } from 'src/class/';

@Injectable({
    providedIn: 'root'
  })
export class KdpService {

    private accessToken: string = '';

    headers: Headers = new Headers();

    constructor(private http: Http, private cookeService: CookieService) { }

    // private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken'))
    private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken').toString());
/*
    public postAccessToken = async (): Promise<any> => {
        const response = await this.http.post(environment.API_URL_AUTH_EXT + '/accesstoken',
        {user: environment.USER, password: environment.PASSWORD},
        // {user: 'portal', password: 'portal@01'},
        new RequestOptions({ headers: this.headers })).toPromise();

        const access = response.json();
        this.accessToken = access.accessToken;
        this.createAuthHeader();
    }
*/
    public uploadDevices = (uploadDevices: UploadDevices): Promise<any> => {
        this.createAuthHeader();
        return this.http.put(`${environment.API_URL_KDP}/devices/upload`, uploadDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public deleteDevices = (deleteDevices: KdpDeleteDevices): Promise<any> => {
        this.createAuthHeader();
        return this.http.put(`${environment.API_URL_KDP}/devices/delete/`, deleteDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public listDevicesStatus = (listDevicesStatus: KdpListDevicesStatus): Promise<any> => {
        this.createAuthHeader();
        return this.http.get(`${environment.API_URL_KDP}/devices/status?transactionId=${listDevicesStatus.transactionId}&operationType=${listDevicesStatus.operationType}&state=${listDevicesStatus.state}&sortOrder=${listDevicesStatus.sortOrder}`,
        // return this.http.get(`${environment.API_URL_KDP}/devices/status?resellerId=${listDevicesStatus.resellerId}&customerId=${listDevicesStatus.customerId}&vendorId=${listDevicesStatus.vendorId}&transactionId=${listDevicesStatus.transactionId}&operationType=${listDevicesStatus.operationType}&state=${listDevicesStatus.state}&pageNum=${listDevicesStatus.pageNum}&pageSize=${listDevicesStatus.pageSize}&timestamp=${listDevicesStatus.timestamp}&sortBy=${listDevicesStatus.sortBy}&sortOrder=${listDevicesStatus.sortOrder}`,        
        new RequestOptions({headers: this.headers})).toPromise();
    }

    // public devicesReseller = (devicesReseller: devicesReseller): Observable<any> => {
    //     this.createAuthHeader();
    //     return this.http.post(`${environment.API_URL_KDP}/devices`, devicesReseller, new RequestOptions({headers: this.headers}));
    // }
    // public customersReseller = (getLicenseInfo: GetLicenseInfo): Observable<any> => {
    //     this.createAuthHeader();
    //     return this.http.post(`${environment.API_URL_KDP}/customers/list`, getLicenseInfo, new RequestOptions({headers: this.headers}));
    // }
}
