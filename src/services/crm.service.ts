import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { MessageDevice } from 'src/class/messageDevice';
import { ObjectPin } from 'src/class/objectPin';
import { retry } from 'rxjs/operators';
import { LockDevices } from 'src/class/lock-devices';
import { UnlockDevices } from 'src/class/unlock-devices';
import { MessageDeviceBlink } from 'src/class/messageDeviceBlink';


@Injectable({
  providedIn: 'root'
})
export class CrmService {

  private accessToken = '';
  private headers: Headers = new Headers();

  constructor(private http: Http, private cookeService: CookieService) { }

  private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken').toString());
  // private createAuthHeader = () => this.headers.set('x-access-token', this.accessToken);
/*
  public postAccessToken = async (): Promise<any> => {
      const response = await this.http.post(environment.API_URL_AUTH_EXT + '/accesstoken',
      {user: environment.USER, password: environment.PASSWORD},
      // {user: 'portal', password: 'portal@01'},
      new RequestOptions({ headers: this.headers })).toPromise();

      const access = response.json();
      this.accessToken = access.accessToken;
      this.createAuthHeader();
  }
*/
  public postSendMessage = async (messageDevice: MessageDevice): Promise<any> => {
    this.createAuthHeader();
    return await this.http.post(environment.API_URL_CRM + '/messages', messageDevice,
    new RequestOptions({ headers: this.headers })).toPromise();
  }

  public postBlinkMessage = async (BlinkDevice: MessageDeviceBlink): Promise<any> => {
    this.createAuthHeader();
    return await this.http.post(environment.API_URL_CRM + '/messages/blink', BlinkDevice,
    new RequestOptions({ headers: this.headers })).toPromise();
  }

  public putLockDevices = async (lockDevice: LockDevices): Promise<any> => {
    this.createAuthHeader();
    return await this.http.put(environment.API_URL_CRM + '/devices/lock', lockDevice,
    new RequestOptions({ headers: this.headers })).toPromise();
  }

  public putUnlockDevices = async (unlockDevice: UnlockDevices): Promise<any> => {
    this.createAuthHeader();
    return await this.http.put(environment.API_URL_CRM + '/devices/unlock', unlockDevice,
    new RequestOptions({ headers: this.headers })).toPromise();
  }

  public postGetPin = async (objectPin: ObjectPin): Promise<any> => {
    this.createAuthHeader();
    return await this.http.post(environment.API_URL_CRM + '/devices/pin', objectPin,
    new RequestOptions({ headers: this.headers })).toPromise();
  }
}
