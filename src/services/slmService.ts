import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';

import { GenerateLicense } from 'src/class/generatelicense';
import { ExtendLicenses } from 'src/class/extend-licenses';
import { ChangeLicenses } from 'src/class/change-licenses';
import { CustomerLicenses } from 'src/class/customer-licenses';
import { GetLicenseInfo } from 'src/class/get-license-info';
import { AddVendorInfo } from 'src/class/add-vendor-info';
import { UpdateVendorInfo } from 'src/class/update-vendor-info';
import { RemoveVendorInfo } from 'src/class/remove-vendor-info';
import { GetvendorInfo } from 'src/class/get-vendor-info';
import { Usage } from 'src/class/usage';

@Injectable()
export class slmService {

    private headers: Headers = new Headers();

    constructor(private http: Http, private cookeService: CookieService) { }

    // private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken'))
    private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken').toString());

    public licenseGenerate = async (generateLicense: GenerateLicense): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/generate`, generateLicense, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public extendLicenses = async (extendLicenses: ExtendLicenses): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/extend/`, extendLicenses, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public changeLicenses = async (changeLicenses: ChangeLicenses): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/change`, changeLicenses, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public customerLicenses = async (customerLicenses: CustomerLicenses): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`http://localhost:3001/slm/license/customerLicense`, customerLicenses, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public getLicenseInfo = async (getLicenseInfo: GetLicenseInfo): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/licenseInfo`, getLicenseInfo, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public addVendorInfo = async (addVendorInfo: AddVendorInfo): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/addVendorInfo/`, addVendorInfo, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public updateVendorInfo = async (updateVendorInfo: UpdateVendorInfo): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/updateVendorInfo`, updateVendorInfo, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public removeVendorInfo = async (removeVendorInfo: RemoveVendorInfo): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/removeVendorInfo/`, removeVendorInfo, new RequestOptions({ headers: this.headers })).toPromise();
    }
    public getVendorInfo = async (getVendorInfo: GetvendorInfo): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL}/license/getVendorInfo/`, getVendorInfo, new RequestOptions({ headers: this.headers })).toPromise();
    }

    public getUsage = async (): Promise<Usage | any> => {
        this.createAuthHeader();
        return this.http.get(`${environment.API_URL_LOCAL}/slm/license/getUsage`, new RequestOptions({ headers: this.headers })).toPromise();
    }

    public updateQuantity= async (body: any): Promise<any> => {
        this.createAuthHeader();
        return this.http.post(`${environment.API_URL_SLM}/license/updateQuantity`, body , new RequestOptions({headers: this.headers})).toPromise();
    }
}