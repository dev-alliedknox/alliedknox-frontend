import { Http } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/class/user';
import { Group } from 'src/class/group';
import { getErrorBody } from 'src/utils/error-utils';

@Injectable({
    providedIn: 'root'
})

export class AuthorizationService {


    constructor(private http: Http, private cookeService: CookieService, private httpClient: HttpClient) { }

    public listUsers = async (): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/users')
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).users);
    }

    public deleteUser = async(id: number): Promise<any> => {
        return await this.http.delete(
            environment.API_URL_AUTHORIZATION + '/users/' + id)
            .toPromise();
    }

    public createUser = async(user: User, resellers: number[]): Promise<any> => {

        return await this.http.post(`${environment.API_URL_AUTHORIZATION}/users`, {user: user, resellers: resellers})
            .toPromise();
    }

    public listGrupos = async (): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/groups')
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).groups);
    }

    public deleteGrupo = async(id: number): Promise<any> => {
        return await this.http.delete(
            environment.API_URL_AUTHORIZATION + '/groups/' + id)
            .toPromise();
    }

    public createGrupo = async(group: Group): Promise<any> => {
        return await this.http.post(
            environment.API_URL_AUTHORIZATION + '/groups', {
                'group': {
                    'name': group.name,
                    'description': group.description
                }
            }
        ).toPromise();
    }

    public updateUser = async(id, user: User, resellers: number[]): Promise<any> => {

        return await this.http.put(`${environment.API_URL_AUTHORIZATION}/users/${id}`, { user: user, resellers: resellers })
            .toPromise();
    }

    public disassociateUser = async(idUser: number, idGroup: number): Promise<any> => {
        return await this.httpClient.request('delete',
            environment.API_URL_AUTHORIZATION + '/groups/users/disassociate',
            {body: {'associations': [{'idUser': idUser, 'idGroup': idGroup}]} })
            .toPromise();
    }

    public associateUser = async(payload: any): Promise<any> => {
        return await this.http.post(environment.API_URL_AUTHORIZATION + '/groups/users/associate',
            {associations: payload})
            .toPromise();
    }

    public listUsersByGrupo = async (idGroup: number): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/groups/users/' + idGroup)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).users);
    }


    public listProfiles = async (): Promise<any> => {

        return await this.http.get(`${environment.API_URL_AUTHORIZATION}/profiles`)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).profiles)
            .catch(getErrorBody);
    }

    public createProfile = async (profile): Promise<any> => {

        return await this.http.post(`${environment.API_URL_AUTHORIZATION}/profiles`, { profile: profile })
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).profileId)
            .catch(getErrorBody);
    }

    public deleteProfile = async (id: number): Promise<any> => {

        return await this.http.delete(`${environment.API_URL_AUTHORIZATION}/profiles/${id}`)
            .toPromise()
            .catch(getErrorBody);
    }

    public listPermissions = async (): Promise<any> => {

        return await this.http.get(`${environment.API_URL_AUTHORIZATION}/permissions`)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).permissions);
    }

    public associatePermissions = async (permissionAssociations): Promise<any> => {

        return await this.http.post(`${environment.API_URL_AUTHORIZATION}/profiles/permissions/associate`,
            { permissionAssociations: permissionAssociations })
                .toPromise()
                .catch(getErrorBody);
    }

    public associateGroups = async (permissionGroups): Promise<any> => {

        return await this.http.post(`${environment.API_URL_AUTHORIZATION}/profiles/groups/associate`,
            { permissionGroups: permissionGroups })
                .toPromise()
                .catch(getErrorBody);
    }

    public updateProfile = async(id: number, profile): Promise<any> => {

        return await this.http.put(`${environment.API_URL_AUTHORIZATION}/profiles/${id}`, { profile: profile })
            .toPromise()
            .catch(getErrorBody);
    }

    public listGroups = async (): Promise<any> => {

        return await this.http.get(`${environment.API_URL_AUTHORIZATION}/groups`)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).groups);
    }

    public getGroupById = async (id: number): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/groups/' + id)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body));
    }

    public updateGrupo = async(grupo: Group, id): Promise<any> => {
        delete grupo.id;
        return await this.http.put(
            environment.API_URL_AUTHORIZATION + '/groups/' + id, {group: grupo}
        ).toPromise();
    }

    public listPermissionsByProfile = async (profileId: number): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/profiles/permissions/' + profileId)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).permissions);
    }

    public listGroupsByProfile = async (profileId: number): Promise<any> => {
        return await this.http.get(
            environment.API_URL_AUTHORIZATION + '/profiles/groups/' + profileId)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).groups);
    }

    public disassociateGroups = async (profileGroups): Promise<any> => {

        return await this.httpClient.request('delete', `${environment.API_URL_AUTHORIZATION}/profiles/groups/disassociate`,
            { body: { profileGroups: profileGroups }})
            .toPromise()
            .catch(getErrorBody);
    }

    public listResellers = async (): Promise<any> => {

        return await this.http.get(`${environment.API_URL_AUTHORIZATION}/resellers`)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).resellers);
    }

    public listResellersByUserId = async (idUser: number): Promise<any> => {

        return await this.http.get(`${environment.API_URL_AUTHORIZATION}/users/${idUser}/resellers`)
            .toPromise()
            .then((resp: any) => JSON.parse(resp._body).resellers);
    }

    public disassociateResellers = async (userResellers): Promise<any> => {

        return await this.httpClient.request('delete', `${environment.API_URL_AUTHORIZATION}/users/resellers/disassociate`,
            { body: { userResellers: userResellers }})
            .toPromise()
            .catch(getErrorBody);
    }
}
