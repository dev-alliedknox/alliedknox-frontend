import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class AuthenticationService {

    constructor(private http: Http) { }

    public login = (user: string, password: string): Promise<any> =>
        this.http.post(`${environment.API_URL}/accesstoken`, {user: user, password: password}).toPromise();
}