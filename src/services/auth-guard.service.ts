import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private cookieService: CookieService) {}

    canActivate() {
        if (this.cookieService.check('accessToken')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}