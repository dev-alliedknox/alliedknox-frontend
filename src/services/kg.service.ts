import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { GetOfflinePin } from 'src/class/get-offline-pin';
import { GetPin } from 'src/class/get-pin';
import { AddLicense } from 'src/class/add-license';
import { ApproveDevice } from 'src/class/approve-device';
import { BlinkDevices } from 'src/class/blink-devices';
import { DeleteDevices } from 'src/class/delete-devices';
import { LockDevices } from 'src/class/lock-devices';
import { SendMessage } from 'src/class/send-message';
import { UnlockDevices } from 'src/class/unlock-devices';
import { UpdateInfo } from 'src/class/update-info';
import { CompleteDevices } from 'src/class/complete-devices';


@Injectable({
  providedIn: 'root'
})
export class KgService {

    private accessToken = '';
    headers: Headers = new Headers();

    constructor(private http: Http, private cookeService: CookieService) { }

    private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken'));
    // private createAuthHeader = () => this.headers.set('x-access-token', this.cookeService.get('accessToken').toString());
/*
    public postAccessToken = async (): Promise<any> => {
        const response = await this.http.post(environment.API_URL_AUTH_EXT + '/accesstoken',
        {user: environment.USER, password: environment.PASSWORD},
        // {user: 'portal', password: 'portal@01'},
        new RequestOptions({ headers: this.headers })).toPromise();

        const access = response.json();
        this.accessToken = access.accessToken;
        this.createAuthHeader();
    }
*/
    public getOfflinePin = async (getOfflinePin: GetOfflinePin): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/device/getOfflinePin`, getOfflinePin,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public getPinDevice = async (getPinDevice: GetPin): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/device/getPin/`, getPinDevice,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public addLicense = async (addLicense: AddLicense): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/license/add`, addLicense,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public approveDevices = async (approveDevices: ApproveDevice): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/approve/`, approveDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public blinkDevices = async (blinkDevices: BlinkDevices): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/blink`, blinkDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public deleteDevices = async (deleteDevices: DeleteDevices): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/delete/`, deleteDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public lockDevices = async (lockDevices: LockDevices): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/lock`, lockDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public sendMessageDevices = async (sendMessageDevices: SendMessage): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/sendMessage/`, sendMessageDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public unlockDevices = async (unlockDevices: UnlockDevices): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/unlock/`, unlockDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public updateCurrentPayment = async (updateCurrentPayment: UpdateInfo): Promise<any> => {
      this.createAuthHeader();
      return await this.http.post(`${environment.API_URL_KG}/devices/updateInfo/`, updateCurrentPayment,
      new RequestOptions({headers: this.headers})).toPromise();
    }

    public deviceStatus = async (serial: string): Promise<any> => {
        this.createAuthHeader();
        return await this.http.post(`${environment.API_URL_KG}/devices/list`, {'pageNum': 0, 'pageSize': 5, 'search': serial},
        new RequestOptions({headers: this.headers})).toPromise();
    }

    public completeDevices = async (completeDevices: CompleteDevices): Promise<any> => {
        this.createAuthHeader();
        console.log('HEADER: ', this.headers);
        return await this.http.post(`${environment.API_URL_KG}/devices/complete/`, completeDevices,
        new RequestOptions({headers: this.headers})).toPromise();
    }
}
