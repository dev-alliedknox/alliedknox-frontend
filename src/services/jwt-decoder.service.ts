import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })


export class JWTDecoderService {

    private helper: JwtHelperService = new JwtHelperService();

    decodedToken = (tokenJwt: string) => this.helper.decodeToken(tokenJwt);
    expirationDate = (tokenJwt: string) => this.helper.getTokenExpirationDate(tokenJwt);
    isExpired = (tokenJwt: string) => this.helper.isTokenExpired(tokenJwt);

}
