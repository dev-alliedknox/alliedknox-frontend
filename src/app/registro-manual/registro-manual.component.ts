import { Component, OnInit } from '@angular/core';
import { RegistroManual } from 'src/class/registro-manual';
import { CadastroRegistroManualComponent } from '../modal/cadastro-registro-manual/cadastro-registro-manual.component';
import { MatDialog } from '@angular/material';
import { BancoService } from 'src/services/banco.service';
import { LoaderService } from '../loading/loader.service';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';


@Component({
  selector: 'app-registro-manual',
  templateUrl: './registro-manual.component.html',
  styleUrls: ['./registro-manual.component.css']
})
export class RegistroManualComponent implements OnInit {

  public fileData: any;
  public modelInputFile;
  listaRegistroManual: Array<RegistroManual> = new Array<RegistroManual>();
  currentPageListaRegistroManual = 1;
  dropdownPagesListaRegistroManual = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaRegistroManual = this.dropdownPagesListaRegistroManual[0].itemsPerPage;

  constructor(
    private dialog: MatDialog,
    private service: BancoService,
    private loader: LoaderService
  ) { }

  async ngOnInit() {
    this.loader.show();
    await this.fillVendaManual();
    this.loader.hide();
   }

   fillVendaManual = async () => {
    try {
      const responseVendaManual = await this.service.getVendaManual();
      this.listaRegistroManual = responseVendaManual.json();
      // console.log('TESTE: ', this.listaRegistroManual);
    } catch (error) {
      console.log(error);
    }
  }

  incluirRegistroManualModal = async () => {
    const dialogRef = this.dialog.open(CadastroRegistroManualComponent, {
      data: new RegistroManual(),
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillVendaManual();
    });
  }

  editarRegistroManual = async (registroManual: RegistroManual) => {
    const dialogRef = this.dialog.open(CadastroRegistroManualComponent, {
      data: registroManual,
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillVendaManual();
    });
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  importarRegistroManualModal = async (fileInput: any) => {
    try {
      this.loader.show();
      this.fileData = fileInput.target.files[0];
      const formData = new FormData();
      formData.append('vendas', this.fileData, this.fileData.name);
      const response = await this.service.postVendaManualSpreadSheet(formData);
      // const resultlist = JSON.parse(response._body).resultlist;
      // if (resultlist.length > 0) { throw new Error('Erro ao inserir'); }
      const result = JSON.parse(response._body);
      if (result.fail > 0) { throw new Error(result.resultlist[0].error.message); }
      await this.fillVendaManual();
      this.loader.hide();
      this.modalMensagem('Registro cadastrado com sucesso.');
    } catch (error) {
      this.loader.hide();
      // let errMsg = JSON.parse(error);
      this.modalMensagem('Erro ao importar registro(s) - ' + error);
      // this.modalMensagem('Erro ao tentar cadastrar registro manual, tente novamente.');
    }
  }

  pesquisarHandler = async (prRegistroManual: RegistroManual) => {
    // const dialogRef = this.dialog.open(HistoricoImeiComponent, {
    //   width: '1214px',
    //   height: '750px',
    //   data: {imei: prLogNotificacao.codigoImei}
    // });
  }

}
