import { Component, OnInit, Injectable } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { LoaderService } from '../loading/loader.service';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { MatDialog } from '@angular/material';
import { VerifyTokenService } from '../../services/verify-token.service';
import { environment } from 'src/environments/environment';


@Injectable()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: string;
  password: string;
  target: string;


  constructor(private verifyTokenService: VerifyTokenService, private dialog: MatDialog, private authenticationService: AuthenticationService, private cookieService: CookieService, private router: Router, private loader: LoaderService) { }

  ngOnInit() {
    this.verifyTokenService.verifyToken();

    if ( (this.cookieService.get('LoggedIn') === 'yes') && (!this.cookieService.get('accessToken')) ) {
      this.cookieService.delete('LoggedIn');
      this.modalMensagem('Seu tempo de sessão esgotou, favor logar novamente!');
    }
  }


  login = async () => {

    if (!this.user || !this.password) {
      this.modalMensagem('Insira usuário e senha');
      return;
    }

    this.loader.show();

    try {
      const response = await this.authenticationService.login(this.user, this.password);

      const data = JSON.parse(response._body);

      this.cookieService.set('LoggedIn', 'yes');
      let expireTokenTime = new Date();
      expireTokenTime.setMinutes(expireTokenTime.getMinutes() + 29);

      this.cookieService.set('accessToken', data.accessToken, expireTokenTime);
      this.cookieService.set('user', this.user);
      this.cookieService.set('idempresa', data.companiesId);
      this.loader.hide();
      this.router.navigate(['/home']);
    } catch (error) {
      this.cookieService.delete('LoggedIn');
      this.loader.hide();
      this.target = '';
      console.log(error);
      const message = JSON.parse(error._body).message;
      this.modalMensagem('Error ao tentar logar.');
    }

  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

}
