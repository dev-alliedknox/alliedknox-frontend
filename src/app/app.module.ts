import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID  } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MenuComponent } from './menu/menu.component';
import { AuthGuard } from '../services/auth-guard.service';
import { AuthenticationService } from '../services/authentication.service';
import { CookieService } from 'ngx-cookie-service';

import { HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { StatusDispositivosComponent } from './status-dispositivos/status-dispositivos';
import { NotificacoesComponent } from './notificacoes/notificacoes.component'
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { PerfisComponent } from './perfis/perfis.component';
import { LogComponent } from './log/log.component';
import { GrdFilterPipe } from './grd-filter.pipe';
import { LogNotificacoesComponent } from './log-notificacao/log-notificacao.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//Material
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import {MatRadioModule} from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
// import { UsuariosModalComponent } from './modal/usuarios-modal/usuarios-modal.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';

//Pagination
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingComponent } from './loading/loading.component';

//datepicker
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { IncluirMensagemComponent } from './modal/incluir-mensagem/incluir-mensagem.component';
import { CadastroemailComponent } from './modal/cadastroemail/cadastroemail.component';
import { WhitelistComponent } from './modal/whitelist/whitelist.component';
import { MensagemComponent } from './modal/mensagem/mensagem.component';
import { LoaderService } from './loading/loader.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EscolhaComponent } from './modal/escolha/escolha.component';
import { IncluirResselersComponent } from './modal/incluir-resselers/incluir-resselers.component';
import { HistoricoImeiComponent } from './modal/historico-imei/historico-imei.component';
import { IncluirUsuarioComponent } from './modal/incluir-usuario/incluir-usuario.component';
import { IncluirPerfilComponent } from './modal/incluir-perfil/incluir-perfil.component';
import { IncluirGrupoComponent } from './modal/incluir-grupo/incluir-grupo.component';
import localePt from "@angular/common/locales/pt";
import { registerLocaleData } from '@angular/common';
import localeBr from '@angular/common/locales/pt';
import { RegistroManualComponent } from './registro-manual/registro-manual.component';
import { DesbloqueioPinComponent } from './modal/desbloqueio-pin/desbloqueio-pin.component';
import { CadastroRegistroManualComponent } from './modal/cadastro-registro-manual/cadastro-registro-manual.component';
import { EditarGrupoComponent } from './modal/editar-grupo/editar-grupo.component';
import { EditaremailComponent } from './modal/editaremail/editaremail.component';
import { UsuariosModalComponent } from './modal/usuarios-modal/usuarios-modal.component';
import { EditarSoudiComponent } from './modal/editar-relacionamento/editar-soudi-component';

registerLocaleData(localeBr, 'pt')

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    MenuComponent,
    FooterComponent,
    HomeComponent,
    HeaderComponent,
    StatusDispositivosComponent,
    NotificacoesComponent,
    ConfiguracoesComponent,
    RegistrarComponent,
    PerfisComponent,
    LogComponent,
    GrdFilterPipe,
    LogNotificacoesComponent,
    EditarGrupoComponent,
    EditaremailComponent,
    EditarSoudiComponent,
    LoadingComponent,
    UsuariosModalComponent,
    IncluirMensagemComponent,
    CadastroemailComponent,
    WhitelistComponent,
    MensagemComponent,
    EscolhaComponent,
    IncluirResselersComponent,
    HistoricoImeiComponent,
    IncluirUsuarioComponent,
    IncluirPerfilComponent,
    IncluirGrupoComponent,
    RegistroManualComponent,
    DesbloqueioPinComponent,
    CadastroRegistroManualComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatDialogModule,
    MatTableModule,
    FontAwesomeModule,
    NgxPaginationModule,
    MatRadioModule,
    NgxDaterangepickerMd.forRoot(),
    MatProgressBarModule,
    MatProgressSpinnerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ],
  providers: [AuthGuard, AuthenticationService,CookieService, LoaderService, MatDatepickerModule,{ provide: LOCALE_ID, useValue: 'pt' }],
  bootstrap: [AppComponent],

  entryComponents:[DesbloqueioPinComponent, UsuariosModalComponent, EditarGrupoComponent, IncluirMensagemComponent, CadastroRegistroManualComponent, EditaremailComponent, EditarSoudiComponent, CadastroemailComponent, MensagemComponent, LoadingComponent, WhitelistComponent, EscolhaComponent, IncluirResselersComponent, HistoricoImeiComponent, IncluirUsuarioComponent, IncluirPerfilComponent, IncluirGrupoComponent]
})
export class AppModule { }
