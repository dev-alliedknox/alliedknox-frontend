import { Component, OnInit } from '@angular/core';
import { slmService } from '../../services/slmService';
import { BancoService } from '../../services/banco.service';
import { CustomerLicenses } from '../../class/customer-licenses';
import { historicoTransacao, filterDateTimeHistoricoTransacao } from '../../class/historicoTransacao';
import { transacaoVarejista } from '../../class/transacaoVarejista';
import { statusLicensaCompra, filterDateStatusLicensaCompra } from '../../class/statusLicensaCompra';
import { EstoqueLicensas } from 'src/class/estoqueLicencas';
import { LinxMovimentoSerial } from 'src/class/linx-movimento-serial';
import { LoaderService } from '../loading/loader.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [slmService]
})
export class HomeComponent implements OnInit {

  quantidadeDisponivel = 0;
  licencasRegistradas = 0;
  licencasAtivas = 0;
  tVarejista = new transacaoVarejista();

  listTable: Array<transacaoVarejista> = new Array<transacaoVarejista>();

  estoqueLicensa: EstoqueLicensas = new EstoqueLicensas();
  linxMovimentoSerial: LinxMovimentoSerial = new LinxMovimentoSerial();

  listaStatusCompraLicensa: Array<statusLicensaCompra> = new Array<statusLicensaCompra>();

  // TABELA HISTORICO TRANSACAO
  listaHistoricoTransacao: Array<historicoTransacao> = new Array<historicoTransacao>();
  currentPageListaHistoricoTransacao = 1;
  dropdownPagesListaHistoricoTransacao = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaHistoricoTransacao = this.dropdownPagesListaHistoricoTransacao[0].itemsPerPage;

  // TABELA TRANSAÇÕES POR VAREJISTA
  listaTransacaovarejista: Array<transacaoVarejista> = new Array<transacaoVarejista>();
  currentPageListaTransacaovarejista = 1;
  dropdownPagesListaTransacaovarejista = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaTransacaovarejista = this.dropdownPagesListaTransacaovarejista[0].itemsPerPage;


  TotalQuantidade = 0;
  TotalLicensasRegistradas = 0;
  TotalLicensasAtivas = 0;


  constructor(private slm: slmService, private bancoService: BancoService, private loader: LoaderService) { }

  async ngOnInit() {
    this.loader.show();
    // var responseUsage = await this.slm.getUsage();
    await this.fillHistoricoTransacao();
    await this.fillTransacaovarejista();
    await this.fillStatusCompraLicensa();
    await this.fillEstoqueLicensa();
    await this.fillLinxMovimentoSerial();
    this.loader.hide();
  }

  filterTable() {
    if (this.tVarejista.toString() === 'varejistas') {
      this.TotalLicensasRegistradas = this.listaTransacaovarejista.reduce((acc, cur) => acc + cur.licencasRegistradas, 0);
      this.TotalLicensasAtivas = this.listaTransacaovarejista.reduce((acc, cur) => acc + cur.licencasAtivadas, 0);
      return this.listTable = this.listaTransacaovarejista;
    }
    const filteredItem = this.listaTransacaovarejista.filter((item: transacaoVarejista) => {
      return item.varejista === String(this.tVarejista);
    });
    this.TotalLicensasRegistradas = filteredItem[0].licencasRegistradas;
    this.TotalLicensasAtivas = filteredItem[0].licencasAtivadas;
    this.listTable = filteredItem;
  }

  fillEstoqueLicensa = async () => {
    try {
      const response = await this.bancoService.getAllEstoqueLicensa();
       // console.log('RESPONSELICENCA: '); console.log(response);
       const res = response.json();
       if (res.length > 0) { this.estoqueLicensa = response.json()[0]; }
    } catch (error) {
      console.log(error);
    }
  }

  fillLinxMovimentoSerial = async () => {
    try {
      const response = await this.bancoService.getAllLinxMovimentoSerial();
      this.linxMovimentoSerial = response.json()[0];
    } catch (error) {
      console.log(error);
    }
  }

  fillStatusCompraLicensa = async () => {
    try {
      const responseHistorico = await this.bancoService.getAllStatusCompraLicensa();
      this.listaStatusCompraLicensa = responseHistorico.json();
      this.listaStatusCompraLicensa = filterDateStatusLicensaCompra(this.listaStatusCompraLicensa);
    } catch (error) {
      console.log(error);
    }
  }

  fillHistoricoTransacao = async () => {
    try {
      const responseHistorico = await this.bancoService.getHistoricoTransacao();
      this.listaHistoricoTransacao = responseHistorico.json();
      this.listaHistoricoTransacao = filterDateTimeHistoricoTransacao(this.listaHistoricoTransacao);
      this.TotalQuantidade = this.listaHistoricoTransacao.reduce((acc, cur) => acc + cur.quantidade, 0);
    } catch (error) {
      console.log(error);
    }
  }

  countLicensasRegistradas = (acc , cur) => acc.licensasRegistradas + cur.licensasRegistradas;

  fillTransacaovarejista = async () => {
    try {
      const responseTransacaoVarejista = await this.bancoService.getAllTransacaoVarejista();
      const t = new transacaoVarejista();
      this.listaTransacaovarejista.push();
      this.listaTransacaovarejista = responseTransacaoVarejista.json();
      this.listTable = responseTransacaoVarejista.json();
      // console.log('LISTA: ', this.listaTransacaovarejista);
      this.TotalLicensasRegistradas = this.listaTransacaovarejista.reduce((acc, cur) => acc + cur.licencasRegistradas, 0);
      this.TotalLicensasAtivas = this.listaTransacaovarejista.reduce((acc, cur) => acc + cur.licencasAtivadas, 0);
    } catch (error) {
      console.log(error);
    }
  }
}
