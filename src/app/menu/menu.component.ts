import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  clientId = (this.cookieService.get('accessToken') !== null && this.cookieService.get('accessToken') !== undefined && this.cookieService.get('accessToken') !== '') ? true : false;

  constructor(private cookieService: CookieService) { }

  ngOnInit() {
  }

}
