import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  clientId = (this.cookieService.get('accessToken') !== null && this.cookieService.get('accessToken') !== undefined && this.cookieService.get('accessToken') !== '') ? true : false;

  constructor(private cookieService: CookieService, private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.cookieService.delete('LoggedIn');
    this.cookieService.delete('user');
    this.cookieService.delete('accessToken');
    this.cookieService.delete('idempresa');
    this.router.navigate(['/login']);
  }

}
