import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from '../services/auth-guard.service';
// Menu Soudi
import { HomeComponent } from './home/home.component';
import { StatusDispositivosComponent } from './status-dispositivos/status-dispositivos';
import { NotificacoesComponent } from './notificacoes/notificacoes.component';
import { RegistroManualComponent } from './registro-manual/registro-manual.component';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { PerfisComponent } from './perfis/perfis.component';
import { LogComponent } from './log/log.component';
import { LogNotificacoesComponent } from './log-notificacao/log-notificacao.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'home' , component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'status-dispositivos', component: StatusDispositivosComponent ,canActivate:[AuthGuard]},
  { path: 'notificacoes', component: NotificacoesComponent, canActivate:[AuthGuard] },
  { path: 'registro-manual', component: RegistroManualComponent, canActivate:[AuthGuard] },
  { path: 'configuracoes', component: ConfiguracoesComponent ,canActivate:[AuthGuard]},
  { path: 'registrar', component: RegistrarComponent ,canActivate:[AuthGuard]},
  { path: 'perfis', component: PerfisComponent ,canActivate:[AuthGuard]},
  { path: 'log', component: LogComponent, canActivate:[AuthGuard]}, 
  { path: 'log-notificacao', component: LogNotificacoesComponent, canActivate:[AuthGuard]}, 

  { path: '404', component: NotFoundComponent, canActivate: [AuthGuard]  },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})

export class AppRoutingModule { }