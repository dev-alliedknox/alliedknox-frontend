import { Component, OnInit } from '@angular/core';
import { slmService } from '../../services/slmService';
import { BancoService } from '../../services/banco.service';
import { KgService } from 'src/services/kg.service';
import { KdpService } from 'src/services/kdp.service';
// import { StatusDispositivos } from 'src/class/statusDispositivos';
import { EscolhaComponent } from '../modal/escolha/escolha.component';
import { EstoqueLicensas } from 'src/class/estoqueLicencas';
import { cadastroEmails } from 'src/class/cadastroEmails';
import { LinxResellers } from 'src/class/linxResellers';
import { ApproveDevice } from 'src/class/approve-device';
// import { DeleteDevices } from 'src/class/delete-devices';
// import { CompleteDevices } from 'src/class/complete-devices';
import { MatDialog } from '@angular/material';
import { CadastroemailComponent } from '../modal/cadastroemail/cadastroemail.component';
import { IncluirResselersComponent } from '../modal/incluir-resselers/incluir-resselers.component';
import { WhitelistComponent } from '../modal/whitelist/whitelist.component';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { LoaderService } from '../loading/loader.service';
import { ApproveList } from 'src/class/approve-list';
import { UploadDevices } from 'src/class/upload-devices';
import TransactionRequest from 'src/class/transactionRequest';
import { CookieService } from 'ngx-cookie-service';
import { isNullOrUndefined, isArray } from 'util';
import { EditaremailComponent } from '../modal/editaremail/editaremail.component';
// import { transacaoVarejista } from 'src/class/transacaoVarejista';
import { KdpDeleteDevices } from 'src/class/kdp-delete-devices';
import { KdpListDevicesStatus } from 'src/class/kdp-list-devices-status';
import { RelacionamentoSoudi } from 'src/class/relacionamento-soudi';
import { EditarSoudiComponent } from '../modal/editar-relacionamento/editar-soudi-component';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.css'],
  providers: [slmService]
})
export class ConfiguracoesComponent implements OnInit {
  CadastroEmails: cadastroEmails = new cadastroEmails();
  apiWhiteList: Array<cadastroEmails> = new Array<cadastroEmails>();
  listWhiteList: Array<cadastroEmails> = new Array<cadastroEmails>();
  listWhiteTodos: Array<cadastroEmails> = new Array<cadastroEmails>();

  desiredLicenses = 0;

  listaLinxResellers: Array<LinxResellers> = new Array<LinxResellers>();
  currentPageLinxResellers = 1;
  dropdownPagesLinxResellers = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageLinxResellers = this.dropdownPagesLinxResellers[0].itemsPerPage;

  searchEmailList = '';
  searchVarejista = '';

  listaTransacaovarejista: Array<LinxResellers> = new Array<LinxResellers>();
  listTable: Array<LinxResellers> = new Array<LinxResellers>();
  tVarejista = 'varejistas';
  lisCadastroEmails: Array<cadastroEmails> = new Array<cadastroEmails>();
  lisRelacionamento: Array<RelacionamentoSoudi> = new Array<RelacionamentoSoudi>();
  currentPageCadastroEmails = 1;
  lisCadastroEmailsTodos: Array<cadastroEmails> = new Array<cadastroEmails>();
  dropdownPagesCadastroEmails = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageCadastroEmails = this.dropdownPagesCadastroEmails[0].itemsPerPage;

  listaLinxResellersCadastro: Array<LinxResellers> = new Array<LinxResellers>();
  listaLinxResellersCadastroTodos: Array<LinxResellers> = new Array<LinxResellers>();
  listaAux: Array<LinxResellers> = new Array<LinxResellers>();
  estoqueLicensa: EstoqueLicensas = new EstoqueLicensas();
  listEstoqueLicensa: Array<EstoqueLicensas> = new Array<EstoqueLicensas>();
  pattern = /\d+/g;

  approveDevice = new ApproveDevice();
  approveList = new ApproveList();
  // deleteDevices = new DeleteDevices();
  // completeDevices = new CompleteDevices();
  kdpDeleteDevices = new KdpDeleteDevices();
  kdpListDevicesStatus = new KdpListDevicesStatus();
  uploadDevices = new UploadDevices();
  listaImei: Array<string> = new Array<string>();
  imei = '';
  radioASSOC: number;
  User: string = this.cookeService.get('user').toString();
  // idempresa = Number(this.cookeService.get('idempresa'));
  varejistaOption: [{}];
  varejistaEscolhido = 0;

  status = [ {label: 'Status', value: 2}, {label: 'Ativo', value: 1}, {label: 'Inativo', value: 0} ];
  statusModel = 2;

  // tslint:disable-next-line: no-shadowed-variable
  constructor(private slmService: slmService, private kg: KgService, private bancoService: BancoService,
    private kdp: KdpService, private dialog: MatDialog, private loader: LoaderService, private cookeService: CookieService) { }

  async ngOnInit() {
    this.loader.show();
    // await this.fillCadastroEmail();
    this.fillEstoqueLicensa();
    // await this.fillRelacionamento();
    this.fillLinxResellers()
    // var responseUsage = await this.slm.getUsage();
    const responseLinxResellers = await this.bancoService.getAllTransacaoVarejista();
    this.listaLinxResellersCadastro = responseLinxResellers.json();
    this.listaLinxResellersCadastroTodos = responseLinxResellers.json();
    this.loader.hide();
  }

  fillLinxResellers = async () => {
    try {
      const responseLinx = await this.bancoService.getAllLinxResellers();
      this.listaLinxResellers = responseLinx.json();
      this.listaAux = responseLinx.json();
      const varejistas = JSON.parse(responseLinx._body);
      this.varejistaOption = varejistas.map((item) => {
        return {ID: item.idempresa, Nome: item.nome_fantasia };
      });
    } catch (error) {
      // this.modalMensagem(error.json());
    }
  }

  fillCadastroEmail = async () => {
    try {
      const responseCadastroEmail = await this.bancoService.getAllCadastroEmail();
      this.lisCadastroEmails = responseCadastroEmail.json();
      this.lisCadastroEmailsTodos = responseCadastroEmail.json();
    } catch (error) {
      // this.modalMensagem(error.json());
    }
  }

  fillRelacionamento = async () => {
    try {
      const responseRelacionamento = await this.bancoService.getRelacionamentoSoudi();
      this.lisRelacionamento = responseRelacionamento.json();
    } catch (error) {
      console.log(error);
      // this.modalMensagem(error.json());
    }
  }

  fillEstoqueLicensa = async () => {
    try {
      const responseEstoqueLicensa = await this.bancoService.getAllEstoqueLicensa();
      this.listEstoqueLicensa = responseEstoqueLicensa.json();
    } catch (error) {
      // this.modalMensagem(error.json());
    }
  }

  removeDevice = (prListaImei: string) => {
    this.listaImei = this.listaImei.filter(item => item !== prListaImei);
  }

  validateIMEI = async () => {
    if (this.imei.length === 0 || this.imei === '') {
      this.modalMensagem('Campo IMEI/SN é obrigatorio.');
      return;
    }
    if (this.imei.length < 11) {
      this.modalMensagem('Digite um IMEI/SN válido.');
      return;
    }
    this.listaImei.push(this.imei);
    this.imei = '';
    return;
  }

  // filterByStatus = () => {
  //   if (this.statusModel == 2) {
  //     return this.listaLinxResellers = this.listaAux;
  //   } else {
  //     const filterStatus = this.listaAux.filter((item) => {
  //       return item.ativo.includes(this.statusModel.toString());
  //     });
  //     this.listaLinxResellers = filterStatus;
  //   }
  // }

  mostraValorImei = () => {
    // console.log('==radioASSOC== : ' + this.radioASSOC);
  }

  sendForUrlByTipo = async () => {
    if (isNullOrUndefined(this.radioASSOC)) {
      this.modalMensagem('Escolha um tipo de ação (Associar / Desassociar).');
      this.loader.hide();
      return;
    }

    if (this.listaImei.length === 0) {
        this.modalMensagem('Adicione um IMEI/SN para associar ou desassociar.');
    } else {
      this.loader.show();
      const idempresa = Number(this.cookeService.get('idempresa'));
      // console.log('==listaImei=='); console.log(this.listaImei);

      if (Number(this.radioASSOC) == 0) { // ASSOCIAR : só em status 'Pending' -> fazer só pra um e jogar no objeto
        this.modalMensagem('Funcionalidade indisponível no momento.');
        // this.approveDevice[0].deviceUid = '';
/*        try {
          // const resultId = await this.bancoService.getTransactionId();
          // const transactionId = resultId.json();      358692101141580
          // console.log('==TRANSACTIONID=='); console.log(transactionId[0].transactionId);

          this.uploadDevices.devices = this.listaImei;

           const transactionRequest =
           new TransactionRequest('', this.User, 'uploadDevices', 'Upload de dispositivos (KDP)',
                                  this.uploadDevices, this.uploadDevices.devices, this.idempresa); // retirar '1'
                                                                                 // PEGAR IDEMPRESA
// Grava Request no banco
          const transactionId = await this.bancoService.gravaRequest(transactionRequest);
          const tranId = JSON.parse(transactionId._body);
// Upload (KG)
          this.uploadDevices.transactionId = tranId[0].id;
          this.uploadDevices.devices = this.listaImei;
          const responseKdp = await this.kdp.uploadDevices(this.uploadDevices);
// grava response
          const res = JSON.stringify(responseKdp);
          await this.bancoService.gravaResponse(tranId[0].id, res);
// Se houver erro, mostra erro e interrompe fluxo
          const responseBody = JSON.parse(responseKdp._body);
// continuar daqui...
          if (responseKdp.status != 200) {
            this.modalMensagem('Erro ao associar dispositivo(s): KDP');
            this.loader.hide();
          } else {

            // await this.kg.postAccessToken();
            let item = new ApproveList();
            for (let i = 0; i < this.listaImei.length ; i++) {
              item.deviceUid = this.listaImei[i];
              this.approveDevice[i].deviceUid = item.deviceUid;
  //          }
            const transactionRequest =
            new TransactionRequest('', this.User, 'approveDevice', 'Associar dispositivos',
                                   this.approveDevice, this.approveDevice[0].deviceUid, idempresa);
// Grava Request no banco
          const transactionId = await this.bancoService.gravaRequest(transactionRequest);
          const tranId = JSON.parse(transactionId._body);
// Approve (KG)
            var responseKg = await this.kg.approveDevices(this.approveDevice);
// grava response
          const res = JSON.stringify(responseKg);
          await this.bancoService.gravaResponse(tranId[0].id, res);

            this.modalMensagem('Dispositivo(s) associado(s) com sucesso!');
            this.loader.hide();
          }

        } catch (error) {
          const msg = JSON.parse(error._body);
          // this.modalMensagem(msg.message);
          this.modalMensagem('Erro ao associar dispositivo: ' + msg.message);
          this.loader.hide();
        }
        this.uploadDevices.devices = [];
        this.approveDevice = new ApproveDevice();
        return;
*/
      } else { // DESASSOCIAR
        this.kdpDeleteDevices.devices = [];
        try {
          // await this.kdp.postAccessToken();
          this.kdpDeleteDevices.devices[0] = this.listaImei[0];
              // console.log('==deleteDevices.deviceUid=='); console.log(this.kdpDeleteDevices.devices);
          const transactionRequest = new TransactionRequest('', this.User, 'deleteDevices', 'Desassociar dispositivos', this.kdpDeleteDevices, this.kdpDeleteDevices.devices, idempresa);
// Grava Request no banco
          const transactionId = await this.bancoService.gravaRequest(transactionRequest);
              console.log('gravaRequest-response'); console.log(transactionId);
          const tranId = JSON.parse(transactionId._body);
              console.log('tranId: '); console.log(tranId[0].id);
// KDP delete devices
          this.kdpDeleteDevices.transactionId = tranId[0].id;
          const response = await this.kdp.deleteDevices(this.kdpDeleteDevices);
          const responseBody = JSON.parse(response._body);
              console.log('deleteDevices.response: '); console.log(responseBody);
// KDP get status da transação
          this.kdpListDevicesStatus.transactionId = tranId[0].id;
          this.kdpListDevicesStatus.operationType = 'DELETE';
          this.kdpListDevicesStatus.state = 'Rejected';
          this.kdpListDevicesStatus.sortOrder = 'descending';
          const kdpStatus = await this.kdp.listDevicesStatus(this.kdpListDevicesStatus);
              console.log('KdpStatus: '); console.log(kdpStatus);
// Grava Response no banco
          const res = JSON.stringify(kdpStatus);
          await this.bancoService.gravaResponse(tranId[0].id, res);
// Se não houver rejeitados (!= 0): sucesso
          const kdpStatusBody = JSON.parse(kdpStatus._body);
          if (kdpStatusBody.devices.totalCount != 0) {
            const reason = JSON.stringify(kdpStatusBody.devices.transactions[0].devices[0]);
            this.modalMensagem('Erro ao desassociar dispositivo: ' + reason);
            this.loader.hide();
            return;
          } else {
// banco Update (delete de movSerial e faturamento)
            await this.bancoService.removeMovSerial(this.kdpDeleteDevices.devices[0]);
            this.modalMensagem('Dispositivo(s) desassociado(s) com sucesso!');
            this.loader.hide();
          }
        } catch (error) {
          const msg = JSON.parse(error._body);
          // this.modalMensagem(msg.message);
          this.modalMensagem('Erro ao desassociar dispositivo: ' + msg.message);
          this.loader.hide();
        }
        this.kdpDeleteDevices.devices = [];
        this.kdpListDevicesStatus.transactionId = '';
        return;
      }
    }
    this.listaImei = [];
    this.loader.hide();
  }

  // filterTable = async () => {
  //     if (this.tVarejista.toString() === 'varejistas') {
  //       return this.listaLinxResellers = this.listaAux;
  //     }
  //     const filteredItem = this.listaAux.filter((item: LinxResellers) => {
  //       return item.nome_fantasia.includes(this.tVarejista.toString());
  //     });
  //     this.listaLinxResellers = filteredItem;
  // }

  filterAllVarejista  = () => {
    if ((this.searchVarejista === '') &&
        (this.tVarejista.toString() === 'varejistas') &&
        (this.statusModel == 2)) {
      this.listaLinxResellers = this.listaAux;
    }

    if ((this.searchVarejista !== '') &&
        (this.tVarejista.toString() === 'varejistas') &&
        (this.statusModel == 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome.includes(this.searchVarejista)) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista !== '') &&
        (this.tVarejista.toString() !== 'varejistas') &&
        (this.statusModel == 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome.includes(this.searchVarejista) && item.nome_fantasia.includes(this.tVarejista.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista !== '') &&
        (this.tVarejista.toString() === 'varejistas') &&
        (this.statusModel != 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome.includes(this.searchVarejista) && item.ativo.includes(this.statusModel.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista === '') &&
        (this.tVarejista.toString() !== 'varejistas') &&
        (this.statusModel == 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome_fantasia.includes(this.tVarejista.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista === '') &&
        (this.tVarejista.toString() !== 'varejistas') &&
        (this.statusModel != 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome_fantasia.includes(this.tVarejista.toString()) && item.ativo.includes(this.statusModel.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista === '') &&
        (this.tVarejista.toString() === 'varejistas') &&
        (this.statusModel != 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.ativo &&item.ativo.includes(this.statusModel.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }

    if ((this.searchVarejista !== '') &&
        (this.tVarejista.toString() !== 'varejistas') &&
        (this.statusModel != 2)) {
          const filteredList = this.listaAux.filter((item: LinxResellers) => {
              if (item.nome.includes(this.searchVarejista) &&
                item.nome_fantasia.includes(this.tVarejista.toString()) &&
                item.ativo.includes(this.statusModel.toString())) {
                  return item;
              }
          });
          this.listaLinxResellers = filteredList;
    }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  editarVarejistaModal =  (linx) => {
    const dialogRef = this.dialog.open(IncluirResselersComponent, {
      data: {linxResellers: linx, editing: true},
      width: '800px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillLinxResellers();
    });
  }

  modalWhitelist = () => {
    const dialogRef = this.dialog.open(WhitelistComponent, {
      width: '750px',
      height: '500px'
    });
  }

  abrirEmailModal = async () => {
    const dialogRef = this.dialog.open(CadastroemailComponent, {
      width: '600px',
      height: '300px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillCadastroEmail();
    });
  }

  editarEmail = async (prEmail: cadastroEmails) => {
    const dialogRef = this.dialog.open(EditaremailComponent, {
      data: prEmail,
      width: '600px',
      height: '300px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillCadastroEmail();
    });
  }

  editarRelacionamento = async (prSoudi: RelacionamentoSoudi) => {
    const dialogRef = this.dialog.open(EditarSoudiComponent, {
      data: prSoudi,
      width: '500px',
      height: '250px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillRelacionamento();
    });
  }

  abrirCadastroVarejistaModal = async () => {
    const dialogRef = this.dialog.open(IncluirResselersComponent, {
      data: {linxResellers: {}, editing: false},
      width: '750px',
      height: 'auto'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.fillLinxResellers();
    });
  }

  onlyNumberSeguranca = () => {
    if (String(this.estoqueLicensa.qtdSeguranca) !== '') {
      const newValue = String(this.estoqueLicensa.qtdSeguranca).match(this.pattern)[0];
      this.estoqueLicensa.qtdSeguranca = Number(newValue);
    } else {
      this.estoqueLicensa.qtdSeguranca = 0;
    }
  }

  onlyNumberDiseredLicenses = () => {
    if (String(this.desiredLicenses) !== '') {
      const newValue = String(this.desiredLicenses).match(this.pattern)[0];
      this.desiredLicenses = Number(newValue);
    } else {
      this.desiredLicenses = 0;
    }
  }

  onlyNumberIncremento = () => {
    if (String(this.estoqueLicensa.incrementoCompra) !== '') {
      const newValue = String(this.estoqueLicensa.incrementoCompra).match(this.pattern)[0];
      this.estoqueLicensa.incrementoCompra = Number(newValue);
    } else {
      this.estoqueLicensa.incrementoCompra = 0;
    }
  }

  confirmHandleClick = async () => {
    this.estoqueLicensa.incrementoCompra = Number(this.estoqueLicensa.incrementoCompra);
    this.estoqueLicensa.qtdSeguranca = Number(this.estoqueLicensa.qtdSeguranca);
    this.estoqueLicensa.idempresa = Number(this.varejistaEscolhido);

    if (this.estoqueLicensa.qtdSeguranca === null || this.estoqueLicensa.qtdSeguranca === undefined) {
      this.modalMensagem('Por favor preencher o campo Estoque de Segurança!');
      return;
    }
    if (this.estoqueLicensa.incrementoCompra === null || this.estoqueLicensa.incrementoCompra === undefined) {
      this.modalMensagem('Por favor preencher o campo Incremento de Conta!');
      return;
    }
    if (this.estoqueLicensa.idempresa === null || this.estoqueLicensa.idempresa === undefined) {
      this.modalMensagem('Por favor escolha um varejista para esta ação!');
      return;
    }
    if (this.estoqueLicensa.qtdSeguranca === 0) {
      this.modalMensagem('Estoque de segurança deve ser maior que 0!');
      return;
    }
    if (this.estoqueLicensa.incrementoCompra === 0) {
      this.modalMensagem('Incremento de Conta deve ser maior que 0!');
      return;
    }
    if (this.estoqueLicensa.idempresa === 0) {
      this.modalMensagem('Escolha um varejista!');
      return;
    }

    this.loader.show();
    try {
      this.listEstoqueLicensa[0].incrementoCompra = this.estoqueLicensa.incrementoCompra;
      this.listEstoqueLicensa[0].qtdSeguranca = this.estoqueLicensa.qtdSeguranca;
      this.listEstoqueLicensa[0].idempresa = Number(this.varejistaEscolhido);

      const response = await this.bancoService.postEstoqueLicencas(this.listEstoqueLicensa[0]);
      if (response.status === 200) {
        this.modalMensagem('Dados alterados com sucesso!');
        this.estoqueLicensa.qtdSeguranca = 0;
        this.estoqueLicensa.incrementoCompra = 0;
        this.estoqueLicensa.idempresa = 0;
      }
      this.loader.hide();
    } catch (error) {

      const msg = JSON.parse(error._body);
      this.modalMensagem(msg.message);
      this.loader.hide();
    }
  }

  demandNewLicenses = async () => {
    if (this.desiredLicenses === 0) {
      this.modalMensagem('Quantidade de licenças deve ser maior que 0.');
      return;
    }

    try {
      await this.slmService.updateQuantity({licenseKey: 'KLM08-2DZCV-DFCP4-LQR3K-CQAO5-Z2R67',
                                            productInfo: [{serviceToBeProvisioned: 'MI-OSKCGM1WW-T',
                                            numberOfLicenses: this.desiredLicenses}]
                                          });
      this.modalMensagem('Solicitação de licenças enviada com sucesso.');
    } catch (error) {

      this.modalMensagem('Erro ao tentar aumentar a quantidade de licenças.');
    }
  }

  // filterByInputVarejista = () => {
  //   if (this.searchVarejista === '') {
  //     this.listaLinxResellers = this.listaAux;
  //   } else {
  //     const filterVarejista = this.listaAux.filter((item: LinxResellers) => {
  //        return item.nome.includes(this.searchVarejista);
  //     });
  //     this.listaLinxResellers = filterVarejista;
  //   }
  // }

  filterByInputEmail = () => {
    if (this.searchEmailList === '') {
      this.lisCadastroEmails = this.lisCadastroEmailsTodos;
    } else {
      this.lisCadastroEmails = (this.lisCadastroEmailsTodos.filter(x => x.email.match(new RegExp( '' + this.searchEmailList + '*', 'g')) !== null).length > 0) ? this.lisCadastroEmailsTodos.filter( x => x.email.match(new RegExp('' + this.searchEmailList + '*', 'g')) !== null) : new Array<cadastroEmails>();
    }
  }

  removeEmail = async (prEmail: cadastroEmails) => {
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: {escolha: false, mensagem: 'Deseja realmente deletar esse email?'}
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
          this.loader.show();
          await this.deleteItem(Number(prEmail.id));
          await this.fillCadastroEmail();
          this.loader.hide();
        } catch (error) {
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar excluir um item da Email.');
        }
      }
    });
  }


  deleteItem = async (id: Number) => {
    try {
      await this.bancoService.deleteCadastroEmail(Number(id));
      this.modalMensagem('Email deletado com sucesso!');
    } catch (error) {
      this.modalMensagem('Ocorreu um erro ao tentar deletar o email.');
    }
  }

}
