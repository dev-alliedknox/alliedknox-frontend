import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'grdFilter'
})
export class GrdFilterPipe implements PipeTransform {
  transform(items: any, filter: any, defaultFilter: boolean): any {
    console.log(items);
    console.log(filter);
    console.log(defaultFilter);
    if (!filter){
      console.log('primeiro if')
      return items;
    }

    if (!Array.isArray(items)){
      console.log('segundo if')
      return items;
    }

    if (filter && Array.isArray(items)) {
      console.log('terceiro if');
      let filterKeys = Object.keys(filter);
      console.log(filterKeys);
      if (defaultFilter) {
        console.log('quarto if');
        console.log(defaultFilter);
        return items.filter(item =>
            filterKeys.reduce((x, keyName) =>
                (x && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] == "", true));
      }
      else {
        console.log('Else');
        console.log(items);
        return items.filter(item => {
          return filterKeys.some((keyName) => {
            console.log(keyName);
            return new RegExp(filter[keyName], 'gi').test(item[keyName]) || filter[keyName] == "";
          });
        });
      }
    }
  }
}