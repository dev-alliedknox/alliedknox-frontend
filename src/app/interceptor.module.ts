// import { Injectable, NgModule } from '@angular/core';
// import { Observable } from 'rxjs';
// import {
//  HttpEvent,
//  HttpInterceptor,
//  HttpHandler,
//  HttpRequest,
// } from '@angular/common/http';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { CookieService } from 'ngx-cookie-service';
// import { MensagemComponent } from '../app/modal/mensagem/mensagem.component';
// import { MatDialog } from '@angular/material';
// import { Router } from '@angular/router';

// @Injectable()

// export class HttpsRequestInterceptor implements HttpInterceptor {

//   constructor(private cookieService: CookieService, private dialog: MatDialog, private router: Router) {}

//   modalMensagem = mensagem => {
//     const dialogRef = this.dialog.open(MensagemComponent, {
//       width: '400px',
//       height: '200px',
//       data: {mensagem: mensagem}
//     });
//   }

//  intercept(
//   req: HttpRequest<any>,
//   next: HttpHandler,
//  ): Observable<HttpEvent<any>> {
//     console.log('PASSOU INTERCEPTOR');
//     if ( !this.cookieService.get('accessToken') ) {
//       this.modalMensagem('Seu tempo de sessão esgotou, favor logar novamente!');
//       this.router.navigate(['/login']);
//     }
//     return;
//  }
// }

// @NgModule({
// providers: [
//  {
//   provide: HTTP_INTERCEPTORS,
//   useClass: HttpsRequestInterceptor,
//   multi: true,
//  },
// ],
// })


// export class Interceptor {};