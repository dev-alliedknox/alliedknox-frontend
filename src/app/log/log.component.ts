import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { UsuariosModalComponent } from '../modal/usuarios-modal/usuarios-modal.component';
import { IncluirUsuarioComponent } from '../modal/incluir-usuario/incluir-usuario.component';
import { IncluirPerfilComponent } from '../modal/incluir-perfil/incluir-perfil.component';
import { IncluirGrupoComponent } from '../modal/incluir-grupo/incluir-grupo.component';
import { User } from '../../class/user';
import { AuthorizationService } from '../../services/authorization.service';
import { Group } from 'src/class/group';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { EscolhaComponent } from '../modal/escolha/escolha.component';
import { LoaderService } from '../loading/loader.service';
import { EscolhaService } from '../modal/escolha/escolha.service';
import { EditarGrupoComponent } from '../modal/editar-grupo/editar-grupo.component';
import { treatError } from 'src/utils/error-utils';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  public imeiModel: string = '';
  public nomeModel: string = '';
  public statusModel = {id: 0, name: ''};
  public statusData = [
    {id: 0, name: 'Ativo'},
    {id: 1, name: 'Pendente Ativação'},
    {id: 2, name: 'Inativo'}
  ];
  public tipoModel = {};
  public data: any;
  public listUser: Array<User> = new Array<User>();
  public listFindUser: Array<User> = new Array<User>();
  public userByName: string = '';
  public profileName = '';
  public listGrupo: Array<Group> = new Array<Group>();
  public listFindGrupo: Array<Group> = new Array<Group>();
  public grupoByName: string = '';

  public profiles: Array<any> = new Array<any>();
  public profilesAux: Array<any> = new Array<any>();

  constructor(public dialog: MatDialog, public authorizationService: AuthorizationService,
    private escolhaService: EscolhaService,
    private loader: LoaderService) {}

  editarGrupo(groupId: number): void {
    const dialogRef = this.dialog.open(EditarGrupoComponent, {
      data: {groupId: groupId},
      width: '750px',
      height: '500px'
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.listGrupos();
    });
  }

  async ngOnInit() {
    this.loader.show();
    await Promise.all([ this.listUsers(), this.listGrupos(), this.listProfiles() ]);
    this.loader.hide();
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  incluirUsuario = () => {
    const dialogRef = this.dialog.open(IncluirUsuarioComponent, {
      data: {user: new User(), editing: false},
      width: '800px',
      height: '500px',
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.listUsers();
    });
  }

  openUpdateUser = (user) => {
    const dialogRef = this.dialog.open(IncluirUsuarioComponent, {
      data: { user: user, editing: true },
      width: '800px',
      height: '500px',
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.listUsers();
    });
  }

  filterUser = async () => {
    const filterUser = this.listFindUser.filter((item: User) => {
      return item.name.toLowerCase().includes(this.userByName.trim().toLowerCase());
    });
    this.listUser = filterUser;
  }

  deleteUser = async (id: number) => {
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: {escolha: false, mensagem: 'Deseja realmente deletar esse usuário?'}
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
            this.loader.show();
            const deleteUser = await this.authorizationService.deleteUser(id);
            await this.listUsers();
            this.modalMensagem('Usuário Deletado com Sucesso');
            this.loader.hide();
            return deleteUser;
        } catch (err) {
          console.log(err);
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar excluir usuário.');
        }
      }
    });
  }

  listUsers = async () => {
    try {
      this.listUser = await this.authorizationService.listUsers();
      this.listFindUser = this.listUser;
      for (let index in this.listUser) {
        const isTrue = this.listUser[index].isActive ? 1 : 0
        isTrue == 1 ? this.listUser[index].isActiveLabel = 'Ativo' : 
          this.listUser[index].isActiveLabel = 'Inativo'
      }
    } catch (error) {
      console.log(error);
    }
  }

  incluirPerfil = () => {
    const dialogRef = this.dialog.open(IncluirPerfilComponent, {
      data: { profile: {}, editing: false },
      width: '800px',
      height: '500px',
    });

    dialogRef.afterClosed().subscribe(async result => await this.listProfiles());
  }

  incluirGrupo = () => {
    const dialogRef = this.dialog.open(IncluirGrupoComponent, {
      width: '800px',
      height: '500px',
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.listGrupos();
    });
  }

  findGrupo = async () => {
    const filterGrupo = this.listFindGrupo.filter((item: Group) => {
      return item.name.includes(this.grupoByName);
    });
    this.listGrupo = filterGrupo;
  }

  deleteGrupo = async (id: number) => {
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: {escolha: false, mensagem: 'Deseja realmente deletar esse grupo?'}
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
            this.loader.show();
            let users = await this.authorizationService.listUsersByGrupo(id);
            // console.log('users: ', users);
            if (users.length > 0) {
              this.modalMensagem('Para deletar um grupo é necessário desassociar os usuários antes.');
              this.loader.hide();
            } else {
              const deleteGrupo = await this.authorizationService.deleteGrupo(id);
              await this.listGrupos();
              this.modalMensagem('Grupo Deletado com Sucesso');
              this.loader.hide();
              return deleteGrupo;
            }
        } catch (err) {
          console.log(err);
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar excluir grupo.');
        }
      }
    });
  }

  listGrupos = async () => {
    // try {
      this.listGrupo = await this.authorizationService.listGrupos();
      this.listFindGrupo = this.listGrupo;
    // } catch (error) {
    //   console.log(error);
    // }
  }

  async listProfiles() {

    this.profiles = await this.authorizationService.listProfiles();
    this.profilesAux = this.profiles;
  }

  async deleteProfile(id: number) {

    const dialogRef = this.escolhaService.openDialogMensagem();

    dialogRef.afterClosed().subscribe(async (result: any) => {

      if (result.escolha) {

        this.loader.show();

        await this.authorizationService.deleteProfile(id)
          .then((resp) => this.modalMensagem('Perfil deletado com sucesso'))
          .catch((err) => treatError(err, this.dialog, this.loader));

        await this.listProfiles().catch((err) => treatError(err, this.dialog, this.loader));
        this.loader.hide();
      }
    });
  }

  filterProfile(): void {

    this.profiles = this.profilesAux.filter((item) => item.name.toLowerCase().includes(this.profileName.trim().toLowerCase()));
  }

  openDialogUpdateProfile(profile) {

    const dialogRef = this.dialog.open(IncluirPerfilComponent, {
      data: { profile: profile, editing: true },
      width: '800px',
      height: '500px',
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.listProfiles();
    });
  }
}
