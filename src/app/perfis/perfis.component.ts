import { Component, OnInit } from '@angular/core';
import { WhitelistComponent } from '../modal/whitelist/whitelist.component';
import { BancoService } from 'src/services/banco.service';
import { MatDialog } from '@angular/material';
import { serialWhitelist } from 'src/class/serialWhitelist';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { LoaderService } from '../loading/loader.service';
import { EscolhaComponent } from '../modal/escolha/escolha.component';
import { listaBlinkados, filterDateBlinkados } from 'src/class/lista-blinkados';
import { listaAtivos, filterDateAtivos, } from 'src/class/lista-ativos';
import { listaBloqueados, filterDateBloqueados } from 'src/class/lista-bloqueados';
import { listaQuitados, filterDateQuitados } from 'src/class/lista-quitados';
import { UnlockDevices } from 'src/class/unlock-devices';
import { CrmService } from 'src/services/crm.service';
import { DesbloqueioPinComponent } from '../modal/desbloqueio-pin/desbloqueio-pin.component';
import { CookieService } from 'ngx-cookie-service';
import { listaImeiManual, filterDateImeiManual } from 'src/class/lista-imeiManual';
import { isNullOrUndefined, isNull } from 'util';


@Component({
  selector: 'app-perfis',
  templateUrl: './perfis.component.html',
  styleUrls: ['./perfis.component.css']
})
export class PerfisComponent implements OnInit {
  flagOrderByDtHrBloqueados: boolean = false;
  whiteList: serialWhitelist = new serialWhitelist();
  apiWhiteList: Array<serialWhitelist> = new Array<serialWhitelist>();
  listWhiteList: Array<serialWhitelist> = new Array<serialWhitelist>();
  listWhiteTodos: Array<serialWhitelist> = new Array<serialWhitelist>();
  searchWiteList: string = '';
  pattern = /\d+/g;
  patternNumberLetter = /\[d+a-zA-Z]+/g;
  radioIMEICPF: number = 1;
  unlockDevice: UnlockDevices = new UnlockDevices();
  varejistaOption: [{}];
  varejistaEscolhido = 0;
  flagOrderByAtivos: boolean = false
  flagOrderByDtHrBlinq: boolean = false
  user : string = '';
  cpf: string = '';
  imei: string = '';
  imeiPerfil: string = 'imeis';
  userActivate: string = '';
  cpfActivate: string = '';
  listBloqueados: Array<listaBloqueados> = new Array<listaBloqueados>();
  listBlinkados: Array<listaBlinkados> = new Array<listaBlinkados>();
  listBlinkadosAux: Array<listaBlinkados> = new Array<listaBlinkados>();

  listAtivos: Array<listaAtivos> = new Array<listaAtivos>();
  listAtivosAux: Array<listaAtivos> = new Array<listaAtivos>();
  listQuitados: Array<listaQuitados> = new Array<listaQuitados>();
  listImeiManual: Array<listaImeiManual> = new Array<listaImeiManual>();

  currentPageListaBloqueados: number = 1;
  dropdownPagesListaBloqueados = [{ itemsPerPage: 10 }, { itemsPerPage: 25 }, { itemsPerPage: 50 }];
  currentItemsPerPageListaBloqueados = this.dropdownPagesListaBloqueados[0].itemsPerPage;

  currentPageListaBlinkados: number = 1;
  dropdownPagesListaBlinkados = [{ itemsPerPage: 10 }, { itemsPerPage: 25 }, { itemsPerPage: 50 }];
  currentItemsPerPageListaBlinkados = this.dropdownPagesListaBlinkados[0].itemsPerPage;

  currentPageListaAtivos: number = 1;
  dropdownPagesListaAtivos = [{ itemsPerPage: 10 }, { itemsPerPage: 25 }, { itemsPerPage: 50 }];
  currentItemsPerPageListaAtivos = this.dropdownPagesListaAtivos[0].itemsPerPage;

  currentPageListaQuitados: number = 1;
  dropdownPagesListaQuitados = [{ itemsPerPage: 10 }, { itemsPerPage: 25 }, { itemsPerPage: 50 }];
  currentItemsPerPageListaQuitados = this.dropdownPagesListaQuitados[0].itemsPerPage;

  currentPageListaImeiManual: number = 1;
  dropdownPagesListaImeiManual = [{ itemsPerPage: 10 }, { itemsPerPage: 25 }, { itemsPerPage: 50 }];
  currentItemsPerPageListaImeiManual = this.dropdownPagesListaImeiManual[0].itemsPerPage;


  constructor(private crm: CrmService, private bancoService: BancoService, private dialog: MatDialog, private loader: LoaderService) { }

  async ngOnInit() {
    this.loader.show();
    // await this.fillWhiteList();
    // await this.fillBloqueados();
    // await this.fillBlinkados();
    // await this.fillAtivos();
    // await this.fillQuitados();
    await this.getAllLinxResellers();
    this.loader.hide();
  }

  modalWhitelist = () => {
    const dialogRef = this.dialog.open(WhitelistComponent, {
      width: '750px',
      height: '500px',
    });
    this.loader.show();
  }

  // onlyNumberIMEICPF = () => {
  //   if (String(this.whiteList.serialNumber) !== "") {
  //     var newValue = String(this.whiteList.serialNumber).match(this.pattern)[0];
  //     this.whiteList.serialNumber = newValue;
  //   } else {
  //     this.whiteList.serialNumber = '0';
  //   }
  // }

  // radioChange = ($event) => {
  //   (this.radioIMEICPF === 1) ? this.radioIMEICPF = 0 : this.radioIMEICPF = 1;
  // }

  getAllLinxResellers = async () => {
    const result = await this.bancoService.getAllLinxResellers();
    // console.log('varejistas: ', result);
    const varejistas = JSON.parse(result._body);
    // console.log('varejistas2: ', varejistas);
    this.varejistaOption = varejistas.map((item) => 
        { return {ID: item.idempresa, Nome: item.nome_fantasia }; });
    // console.log('Option: ', this.varejistaOption);
  }


  validateIMEICPF = () => {
    console.log(this.radioIMEICPF);
    if (this.whiteList.serialNumber.length === 0 || this.whiteList.serialNumber === '') {
      this.modalMensagem('Campo CPF ou IMEI e obrigatorio.');
      return;
    }

    if (this.whiteList.serialNumber.length < 11) {
      this.modalMensagem('Digite um CPF ou IMEI valido.');
      return;
    }

    if (this.apiWhiteList.filter(x => Number(x.serialNumber) === Number(this.whiteList.serialNumber)).length > 0) {
      this.modalMensagem('IMEI ou CPF já foi cadastrado.');
      return;
    }

    if (this.radioIMEICPF === 1 && (this.listWhiteList.length > 0 || this.listWhiteList.length === 0)) {
      this.whiteList.ativo = 1;
      this.listWhiteList.push(this.whiteList);
      this.whiteList = new serialWhitelist();
      return;
    }

    if (this.radioIMEICPF === 0 && this.listWhiteList.length === 0) {
      this.whiteList.ativo = 1;
      this.listWhiteList.push(this.whiteList);
      this.whiteList = new serialWhitelist();
      return;
    }
    this.modalMensagem('Só é possivel adicionar um por vez!');

  }

  editBloqueados = (prBloqueados) => {
    const dialogRef = this.dialog.open(DesbloqueioPinComponent, {
      width: '750px',
      height: '500px',
      data: prBloqueados
    })
  }

  editBlinkados = (prBlinkados) => {
    const dialogRef = this.dialog.open(DesbloqueioPinComponent, {
      width: '750px',
      height: '500px',
      data: prBlinkados
    })
  }

  fillWhiteList = async () => {
    try {
      var response = await this.bancoService.getAllSerialWhitelist();
      console.log(response);
      this.listWhiteList = response.json();
      this.listWhiteTodos = response.json();
      this.apiWhiteList = response.json();
    } catch (error) {
      console.log(error);
    }
  }

  insertWhiteList = async () => {
     // console.log('varejistaEscolhido: ', this.varejistaEscolhido);
    this.whiteList.idempresa = Number(this.varejistaEscolhido);
     // console.log('whiteList: ', this.whiteList);
    if (this.whiteList.serialNumber.length >= 12) {
      try {
        if (this.whiteList.idempresa === 0) {
          this.modalMensagem('Selecione um varejista!');
          this.loader.hide();
        } else {
          this.loader.show();
          await this.bancoService.postSerialWhitelist(this.whiteList);
          this.modalMensagem('IMEI inserido com sucesso!');
          this.listWhiteList.push(this.whiteList);
          this.loader.hide();
          this.whiteList = new serialWhitelist();
        }
      } catch (error) {
        console.log(error);
        this.loader.hide();
        this.modalMensagem('Ocorreu um Erro ao tentar adicionar um IMEI.');
      }
    } else {
      this.modalMensagem('Imei deve ter pelo menos 12 caracteres.');
    }
  }

  removeWhiteList = async (prWhiteList: serialWhitelist) => {
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: { escolha: false }
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
          this.loader.show();
          await this.deleteItem(Number(prWhiteList.id));
          await this.fillWhiteList();
          this.loader.hide();
        } catch (error) {
          console.log(error);
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar excluir um item da whitelist.');
        }
      }
    });
  }

  deleteItem = async (id: Number) => {
    try {
      var response = await this.bancoService.deleteWhiteList(Number(id));
      console.log(response);
      this.modalMensagem('Item deletado com sucesso!');
    } catch (error) {
      console.log(error);
      this.modalMensagem('Ocorreu um erro ao tentar deletar o item.');
    }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: { mensagem: mensagem }
    });
  }

  filterByInputWhiteList = () => {
    if (this.searchWiteList === '') {
      this.listWhiteList = this.listWhiteTodos;
    } else {
      this.listWhiteList = (this.listWhiteTodos.filter(x => x.serialNumber.match(new RegExp('' + this.searchWiteList + '*', 'g')) !== null).length > 0) ? this.listWhiteTodos.filter(x => x.serialNumber.match(new RegExp('' + this.searchWiteList + '*', 'g')) !== null) : new Array<serialWhitelist>();
    }
  }

  fillBloqueados = async () => {
    try {
      var responseBloqueados = await this.bancoService.getListaBloqueados();
      // console.log("Lista BLOQUEADOS", responseBloqueados);
      this.listBloqueados = responseBloqueados.json();
      this.listBloqueados = filterDateBloqueados(this.listBloqueados);
    } catch (error) {
      console.log(error);
    }
  }

  fillBlinkados = async () => {
    try {
      var responseBlinkados = await this.bancoService.getListaBlinkados();
      // console.log("Lista BLINK", responseBlinkados);
      this.listBlinkados = responseBlinkados.json();
      this.listBlinkadosAux = this.listBlinkados;
      this.listBlinkados = filterDateBlinkados(this.listBlinkados);
      this.listBlinkadosAux = filterDateBlinkados(this.listBlinkadosAux)
      this.listBlinkados.forEach(x => {
        if(x.dthora){
          var [day, month , year] = x.dthora.split(' ')[0].split('/');
          var [hour, minute , second] = x.dthora.split(' ')[1].split(':');
        }
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
      })
      this.listBlinkadosAux.forEach(x => {
        if(x.dthora){
          var [day, month , year] = x.dthora.split(' ')[0].split('/');
          var [hour, minute , second] = x.dthora.split(' ')[1].split(':');
        }
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
      })
    } catch (error) {
      console.log(error);
    }
  }

  filterAllBlinq = async () => {
    
    if(!this.userActivate && !this.cpfActivate &&
    this.imeiPerfil=='imeis'){
      this.listBlinkados = this.listBlinkadosAux;
    }
    

    if(this.userActivate!='' && this.cpfActivate === '' && this.imeiPerfil != 'imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) => {
        if(item.nome.includes(this.userActivate) && item.imei.includes(this.imeiPerfil)) {
          return item;
        }
      })
      this.listBlinkados = filterBlinq
    }

    if(this.userActivate!='' && this.cpfActivate!='' && this.imeiPerfil == 'imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) => {
        if(item.nome.includes(this.userActivate) && item.cpf.includes(this.cpfActivate)){
          return item
        }
      })
      this.listBlinkados = filterBlinq
    }
    
    if(this.userActivate !='' && this.cpfActivate === '' && this.imeiPerfil === 'imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) => {
        if(item.nome.includes(this.userActivate)){
          return item
        }
      })
      this.listBlinkados = filterBlinq
    }

    if(this.userActivate === '' && this.cpfActivate!='' && this.imeiPerfil == 'imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) => {
        if(item.cpf.includes(this.cpfActivate)){
          return item
        }
      })
      this.listBlinkados = filterBlinq
    }

    if(this.userActivate === '' && this.cpfActivate!='' && this.imeiPerfil !='imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) =>{
        if(item.cpf.includes(this.cpfActivate) && item.imei.includes(this.imeiPerfil)){
          return item;
        }
      })
      this.listBlinkados = filterBlinq
    }

    if(this.userActivate === '' && this.cpfActivate === '' &&
      this.imeiPerfil !='imeis'){
        const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) =>{
          if(item.imei.includes(this.imeiPerfil)){
            return item;
          }
        })
        this.listBlinkados = filterBlinq
    }

    if(this.userActivate!='' && this.cpfActivate!='' && this.imeiPerfil != 'imeis'){
      const filterBlinq = this.listBlinkadosAux.filter((item:listaBlinkados) => {
        if(item.nome.includes(this.userActivate) && item.cpf.includes(this.cpfActivate) 
        && item.imei.includes(this.imeiPerfil)){
          return item;
        }
      })
      this.listBlinkados = filterBlinq
    }
    
  }

  fillAtivos = async () => {
    try {
      var responseAtivos = await this.bancoService.getListaAtivos();
      // console.log("Lista ATIVOS", responseAtivos);
      this.listAtivos = responseAtivos.json();
      this.listAtivosAux = responseAtivos.json();
      this.listAtivosAux = filterDateAtivos(this.listAtivosAux);
      this.listAtivos = filterDateAtivos(this.listAtivos);
      this.listAtivos.forEach(x => {
        if(x.dthora){
          var [day, month , year] = x.dthora.split(' ')[0].split('/');
          var [hour, minute , second] = x.dthora.split(' ')[1].split(':');
        }
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
      })
      this.listAtivosAux.forEach(x => {
        if(x.dthora){
          var [day, month , year] = x.dthora.split(' ')[0].split('/');
          var [hour, minute , second] = x.dthora.split(' ')[1].split(':');
        }
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
      })
    } catch (error) {
      console.log(error);
    }
  }

  orderByDtHr = () => {
    if (this.flagOrderByAtivos) {
      this.flagOrderByAtivos = false;
      this.listAtivos = this.listAtivos.sort((a, b) => {
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByAtivos = true;
      this.listAtivos = this.listAtivos.sort((a, b) => {
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }

  fillQuitados = async () => {
    try {
      var responseQuitados = await this.bancoService.getListaQuitados();
      // console.log("Lista QUITADOS", responseQuitados);
      this.listQuitados = responseQuitados.json();
      this.listQuitados = filterDateQuitados(this.listQuitados);
    } catch (error) {
      console.log(error);
    }
  }

  filterByInputCpf = () => {
    if(this.cpf === '') {
      this.listAtivos = this.listAtivosAux;
    } else {
      const filterCpf = this.listAtivosAux.filter((item: listaAtivos) =>{
         return item.cpf.includes(this.cpf)
      })
      this.listAtivos = filterCpf
    }
  }

  filterByInputUser = () => {
    if(this.user === '') {
      this.listAtivos = this.listAtivosAux;
    } else {
      const filterNameUser = this.listAtivosAux.filter((item: listaAtivos) =>{
         return item.nome.includes(this.user)
      })
      this.listAtivos = filterNameUser
    }
  }

  filterByInputImei = () =>{
    if(this.imei === '') {
      this.listAtivos = this.listAtivosAux;
    } else {
      const filterImeiUser = this.listAtivosAux.filter((item: listaAtivos) =>{
         return item.imei.includes(this.imei)
      })
      this.listAtivos = filterImeiUser
    }
  }

  orderByDtHrBlinq = () => {
    if (this.flagOrderByDtHrBlinq) {
      this.flagOrderByDtHrBlinq = false;
      this.listBlinkados = this.listBlinkados.sort((a, b) => {
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrBlinq = true;
      this.listBlinkados = this.listBlinkados.sort((a, b) => {
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }

  fillImeiManual = async () => {
    try {
      var responseImeiManual = await this.bancoService.getListaImeiManual();
      // console.log("Lista IMEIMANUAL", responseImeiManual);
      this.listImeiManual = responseImeiManual.json();
      this.listImeiManual = filterDateImeiManual(this.listImeiManual);
    } catch (error) {
      console.log(error);
    }
  }

  // orderByDtHLogNotificacoes = () => {
  //   if(this.flagOrderByDtHrBloqueados) {
  //     this.flagOrderByDtHrBloqueados = false;
  //     this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
  //       if(a.timeSpan.getTime() < b.timeSpan.getTime()) {
  //         return 1;
  //       }

  //       if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
  //         return -1;
  //       }

  //       return 0;
  //     });      
  //   } else {
  //     this.flagOrderByDtHrBloqueados = true;
  //     this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
  //       if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
  //         return 1;
  //       }

  //       if(a.timeSpan.getTime() < b.timeSpan.getTime()) {
  //         return -1;
  //       }

  //       return 0;
  //     });
  //   }
  // }

  filterByInputUserActivate = () =>{
    if(this.userActivate){
      this.listBlinkados = this.listBlinkadosAux;
    }else{
      const filterUser = this.listBlinkadosAux.filter((item: listaBlinkados) =>{
        return item.nome.includes(this.userActivate)
      })
      filterUser.length > 0 ?  this.listBlinkados = filterUser : this.listBlinkados = this.listBlinkadosAux;
    }
  }

  desbloqueioListaBloqueados = async (prUnlock: listaBloqueados) => {

    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: { escolha: false, mensagem: 'Deseja realmente desbloquear esse item?' }
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        console.log('entrou');
        try {
          this.loader.show();
          // await this.crm.postAccessToken();

          this.unlockDevice.imei = prUnlock.imei;
          this.unlockDevice.cpf = prUnlock.cpf;

          console.log('==unlockDevice=='); console.log(this.unlockDevice);

          var response = await this.crm.putUnlockDevices(this.unlockDevice);

          this.modalMensagem('Desbloqueio enviado com sucesso!');
          await this.fillBloqueados();
          this.loader.hide();
        } catch (error) {
          await this.fillBloqueados();
          this.modalMensagem('Desbloqueio enviado com sucesso.');
          this.loader.hide();
        }
      }
    });
  }

  filterByInputCpfActivate = () => {
    if(this.cpfActivate.toString()===""){
      this.listBlinkados = this.listBlinkadosAux;
    }else{
      const filterImei = this.listBlinkadosAux.filter((item:listaBlinkados) =>{
        return item.cpf.includes(this.cpfActivate)
      })
      filterImei.length > 0 ?  this.listBlinkados = filterImei : this.listBlinkados = this.listBlinkadosAux; 
    }
  }

  filterTable = () => {
    if(this.imeiPerfil.toString()==="imeis"){
      this.listBlinkados = this.listBlinkadosAux;
    }else{
      const filterImei = this.listBlinkadosAux.filter((item:listaBlinkados) =>{
        return item.imei.includes(this.imeiPerfil)
      })
      filterImei.length > 0 ?  this.listBlinkados = filterImei : this.listBlinkados = this.listBlinkadosAux;
    }
  }

  desbloqueioListaBlinkados = async (prUnlock: listaBlinkados) => {

    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: { escolha: false, mensagem: 'Deseja realmente desbloquear esse item?' }
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
          this.loader.show();
          // await this.crm.postAccessToken();

          this.unlockDevice.imei = prUnlock.imei;
          this.unlockDevice.cpf = prUnlock.cpf;

          console.log('==unlockDevice=='); console.log(this.unlockDevice);

          var response = await this.crm.putUnlockDevices(this.unlockDevice);

          this.modalMensagem('Desbloqueio enviado com sucesso!');
          await this.fillBlinkados();
          this.loader.hide();
        } catch (error) {
          await this.fillBlinkados();
          this.modalMensagem('Desbloqueio enviado com sucesso.');
          this.loader.hide();
        }
      }
    });
  }

}
