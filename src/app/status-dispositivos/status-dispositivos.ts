import { Component, OnInit } from '@angular/core';
import { BancoService } from '../../services/banco.service';
import { StatusDispositivos, filterDateTimeKDPStatusDispositivos, filterDateTimeKGStatusDispositivos } from 'src/class/statusDispositivos';
import { advanceActivatedRoute } from '@angular/router/src/router_state';
import { LoaderService } from '../loading/loader.service';
import * as moment from 'moment';
import * as localization from 'moment/locale/pt';
moment.locale('pt', localization);
import { transacaoVarejista } from 'src/class/transacaoVarejista';


@Component({
  selector: 'app-status-dispositivos',
  templateUrl: './status-dispositivos.component.html',
  styleUrls: ['./status-dispositivos.component.css']
})
export class StatusDispositivosComponent implements OnInit {
  minDate = moment();


  listaStatusDispositivos: Array<StatusDispositivos> = new Array<StatusDispositivos>();
  listaStatusDispositivosTodos: Array<StatusDispositivos> = new Array<StatusDispositivos>();

  listaTransacaovarejista: Array<StatusDispositivos> = new Array<StatusDispositivos>();
  listaforVarejista: Array<StatusDispositivos> = new Array<StatusDispositivos>();

  currentPageListaStatusDispositivos = 1;
  dropdownPagesListaStatusDispositivos = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaStatusDispositivos = this.dropdownPagesListaStatusDispositivos[0].itemsPerPage;

  // datepicker
  selected = {startDate: moment(), endDate: moment()};

  sVarejista = new transacaoVarejista();

  flagOrderByDtHrStatusDispositivosKDP = false;
  flagOrderByDtHrStatusDispositivosKG = false;
  flagOrderByDtHrStatusDispositivos = false;
  searchIMEI = '';

  comboBoxLogNotificacoesStatusKG = [{id: 0, name: 'Status KG'}, {id: 1, name: 'Pending'}, {id: 2, name: 'Rejected'}, {id: 3, name: 'Accepted'}, {id: 4, name: 'Enrolled'}, {id: 5, name: 'Exchanging'}, {id: 6, name: 'Resetting'}, {id: 7, name: 'Overdue'}, {id: 8, name: 'StartingReminder'}, {id: 9, name: 'StoppingReminder'}, {id: 10, name: 'Blinked'}, {id: 11, name: 'Locking'}, {id: 12, name: 'Locked'}, {id: 13, name: 'Unlocking'}, {id: 14, name: 'Completing'}, {id: 15, name: 'Completed'}, {id: 16, name: 'Desassociado'}];
  comboBoxLogNotificacaoStatusKGModel = this.comboBoxLogNotificacoesStatusKG[0];

  comboBoxLogNotificacoesStatusKDP = [{id: 0, name: 'Status KDP'}, {id: 1, name: 'Pending'}, {id: 2, name: 'Verified'}, {id: 3, name: 'MPTS error'}, {id: 3, name: 'Exchanging'}];
  comboBoxLogNotificacaoStatusKDPModel = this.comboBoxLogNotificacoesStatusKDP[0];


  constructor(private bancoService: BancoService, private loader: LoaderService) { }

  async ngOnInit() {
    try {
      await this.fillTransacaovarejista();
      this.loader.show();
      const responseStatusDispositivos = await this.bancoService.getStatusDispositivos();
      const statusDispositivos = responseStatusDispositivos.json();
      statusDispositivos.forEach((part) => this.fillStatusDispositivos(part));
      statusDispositivos.forEach((part) => this.fillStatusDispositivosTodos(part));
      // this.listaStatusDispositivos = filterDateTimeKDPStatusDispositivos(this.listaStatusDispositivos);
      // this.listaStatusDispositivos = filterDateTimeKGStatusDispositivos(this.listaStatusDispositivos);
      // this.listaStatusDispositivosTodos = filterDateTimeKDPStatusDispositivos(this.listaStatusDispositivosTodos);
      // this.listaStatusDispositivosTodos = filterDateTimeKGStatusDispositivos(this.listaStatusDispositivosTodos);
      this.orderByDtHStatusKGDispositivos();
      this.orderByDtHStatusKDPDispositivos();
      this.listaStatusDispositivosTodos = responseStatusDispositivos.json();
      this.loader.hide();
    } catch (error) {
      this.loader.hide();
    }
  }

  cleanDate = () => {
    this.selected = undefined;
    this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
  }

  filterByDate = event => {
    if (event.startDate && event.endDate) {
    const [yearStart, monthStart, dayStart] = event.startDate.format().split('T')[0].split('-');
    const [yearEnd, monthEnd, dayEnd] = event.endDate.format().split('T')[0].split('-');
    const dateStartPattern = new Date(Number(yearStart), Number(monthStart), Number(dayStart), 23, 60, 60);
    const dateEndPattern = new Date(Number(yearEnd), Number(monthEnd), Number(dayEnd), 23, 60, 60);

    const days = this.listaStatusDispositivosTodos.filter(x => x.timeSpanKG !== undefined && x.timeSpanKG !== null).filter(x => x.timeSpanKG.getTime() >= dateStartPattern.getTime() && x.timeSpanKG.getTime() < dateEndPattern.getTime());
    this.listaStatusDispositivos = days;
    } else {
    this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
    }
  }

  fillDateNow = (): string => {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = Number('0' + dd);
    }
    if (mm < 10) {
      mm = Number('0' + mm);
    }
   return String(yyyy + '-' + mm + '-' + dd);
  }

  fillStatusDispositivos = (prStatusDispositivos: StatusDispositivos) => {
    const statusDispositivos = new StatusDispositivos();
    statusDispositivos.id = prStatusDispositivos.id;
    statusDispositivos.sku = prStatusDispositivos.sku;
    statusDispositivos.nomeProduto = prStatusDispositivos.nomeProduto;
    statusDispositivos.loja = prStatusDispositivos.loja;
    statusDispositivos.notaFiscal = prStatusDispositivos.notaFiscal;
    statusDispositivos.codigoImei = prStatusDispositivos.codigoImei;
    statusDispositivos.statusKDP = prStatusDispositivos.statusKDP;
    statusDispositivos.dtHoraKDP = prStatusDispositivos.dtHoraKDP;
    statusDispositivos.statusKG = prStatusDispositivos.statusKG;
    statusDispositivos.dtHoraKG = prStatusDispositivos.dtHoraKG;
    statusDispositivos.timeSpanKDP = new Date();
    statusDispositivos.timeSpanKG = new Date();
    this.listaStatusDispositivos.push(statusDispositivos);
  }

  fillStatusDispositivosTodos = (prStatusDispositivos: StatusDispositivos) => {
    const statusDispositivos = new StatusDispositivos();
    statusDispositivos.id = prStatusDispositivos.id;
    statusDispositivos.sku = prStatusDispositivos.sku;
    statusDispositivos.nomeProduto = prStatusDispositivos.nomeProduto;
    statusDispositivos.loja = prStatusDispositivos.loja;
    statusDispositivos.notaFiscal = prStatusDispositivos.notaFiscal;
    statusDispositivos.codigoImei = prStatusDispositivos.codigoImei;
    statusDispositivos.statusKDP = prStatusDispositivos.statusKDP;
    statusDispositivos.dtHoraKDP = prStatusDispositivos.dtHoraKDP;
    statusDispositivos.statusKG = prStatusDispositivos.statusKG;
    statusDispositivos.dtHoraKG = prStatusDispositivos.dtHoraKG;
    statusDispositivos.timeSpanKDP = new Date();
    statusDispositivos.timeSpanKG = new Date();
    this.listaStatusDispositivosTodos.push(statusDispositivos);
  }


 filterByInputIMEI = () => {
   if (this.searchIMEI === '') {
     this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
   } else {
    const filterImei = this.listaStatusDispositivosTodos.filter((item: StatusDispositivos) => {
      return item.codigoImei.includes(this.searchIMEI);
    });
    this.listaStatusDispositivos = filterImei;
  }
 }

 filterTable = () => {
  if (this.sVarejista.toString() === 'varejistas') {
    return this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
  }
  const filteredItem = this.listaStatusDispositivosTodos.filter((item: StatusDispositivos) => {
    return item.loja === String(this.sVarejista);
  });
  this.listaStatusDispositivos = filteredItem;
 }

  filterBycomboBoxLogNotificacoesStatusKG = () => {
    if (this.comboBoxLogNotificacaoStatusKGModel.id === 0) {
      this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
    } else {
      this.listaStatusDispositivos = (this.listaStatusDispositivosTodos.filter( x => x.statusKG.match(new RegExp(this.comboBoxLogNotificacaoStatusKGModel.name + '$')) !== null).length > 0) ? this.listaStatusDispositivosTodos.filter( x => x.statusKG.match(new RegExp(this.comboBoxLogNotificacaoStatusKGModel.name + '$')) !== null) : new Array<StatusDispositivos>();
    }
  }


  filterBycomboBoxLogNotificacoesStatusKDP = () => {
    if (this.comboBoxLogNotificacaoStatusKDPModel.id === 0) {
      this.listaStatusDispositivos = this.listaStatusDispositivosTodos;
    } else {
      this.listaStatusDispositivos = (this.listaStatusDispositivosTodos.filter( x => x.statusKDP.match(new RegExp(this.comboBoxLogNotificacaoStatusKDPModel.name + '$')) !== null).length > 0) ? this.listaStatusDispositivosTodos.filter( x => x.statusKDP.match(new RegExp(this.comboBoxLogNotificacaoStatusKDPModel.name + '$')) !== null) : new Array<StatusDispositivos>();
    }
  }


  orderByDtHStatusKGDispositivos = () => {
    if (this.flagOrderByDtHrStatusDispositivosKG) {
      this.flagOrderByDtHrStatusDispositivosKG = false;
      this.listaStatusDispositivos = this.listaStatusDispositivos.sort((a, b) => {
        if (a.timeSpanKG.getTime() < b.timeSpanKG.getTime()) {
          return 1;
        }
        if (a.timeSpanKG.getTime() > b.timeSpanKG.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrStatusDispositivosKG = true;
      this.listaStatusDispositivos = this.listaStatusDispositivos.sort((a, b) => {
        if (a.timeSpanKG.getTime() > b.timeSpanKG.getTime()) {
          return 1;
        }
        if (a.timeSpanKG.getTime() < b.timeSpanKG.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }

  orderByDtHStatusKDPDispositivos = () => {
    if (this.flagOrderByDtHrStatusDispositivosKDP) {
      this.flagOrderByDtHrStatusDispositivosKDP = false;
      this.listaStatusDispositivos = this.listaStatusDispositivos.sort((a, b) => {
        if (a.timeSpanKDP.getTime() < b.timeSpanKDP.getTime()) {
          return 1;
        }
        if (a.timeSpanKDP.getTime() > b.timeSpanKDP.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrStatusDispositivosKDP = true;
      this.listaStatusDispositivos = this.listaStatusDispositivos.sort((a, b) => {
        if (a.timeSpanKDP.getTime() > b.timeSpanKDP.getTime()) {
          return 1;
        }
        if (a.timeSpanKDP.getTime() < b.timeSpanKDP.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }

  fillTransacaovarejista = async () => {
    try {
      const responseTransacaoVarejista = await this.bancoService.getAllTransacaoVarejista();
      const t = new transacaoVarejista();
      this.listaTransacaovarejista.push();
      this.listaTransacaovarejista = responseTransacaoVarejista.json();
    } catch (error) {
      console.log(error);
    }
  }

}
