import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusDispositivosComponent } from './status-dispositivos';

describe('StatusDispositivosComponent', () => {
  let component: StatusDispositivosComponent;
  let fixture: ComponentFixture<StatusDispositivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusDispositivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusDispositivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
