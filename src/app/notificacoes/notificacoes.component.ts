import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BancoService } from '../../services/banco.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { notificacaoMensagens, filterDateTime } from 'src/class/notificacaoMensagens';
import { logNotificacoes, filterDateTimeLogNotificacoes } from 'src/class/logNotificacoes';
import { IncluirMensagemComponent } from '../modal/incluir-mensagem/incluir-mensagem.component';
import { LoaderService } from '../loading/loader.service';
import { ListaMensagem } from 'src/class/listaMensagens';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { MessageDevice } from 'src/class/messageDevice';
import { MessageDeviceBlink } from 'src/class/messageDeviceBlink';
import { LockDevices } from 'src/class/lock-devices';
import { UnlockDevices } from 'src/class/unlock-devices';
import { ObjectPin } from 'src/class/objectPin';
import { CrmService } from 'src/services/crm.service';
import { historicoImei, filterDateTimeHistoricoImei } from '../../class/historicoImei';
import { isNullOrUndefined } from 'util';
import { CookieService } from 'ngx-cookie-service';
import * as moment from 'moment';


@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.component.html',
  styleUrls: ['./notificacoes.component.css']
})
export class NotificacoesComponent implements OnInit {
  // Tabela Mensagens
  listaNotificacaoMensagens: Array<notificacaoMensagens> = new Array<notificacaoMensagens>();

  listaNotificacaoMensagensTodos: Array<notificacaoMensagens> = new Array<notificacaoMensagens>();
  flagOrderByDtHrNotificacoesMensagens = false;
  currentPageListaNotificacaoMensagens = 1;
  dropdownPagesListaNotificacaoMensagens = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaNotificacaoMensagens = this.dropdownPagesListaNotificacaoMensagens[0].itemsPerPage;
  comboBoxNotificacaoMensagensTipo = [{id: 0, name: 'Tipo'}, {id: 1, name: 'Mensagem'}, {id: 2, name: 'Blink'}, {id: 3, name: 'Bloqueio'}];
  comboBoxNotificacaoMensagensTipoModel = this.comboBoxNotificacaoMensagensTipo[0];
  inputNotificacaoMensagensVarejista = '';
  inputMensagemNotificacaoMensagem = '';
  // Tabela Notificacoes
  listaLogNotificacoes: Array<logNotificacoes> = new Array<logNotificacoes>();
  flagOrderByDtHrLogNotificacoes = false;
  currentPageListaLogNotificacoes = 1;
  dropdownPagesListaLogNotificacoes = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaLogNotificacoes = this.dropdownPagesListaLogNotificacoes[0].itemsPerPage;

  listaHistoricoImei: Array<historicoImei> = new Array<historicoImei>();
  listaHistoricoImeiAux: Array<historicoImei> = new Array<historicoImei>();
  user  = '';
  acao  = '';
  currentPageHistoricoImei = 1;
  dropdownPagesHistoricoImei = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageHistoricoImei = this.dropdownPagesHistoricoImei[0].itemsPerPage;



  input = {imei: ''};
  imeiSelecionado = '';

  flagOrderByDataHistorico = false;

  listaMensagensTodos: Array<ListaMensagem> = new Array<ListaMensagem>();
  listaMensagens: Array<ListaMensagem> = new Array<ListaMensagem>();
  listaMensagensModelId = 0;
  status = [{ label: 'Ativo', value: 1 }, {  label: 'Inativo', value: 0 }];
  modelInterval = 14600;

  statusModel = 1;

  flagOrderByDtHrRegistro = false;
  flagOrderByDtHrAtualizacao = false;

  listMensagem = [{ id: 0, name: 'Mensagem', value: 'sendMessage' },
  { id: 1, name: 'Blink', value: 'blinkMessage' },
  { id: 2, name: 'Bloqueio', value: 'lockDevice' },
  { id: 3, name: 'Desbloqueio', value: 'unlockDevice' }];
  mensagemModel = this.listMensagem[0].id;

  radioIMEICPF: number;

  messageDevice: MessageDevice = new MessageDevice();
  messageDeviceBlink: MessageDeviceBlink = new MessageDeviceBlink();
  lockDevice: LockDevices = new LockDevices();
  unlockDevice: UnlockDevices = new UnlockDevices();
  objectPin: ObjectPin = new ObjectPin();

  listaImei: Array<string> = new Array<string>();
  imei = '';
  serial = '';
  passKey = '';
  telaPin = '';
  telaPin2 = '';
  isEmpty = false;
  flagMensagem = true;
  flagBlink = false;
  flagCPF = false;
  flagAndroid = true;
  androidOption = 'android9';


  radioBtnPesquisarPor = [{id: 1, name: 'IMEI/SN'}, {id: 0 , name: 'CPF'}];
  radioBtnPesquisarPorModel = this.radioBtnPesquisarPor[1].id;

 // datepicker
 selected = {startDate: moment(), endDate: moment()};

  pattern = /\d+/g;

  constructor(private crm: CrmService, private cookeService: CookieService, private bancoService: BancoService, private dialog: MatDialog, private loader: LoaderService) { }

  async ngOnInit() {
    this.loader.show();
      // var responseUsage = await this.slm.getUsage();
    this.fillLogNotificacoes();
    await this.fillNotificaoMensagem();
    this.fillListaMensagens();
    this.loader.hide();
    this.orderByRegistro();
    this.orderByAtualizacao();
  }

  fillListaMensagens = async () => {
    try {
      const response = await this.bancoService.getAllListaMensagens();
      this.listaMensagensTodos = response.json();
      this.listaMensagensTodos = this.listaMensagensTodos.filter(x => x.Ativo !== '0');
      this.listaMensagensTodos.filter(x => x.mensagem.length > 97).forEach(x => x.mensagem = x.mensagem.substr(0, 97) + '...');
      this.listaMensagens = response.json();
      this.onChangeTipoMensagem();
      this.listaMensagens = this.listaMensagens.filter(x => x.Ativo !== '0');
      this.listaMensagens.filter(x => x.mensagem.length > 97).forEach(x => x.mensagem = x.mensagem.substr(0, 97) + '...');
      this.listaMensagensModelId = this.listaMensagens[0].id;
    } catch (error) {
      this.modalMensagem('Ocorreu um erro ao buscar as mensagens');
    }
  }

  filterByStatus = () => {
    const filterStatus = this.listaNotificacaoMensagensTodos.filter((item) => {
      return item.status.includes(this.statusModel.toString());
    });
    this.listaNotificacaoMensagens = filterStatus;
  }

  handleIncluir = () => {
    const dialogRef = this.dialog.open(IncluirMensagemComponent, {
      width: '750px',
      height: '500px',
      data: new notificacaoMensagens()
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      this.loader.show();
      await this.fillListaMensagens();
      await this.fillNotificaoMensagem();
      this.loader.hide();
    });
  }

  edit = (prNotificacaoMensagens: notificacaoMensagens) => {
    const dialogRef = this.dialog.open(IncluirMensagemComponent, {
      width: '750px',
      height: '500px',
      data: prNotificacaoMensagens
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      this.loader.show();
      if(result === true){
        await this.fillListaMensagens();
        await this.fillNotificaoMensagem();
      }
      this.loader.hide();
    });
  }


  desabilitarMensagem = () => {
    this.flagMensagem = true;
    this.flagBlink = false;

    if (Number(this.mensagemModel) === 1) {
      this.flagBlink = true;
    }

    if (Number(this.mensagemModel) === 3) {
      this.flagMensagem = false;
    }

    this.onChangeTipoMensagem();
  }

  onChangeTipoMensagem = () => {
    // console.log(this.listMensagem);
    // console.log(this.mensagemModel);
    if (Number(this.mensagemModel) !== 3) {
    const newMensagens = new Array<ListaMensagem>();
    newMensagens.push( {id: 0,
      mensagem: 'Nenhuma mensagem encontrada',
      tipomensagem: '',
      // idlinxresellers: 0,
      idempresa: 0,
      DtAdd: '',
      DtUpd: '',
      Ativo: ''});
   this.listaMensagens = (this.listaMensagensTodos.filter( x => x.tipomensagem.match(new RegExp(this.listMensagem.filter(x => x.id === Number(this.mensagemModel))[0].name + '$')) !== undefined).length > 0) ? this.listaMensagensTodos.filter( x => x.tipomensagem.match(new RegExp(this.listMensagem.filter(x => x.id === Number(this.mensagemModel))[0].name + '$'))) : newMensagens;
    }
    this.listaMensagensModelId = this.listaMensagens[0].id;
    // console.log('idMensagem: ', this.listaMensagensModelId);
  }

  pesquisarCPF = () => {
    console.log(this.radioBtnPesquisarPorModel);
    this.flagCPF = false;
    if (Number(this.radioBtnPesquisarPorModel) === 1) {
      this.flagCPF = true;
    }
    console.log(this.flagCPF);
  }

  removeDevice = (prListaImei: string) => {
      console.log('=this.listaImei='); console.log(prListaImei);
    this.listaImei = this.listaImei.filter(item => item !== prListaImei);
    // this.imei = '';
  }

  mostraValorImei = () => {
    console.log('==radioIMEICPF== : ' + this.radioIMEICPF);
  }


  checkAndroid = () => {
    this.flagAndroid = true;
    if (this.androidOption === 'android9') {
      console.log('option: ', this.androidOption);
      this.flagAndroid = true;
    }
    if (this.androidOption === 'android8') {
      console.log('option: ', this.androidOption);
      this.flagAndroid = false;
    }
  }

  filterByInputAcao = () => {
    if (this.acao === '') {
      this.listaHistoricoImei = this.listaHistoricoImeiAux;
    } else {
      const filterAcao = this.listaHistoricoImeiAux.filter((item: historicoImei) => {
        return item.acao.includes(this.acao);
      });
      filterAcao.length > 0 ?  this.listaHistoricoImei = filterAcao : this.listaHistoricoImei = this.listaHistoricoImeiAux;
    }
  }

  filterByInputSerial = () => {
    if ( this.serial === '') {
      this.listaHistoricoImei = this.listaHistoricoImeiAux;
    } else {
      const filterSerial = this.listaHistoricoImeiAux.filter((item: historicoImei) => {
        return item.serial.includes(this.serial);
      });
      filterSerial.length > 0 ?  this.listaHistoricoImei = filterSerial : this.listaHistoricoImei = this.listaHistoricoImeiAux;
    }
  }

  filterByInputUser = () => {
    if (this.user === '') {
      this.listaHistoricoImei = this.listaHistoricoImeiAux;
    } else {
      const filterUser = this.listaHistoricoImeiAux.filter((item: historicoImei) => {
        return item.usuario.includes(this.user);
      });
      filterUser.length > 0 ?  this.listaHistoricoImei = filterUser : this.listaHistoricoImei = this.listaHistoricoImeiAux;
    }
  }

  filterByDate = event => {
    if (event.startDate && event.endDate) {
    const [yearStart, monthStart, dayStart] = event.startDate.format().split('T')[0].split('-');
    const [yearEnd, monthEnd, dayEnd] = event.endDate.format().split('T')[0].split('-');
    const dateStartPattern = new Date(Number(yearStart), Number(monthStart), Number(dayStart), 23, 60, 60);
    const dateEndPattern = new Date(Number(yearEnd), Number(monthEnd), Number(dayEnd), 23, 60, 60);

    const days = this.listaHistoricoImei.filter(x => x.timeSpan !== undefined && x.timeSpan !== null).filter(x => x.timeSpan.getTime() >= dateStartPattern.getTime() && x.timeSpan.getTime() < dateEndPattern.getTime());
    days.length > 0 ? this.listaHistoricoImei = days : this.listaHistoricoImei = this.listaHistoricoImeiAux;
    } else {
      this.listaHistoricoImei = this.listaHistoricoImeiAux;
    }
  }

  pesquisarHandler = async () => {
    if (!this.input.imei) {
      this.modalMensagem('Digite um IMEI/SN.');
      this.imeiSelecionado = '';
    } else {
        this.loader.show();
        try {
          const IDempresa = this.cookeService.get('idempresa');
          // console.log('idempresa: ', IDempresa);
          const responseImeiMsg = await this.bancoService.postImeiMensagens({imei: this.input.imei, idempresa: IDempresa});
          this.listaHistoricoImei = responseImeiMsg.json();
          this.listaHistoricoImeiAux = responseImeiMsg.json();

          if (this.listaHistoricoImei.length === 0) {
              this.isEmpty = true;
              this.imeiSelecionado = ``;
          } else {
            this.isEmpty = false;
            this.listaHistoricoImei = filterDateTimeHistoricoImei(this.listaHistoricoImei);
            this.input.imei.length === 11 ? this.imeiSelecionado = `CPF Selecionado: ${this.input.imei}` :
            this.imeiSelecionado = `IMEI Selecionado: ${this.input.imei}`;
            this.listaHistoricoImei.forEach(x => {
              if (x.dthora) {
                var [day, month , year] = x.dthora.split(' ')[0].split('/');
                var [hour, minute ] = x.dthora.split(' ')[1].split(':');
              }
              x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), );
            });
            this.listaHistoricoImeiAux.forEach(x => {
              if (x.dthora) {
                var [day, month , year] = x.dthora.split(' ')[0].split('/');
                var [hour, minute ] = x.dthora.split(' ')[1].split(':');
              }
              x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), );
            });
          }
          this.input.imei = '';
          this.loader.hide();
        } catch (error) {
          this.modalMensagem('Ocorreu um erro durante a consulta do histórico');
          this.loader.hide();
        }
    }
  }

  cleanDate = () => {
    this.selected = {startDate: moment(), endDate: moment()};
    this.listaHistoricoImei = this.listaHistoricoImeiAux;
  }

  orderByDtHrList = () => {
    if (this.flagOrderByDataHistorico) {
      this.flagOrderByDataHistorico = false;
      this.listaHistoricoImei = this.listaHistoricoImei.sort((a, b) => {

        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDataHistorico = true;
      this.listaHistoricoImei = this.listaHistoricoImei.sort((a, b) => {
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }

  validateIMEICPF = async () => {
    console.log(this.listaMensagensModelId);

    if (this.imei.length === 0 || this.imei === '') {
      this.modalMensagem('Campo CPF ou IMEI/SN e obrigatorio.');
      return;
    }
    if (this.imei.length < 11) {
      this.modalMensagem('Digite um CPF ou IMEI/SN valido.');
      return;
    }
    this.listaImei.push(this.imei);
    this.imei = '';
    return;
  }


  sendForUrlByTipo = async () => {
    if (isNullOrUndefined(this.radioIMEICPF)) {
      this.modalMensagem('Escolha um tipo de dispositivo (IMEI/SN ou CPF).');
      this.loader.hide();
      return;
    }

    if (this.listaImei.length === 0) {
      this.modalMensagem('Adicione um IMEI/CPF para poder mandar uma mensagem.');
    } else {

      this.loader.show();

      this.messageDevice.apiurl = this.listMensagem.filter(x => x.id == Number(this.mensagemModel))[0].value;

      this.messageDevice.cpf = '';
      this.messageDeviceBlink.cpf = '';
      this.lockDevice.cpf = '';
      this.unlockDevice.cpf = '';
      this.messageDevice.imei = '';
      this.messageDeviceBlink.imei = '';
      this.lockDevice.imei = '';
      this.unlockDevice.imei = '';
      if (this.radioIMEICPF == 1) {
        this.messageDevice.cpf = this.listaImei[0];
        this.messageDeviceBlink.cpf = this.listaImei[0];
        this.lockDevice.cpf = this.listaImei[0];
        this.unlockDevice.cpf = this.listaImei[0];
      } else {
        this.messageDevice.imei = this.listaImei[0];
        this.messageDeviceBlink.imei = this.listaImei[0];
        this.lockDevice.imei = this.listaImei[0];
        this.unlockDevice.imei = this.listaImei[0];
      }

      if (Number(this.mensagemModel) === 0) {

        try {
          // await this.crm.postAccessToken();
          this.messageDevice.messageId = Number(this.listaMensagensModelId);

          console.log('==messageDevice=='); console.log(this.messageDevice);

          let response = await this.crm.postSendMessage(this.messageDevice);
          response = JSON.parse(response._body);
          // console.log('RESPONSE: ', response);
          if (response.fail > 0) {
           // const count = response.count;
           // const success = response.success;
           // const fail = response.fail;
            const message = response.resultList[0].error.message;
            if (response.resultList[0].error.reason) {
              const reason = response.resultList[0].error.reason;
              this.modalMensagem(`Erro ao enviar mensagem: ${reason}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}` +
                                // `Motivo: ${reason}`);
            } else {
              this.modalMensagem(`Erro ao enviar mensagem: ${message}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}`);
            }
            this.loader.hide();
            return;
          } else {
            this.modalMensagem('Mensagem enviada com sucesso!');
            this.loader.hide();
          }
        } catch (error) {
          const msg = JSON.parse(error._body);
          this.modalMensagem('Erro ao enviar mensagem: ' + msg.message);
          // this.modalMensagem('Mensagem enviada com sucesso.');
          this.loader.hide();
          this.messageDevice.imei = '';
          this.messageDevice.cpf = '';
        }
        this.messageDevice.imei = '';
        this.messageDevice.cpf = '';
        return;
      }

      if (Number(this.mensagemModel)  === 1) {
        // await this.crm.postAccessToken();
        this.messageDeviceBlink.apiurl = this.messageDevice.apiurl;
        this.messageDeviceBlink.messageId = Number(this.listaMensagensModelId);

        if (this.modelInterval < 3 || this.modelInterval > 86400) {
          this.modalMensagem('Escolha um intervalo válido (entre 3 e 86400 segundos).');
          this.loader.hide();
          return;
        }

        this.messageDeviceBlink.blinkInterval = Number(this.modelInterval);

       console.log('==messageDeviceBlink=='); console.log(this.messageDeviceBlink);

        try {
          let response = await this.crm.postBlinkMessage(this.messageDeviceBlink);
          response = JSON.parse(response._body);
          // console.log('RESPONSE: ', response);
          if (response.fail > 0) {
           // const count = response.count;
           // const success = response.success;
           // const fail = response.fail;
            const message = response.resultList[0].error.message;
            if (response.resultList[0].error.reason) {
              const reason = response.resultList[0].error.reason;
              this.modalMensagem(`Erro ao enviar blink: ${reason}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}` +
                                // `Motivo: ${reason}`);
            } else {
              this.modalMensagem(`Erro ao enviar blink: ${message}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}`);
            }
            this.loader.hide();
            return;
          } else {
            this.modalMensagem('Blink enviado com sucesso!');
            this.loader.hide();
          }
        } catch (error) {
          const msg = JSON.parse(error._body);
          this.modalMensagem('Erro ao enviar blink: ' + msg.message);
          // this.modalMensagem('Blink enviado com sucesso.');
          this.loader.hide();
          this.messageDeviceBlink.imei = '';
          this.messageDeviceBlink.cpf = '';
        }
        this.messageDeviceBlink.imei = '';
        this.messageDeviceBlink.cpf = '';
        return;
      }

      if (Number(this.mensagemModel) === 2) {

        try {
          // await this.crm.postAccessToken();
          this.lockDevice.messageId = Number(this.listaMensagensModelId);

          console.log('==lockDevice=='); console.log(this.lockDevice);

          let response = await this.crm.putLockDevices(this.lockDevice);
          response = JSON.parse(response._body);
          // console.log('RESPONSE: ', response);
          if (response.fail > 0) {
           // const count = response.count;
           // const success = response.success;
           // const fail = response.fail;
            const message = response.resultList[0].error.message;
            if (response.resultList[0].error.reason) {
              const reason = response.resultList[0].error.reason;
              this.modalMensagem(`Erro ao bloquear dispositivo(s): ${reason}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}` +
                                // `Motivo: ${reason}`);
            } else {
              this.modalMensagem(`Erro ao bloquear dispositivo(s): ${message}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}`);
            }
            this.loader.hide();
            return;
          } else {
            this.modalMensagem('Bloqueio enviado com sucesso!');
            this.loader.hide();
          }
        } catch (error) {
          const msg = JSON.parse(error._body);
          this.modalMensagem('Erro ao enviar bloqueio: ' + msg.message);
          // this.modalMensagem('Bloqueio enviado com sucesso.');
          this.loader.hide();
          this.lockDevice.imei = '';
          this.lockDevice.cpf = '';
        }
        this.lockDevice.imei = '';
        this.lockDevice.cpf = '';
        return;
      }

      if (Number(this.mensagemModel) === 3) {

        try {
          // await this.crm.postAccessToken();
          console.log('==unlockDevice=='); console.log(this.unlockDevice);

          let response = await this.crm.putUnlockDevices(this.unlockDevice);
          response = JSON.parse(response._body);
          // console.log('RESPONSE: ', response);
          if (response.fail > 0) {
           // const count = response.count;
           // const success = response.success;
           // const fail = response.fail;
            const message = response.resultList[0].error.message;
            if (response.resultList[0].error.reason) {
              const reason = response.resultList[0].error.reason;
              this.modalMensagem(`Erro ao desbloquear dispositivo(s): ${reason}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}` +
                                // `Motivo: ${reason}`);
            } else {
              this.modalMensagem(`Erro ao desbloquear dispositivo(s): ${message}`);
                                // `Dispositivos: ${count}` +
                                // `Sucesso: ${success}` +
                                // `Erro: ${fail}` +
                                // `Mensagem: ${message}`);
            }
            this.loader.hide();
            return;
          } else {
            this.modalMensagem('Desbloqueio enviado com sucesso!');
            this.loader.hide();
          }
        } catch (error) {
          const msg = JSON.parse(error._body);
          this.modalMensagem('Erro ao enviar desbloqueio: ' + msg.message);
          // this.modalMensagem('Desbloqueio enviado com sucesso.');
          this.loader.hide();
          this.unlockDevice.imei = '';
          this.unlockDevice.cpf = '';
        }
        this.unlockDevice.imei = '';
        this.unlockDevice.cpf = '';
        return;
      }
      this.loader.hide();
    }
  }


  obterPin = async () => {
    if (this.serial === '') {
      this.modalMensagem('Adicione um IMEI bloqueado para resgatar o PIN.');
    } else {
      this.loader.show();

      this.objectPin.passKey = '';
      this.objectPin.serial = '';
      this.telaPin = '';
      this.telaPin2 = '';

      try {
        // await this.crm.postAccessToken();
        this.objectPin.serial = this.serial;
        this.objectPin.passKey = this.passKey;

        console.log('==objectPin=='); console.log(this.objectPin);

        let response = await this.crm.postGetPin(this.objectPin);
          console.log('response'); console.log(response);
        let result = response.json();

        this.telaPin  = 'IMEI/SN: ' + result.requestedId;
        this.telaPin2 = 'Chave de desbloqueio: ' + result.pinNumber[0];

        this.loader.hide();
      } catch (error) {
        console.log(error);
        const msg = JSON.parse(error._body);
        this.modalMensagem('Erro ao resgatar PIN: ' + msg.message);
        // this.modalMensagem('Erro ao resgatar chave PIN.');
        this.loader.hide();
      }
      this.objectPin.passKey = '';
      this.objectPin.serial = '';
      this.serial = '';
    }
  }


  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '250px',
      data: { mensagem: mensagem }
    });
  }


  fillNotificaoMensagem = async () => {
    try {
      let responseHistorico = await this.bancoService.getNotificacaoMensagens();
      this.listaNotificacaoMensagens = responseHistorico.json();
      this.listaNotificacaoMensagens = filterDateTime(this.listaNotificacaoMensagens);
      this.listaNotificacaoMensagens.forEach(x => {
        if (x.dtHr) {
          var [day, month , year] = x.dtHr.split(' ')[0].split('/');
          var [hour, minute , second] = x.dtHr.split(' ')[1].split(':');
        }

        if (x.dtUpd) {
          var [dayUpd, monthUpd , yearUpd] = x.dtUpd.split(' ')[0].split('/');
          var [hourUpd, minuteUpd , secondUpd] = x.dtUpd.split(' ')[1].split(':');
          x.timeSpanUpd = new Date(Number(yearUpd), Number(monthUpd), Number(dayUpd), Number(hourUpd), Number(minuteUpd), Number(secondUpd));
        }
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
      });
      this.listaNotificacaoMensagensTodos = responseHistorico.json();
      this.listaNotificacaoMensagensTodos.forEach(x => {
        if(x.dtHr){
        let [day, month , year] = x.dtHr.split(' ')[0].split('/');
        let [hour, minute , second] = x.dtHr.split(' ')[1].split(':');
        x.timeSpan = new Date(Number(year), Number(month), Number(day), Number(hour), Number(minute), Number(second));
        }
        if(x.dtUpd){
          var [dayUpd, monthUpd , yearUpd] = x.dtUpd.split(' ')[0].split('/');
          var [hourUpd, minuteUpd , secondUpd] = x.dtUpd.split(' ')[1].split(':');
          x.timeSpanUpd = new Date(Number(yearUpd), Number(monthUpd), Number(dayUpd), Number(hourUpd), Number(minuteUpd), Number(secondUpd));
        }
      });
      this.listaNotificacaoMensagensTodos = filterDateTime(this.listaNotificacaoMensagensTodos);
    } catch (error) {
      console.log(error);
    }
  }

  fillLogNotificacoes = async () => {
    try {
      const responseLogNotificacoes = await this.bancoService.getLogNotificacoes();
      this.listaLogNotificacoes = responseLogNotificacoes.json();
      this.listaLogNotificacoes = filterDateTimeLogNotificacoes(this.listaLogNotificacoes);
    } catch (error) {
      console.log(error);
    }
  }


  orderByDtHr = () => {
    if (this.flagOrderByDtHrNotificacoesMensagens) {
      this.flagOrderByDtHrNotificacoesMensagens = false;
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a, b) => {
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrNotificacoesMensagens = true;
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a, b) => {
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }


  orderByDtHLogNotificacoes = () => {
    if(this.flagOrderByDtHrLogNotificacoes) {
      this.flagOrderByDtHrLogNotificacoes = false;
      this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrLogNotificacoes = true;
      this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
        if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }


  filterByinputNotificacaoMensagensVarejista = () => {
    if(this.inputNotificacaoMensagensVarejista === '') {
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagensTodos;
    } else {
      this.listaNotificacaoMensagens = (this.listaNotificacaoMensagensTodos.filter(x => x.varejista.match(new RegExp('[' + this.inputNotificacaoMensagensVarejista + ']+$', 'gm')) !== null).length > 0) ? this.listaNotificacaoMensagensTodos.filter( x => x.varejista.match(new RegExp('[' + this.inputNotificacaoMensagensVarejista + ']+$', 'gm')) !== null) : new Array<notificacaoMensagens>();
    }
  }


  filterBycomboBoxNotificacaoMensagensTipo = () => {
    if(this.comboBoxNotificacaoMensagensTipoModel.id === 0) {
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagensTodos;  
    } else {
      this.listaNotificacaoMensagens = (this.listaNotificacaoMensagensTodos.filter( x => x.tipo.match(new RegExp(this.comboBoxNotificacaoMensagensTipoModel.name + '$')) !== null).length > 0) ? this.listaNotificacaoMensagensTodos.filter( x => x.tipo.match(new RegExp(this.comboBoxNotificacaoMensagensTipoModel.name + '$')) !== null) : new Array<notificacaoMensagens>();
    }
  }


  orderByRegistro = () => {
    if(this.flagOrderByDtHrRegistro) {
      this.flagOrderByDtHrRegistro = false;

      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a, b) => {

        if(a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return 1;
        }
        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    } else {
      this.flagOrderByDtHrRegistro = true;
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a, b) => {
        if(a.timeSpan.getTime() > b.timeSpan.getTime()) {
          return 1;
        }
        if(a.timeSpan.getTime() < b.timeSpan.getTime()) {
          return -1;
        }
        return 0;
      });
    }
  }


  orderByAtualizacao = () => {
    if(this.flagOrderByDtHrAtualizacao) {
      this.flagOrderByDtHrAtualizacao = false;
      // for(let i=0; i<this.listaNotificacaoMensagens.length; i++){
      //   this.listaNotificacaoMensagens[i].dtUpd == undefined ?
      //    this.listaNotificacaoMensagens[i].dtUpd =  null : this.listaNotificacaoMensagens  
      // }
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a,b) => {
        if(a.dtUpd&&!b.dtUpd){
          if(a.timeSpanUpd.getTime()){
            return -1
          }
        }
        if(!a.dtUpd&&b.dtUpd){
          if(b.timeSpanUpd.getTime()){
            return 1
          }
        }

        if(a.dtUpd && b.dtUpd){
          if(a.timeSpanUpd.getTime() < b.timeSpanUpd.getTime()) {
            return 1;
          }
          if(a.timeSpanUpd.getTime() > b.timeSpanUpd.getTime()) {
            return -1;
          }
          return 0;
        }
      });
    } else {
      
      this.flagOrderByDtHrAtualizacao = true;
      // for(let i=0; i<this.listaNotificacaoMensagens.length; i++){
      //   console.log('entrou no for ')
      //   this.listaNotificacaoMensagens[i].dtUpd == undefined ?
      //    this.listaNotificacaoMensagens[i].dtUpd =  '' : this.listaNotificacaoMensagens  
      // }
      this.listaNotificacaoMensagens = this.listaNotificacaoMensagens.sort((a,b) => {

        if(a.dtUpd&&!b.dtUpd){
          if(a.timeSpanUpd.getTime()){
            return 1
          }
        }
        if(!a.dtUpd&&b.dtUpd){
          if(b.timeSpanUpd.getTime()){
            return -1
          }
        }

        if(a.dtUpd && b.dtUpd){
          if(a.timeSpanUpd.getTime() > b.timeSpanUpd.getTime()) {
            return 1;
          }
          if(a.timeSpanUpd.getTime() < b.timeSpanUpd.getTime()) {
            return -1;
          }
          return 0;
        }
      });
    }
  }

}
