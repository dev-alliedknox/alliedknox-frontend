import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import {AuthorizationService} from '../../../services/authorization.service';
import { Group } from 'src/class/group';
import { User } from 'src/class/user';
import { filterDateBloqueados } from 'src/class/lista-bloqueados';

@Component({
  selector: 'app-incluir-grupo',
  templateUrl: './incluir-grupo.component.html',
  styleUrls: ['./incluir-grupo.component.css']
})
export class IncluirGrupoComponent implements OnInit {
  public name: string;
  public description: string;
  public newGrupo: Group = new Group();
  public listUser: Array<any> = new Array<any>();
  public listFindUser: Array<User> = new Array<User>();
  public groupUserList: Array<number> = new Array<number>();
  // public associado: boolean;

  constructor(
            public authorizationService: AuthorizationService,
            private service: BancoService,
            private loader: LoaderService,
            private dialog: MatDialog) { }

  async ngOnInit() {
    await this.listUsers();
   }

  Cancelar() {
    this.dialog.closeAll();
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }


  //createGrupo = async (groupUserList: number[]) => {
  createGrupo = async ( ) => {
    try {
      if (!this.newGrupo.name || !this.newGrupo.description ) {
        this.modalMensagem('Nome do grupo ou descrição estão em branco');
        return;
      }
      this.loader.show();
      const grupoNew = await this.authorizationService.createGrupo(this.newGrupo);
        console.log('grupoNew: '); console.log(grupoNew);
      const grupoId = JSON.parse(grupoNew._body).groupId;
        console.log('grupoId: '); console.log(grupoId);

      const payload = this.getAssociados(grupoId);

      this.authorizationService.associateUser(payload);

      this.modalMensagem('Grupo incluido com sucesso!');
      this.loader.hide();
      this.newGrupo.name = '';
      this.newGrupo.description = '';
      return grupoNew;
    } catch (error) {
      console.log(error);
      this.loader.hide();
      this.modalMensagem('Ocorreu um erro ao tentar adicionar grupo.');
    }
    // this.dialog.closeAll();
    // this.newGrupo.name = '';
    // this.newGrupo.description = '';
  }

  // modalMensagem = mensagem => {
  //   const dialogRef = this.dialog.open(MensagemComponent,{
  //     width: '400px',
  //     height: '200px',
  //     data: {mensagem: mensagem}
  //   });
  // }

  listUsers = async () => {
    try {
      this.listUser = await this.authorizationService.listUsers();
      // console.log('listUser: '); console.log(this.listUser);
      this.listFindUser = this.listUser;
    } catch (error) {
      console.log(error);
    }
  }

  getChecked() {
    return this.listUser.filter((user) => user.checked);
  }

  getAssociados(groupId: number) {
    const associados = this.getChecked();
    return associados.map((item) => {return { idUser: item.id, idGroup: groupId };
    });
  }


}
