import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirGrupoComponent } from './incluir-grupo.component';

describe('IncluirGrupoComponent', () => {
  let component: IncluirGrupoComponent;
  let fixture: ComponentFixture<IncluirGrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
