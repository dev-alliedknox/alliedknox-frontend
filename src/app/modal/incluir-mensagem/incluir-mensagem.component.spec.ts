import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirMensagemComponent } from './incluir-mensagem.component';

describe('IncluirMensagemComponent', () => {
  let component: IncluirMensagemComponent;
  let fixture: ComponentFixture<IncluirMensagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirMensagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirMensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
