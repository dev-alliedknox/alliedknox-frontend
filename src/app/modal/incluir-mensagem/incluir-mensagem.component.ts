import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { LoaderService } from 'src/app/loading/loader.service';
import { BancoService } from 'src/services/banco.service';
import { LinxResellers } from 'src/class/linxResellers';
import { TagsMensagem } from 'src/class/tagsMensagem';
import { ListaMensagem } from 'src/class/listaMensagens';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { UnionMensagensTags } from 'src/class/unionMensagensTags';
import { notificacaoMensagens } from 'src/class/notificacaoMensagens';

@Component({
  selector: 'app-incluir-mensagem',
  templateUrl: './incluir-mensagem.component.html',
  styleUrls: ['./incluir-mensagem.component.css']
})
export class IncluirMensagemComponent implements OnInit {

  listResellers: Array<LinxResellers> = new Array<LinxResellers>();
  resellersModel: number = 0;

  editable: boolean = false;
  result: boolean = false;
  listTags: Array<TagsMensagem> = new Array<TagsMensagem>();

  listPatternRegexTags: Array<string> = new Array<string>();

  listTagsInsertsIntoMessage: Array<number> = Array<number>();

  unionMensagensTags: UnionMensagensTags = new UnionMensagensTags();

  listBtnRadio = [{id: 0 , name: 'Inativo', value: 'inativo'}, {id: 1, name: 'Ativo', value: 'ativo'}];

  btnRadioModel = this.listBtnRadio[1].value;

  mensagens: ListaMensagem = new ListaMensagem();

  listMensagem = [{ id: 0, name: 'Mensagem', value: 'Mensagem' }, { id: 1, name: 'Blink', value: 'Blink' }, { id: 2, name: 'Bloqueio', value: 'Bloqueio' }];
  mensagemModel = this.listMensagem[0].value;

  constructor(public dialogRef: MatDialogRef<IncluirMensagemComponent>,@Inject(MAT_DIALOG_DATA) public data: notificacaoMensagens ,private loader: LoaderService, private bancoService: BancoService, private dialog: MatDialog) { }
  
  async ngOnInit() {
    this.loader.show();
    await this.comboVarejista();
    await this.listaTagsMensagens();
    if(this.data.id !== 0) {
      this.mensagemModel = this.data.tipo;
      // this.resellersModel = this.data.idlinxresellers;
      this.resellersModel = this.data.idempresa;
      this.mensagens.mensagem = this.data.texto;
      this.btnRadioModel = (Number(this.data.status) === 0) ? 'inativo' : 'ativo';
      console.log(this.btnRadioModel);

    }
    this.loader.hide();
  }

  onNoClick(): void {
    this.dialogRef.close(this.result);
  }

  onChangeRadioValue = () => {
    console.log(this.btnRadioModel);
  }

  comboVarejista = async () => {
    try {
      const response = await this.bancoService.getAllLinxResellers();
      // console.log(response.json());
      this.listResellers = response.json();
      this.resellersModel = this.listResellers[0].id;
    } catch (error) {
      console.log(error);
    }
  }

  listaTagsMensagens = async () => {
    try {
      const response = await this.bancoService.getAllTagsMensagens();
      this.listTags = response.json();
    } catch (error) {
      console.log(error);
    }
  }

  getDate(): string  {
    const today = new Date();
    const date = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();

    return date + ' ' + time;
  }

  adicionarMensagem = async () => {
    if (this.validateMensagem()) {
      try {
        this.loader.show();
        this.mensagens.tipomensagem = this.mensagemModel;
        // this.mensagens.idlinxresellers = Number(this.resellersModel);
        this.mensagens.idempresa = Number(this.resellersModel);
        this.mensagens.DtAdd = this.getDate();
        this.mensagens.Ativo = String(this.listBtnRadio.filter(x => x.value === this.btnRadioModel)[0].id);

        const response = await this.bancoService.postListaMensagens(this.mensagens);
        const id = response.json();
        this.unionMensagensTags.idListaMensagens = id;
        this.fillIdsTagsIntoList();
        await this.listTagsInsertsIntoMessage.forEach(async (part, index, array) => {
          this.unionMensagensTags.idTagsMensagens = part;
          console.log(this.unionMensagensTags.idTagsMensagens)
          this.unionMensagensTags.idempresa = this.mensagens.idempresa;
          const response = await this.bancoService.postUnionMensagensTags(this.unionMensagensTags);
          console.log(response)
        });
        this.loader.hide();
        this.dialogRef.close();
        this.modalMensagem('Mensagem Inserida com sucesso!');

      } catch (error) {
        console.log(error);
      }
    }
  }

  atualizarMensagem = async () => {
    if (this.validateMensagem()) {
      try {
        this.loader.show();
        this.mensagens.id = this.data.id;
        this.mensagens.tipomensagem = this.mensagemModel;
        // this.mensagens.idlinxresellers = Number(this.resellersModel);
        this.mensagens.idempresa = Number(this.resellersModel);
        this.mensagens.DtUpd = this.getDate();
        this.mensagens.Ativo = String(this.listBtnRadio.filter(x => x.value === this.btnRadioModel)[0].id);

        const response = await this.bancoService.putListaMensagens(this.mensagens);
        const id = response.json();
        // this.unionMensagensTags.idListaMensagens = id;
        const resposta = await this.bancoService.getUnionMensagensTags(this.mensagens.id);
        const unionTags = resposta.json();
        const oldTags = unionTags.map((item) => item.idTagsMensagens );
        //  console.log('oldTags: ', oldTags);
        this.fillIdsTagsIntoList();
        // console.log('this CARA antes: ', this.mensagens);
        //  console.log('CARA do FOR: ', this.listTagsInsertsIntoMessage);
        const tagsFinais = this.listTagsInsertsIntoMessage.filter((id) => !oldTags.includes(id));
        //  console.log('CARA do FOR DEPOIS: ', tagsFinais);
        await tagsFinais.forEach(async (part, index, array) => {
          this.unionMensagensTags.idListaMensagens = this.mensagens.id;
          this.unionMensagensTags.idTagsMensagens = part;
          this.unionMensagensTags.idempresa = this.mensagens.idempresa;
          //  console.log('unionMensagensTags: ', this.unionMensagensTags);
          const response = await this.bancoService.postUnionMensagensTags(this.unionMensagensTags);
          console.log(response)
        });
        this.result = true;
        this.loader.hide();
        this.dialogRef.close(this.result);
        this.modalMensagem('Mensagem Atualizada com sucesso!');
      } catch (error) {
        console.log(error);
      }
    }
  }

  validateMensagem = (): boolean => {
    if (this.mensagens.mensagem.length === 0) {
      this.modalMensagem('Campo Texto e obrigatorio.');
      return false;
    }
    return true;
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: { mensagem: mensagem }
    });
  }

  fillIdsTagsIntoList = () => {
    this.listTags.forEach((part, index, value) => {
      const pattern = RegExp(part.descricaoTags, "g");
      var manyTags = this.mensagens.mensagem.match(pattern);
      (manyTags !== undefined && manyTags !== null && manyTags.length > 0) ? manyTags.forEach(() => this.listTagsInsertsIntoMessage.push(part.id)) : null;
    });
  }

  insertTagText = (tag: TagsMensagem) => {
    const valorMaximo = (this.mensagens.mensagem.length + tag.descricaoTags) + 2;
    if (Number(valorMaximo) > 220) {
      this.modalMensagem('Texto passa do limite com a adição dessa tag.');
    } else {
      this.mensagens.mensagem += '[' + tag.descricaoTags + ']';
    }
  }

}
