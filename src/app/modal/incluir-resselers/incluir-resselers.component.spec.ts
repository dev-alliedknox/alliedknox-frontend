import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirResselersComponent } from './incluir-resselers.component';

describe('IncluirResselersComponent', () => {
  let component: IncluirResselersComponent;
  let fixture: ComponentFixture<IncluirResselersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirResselersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirResselersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
