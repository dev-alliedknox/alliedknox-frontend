import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { LoaderService } from 'src/app/loading/loader.service';
import { cadastroEmails } from 'src/class/cadastroEmails';
import { LinxResellers } from 'src/class/linxResellers';
import { MensagemComponent } from 'src/app/modal/mensagem/mensagem.component';
import { BancoService } from 'src/services/banco.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-incluir-resselers',
  templateUrl: './incluir-resselers.component.html',
  styleUrls: ['./incluir-resselers.component.css']
})
export class IncluirResselersComponent implements OnInit {

  listBtnRadio = [{id: 0 , name: 'Inativo', value: 'inativo'}, {id: 1, name: 'Ativo', value: 'ativo'}];
  btnRadioModel = this.listBtnRadio[1].value;

  public linxResellers: LinxResellers = new LinxResellers();
  pattern = /^(?:\()[0-9]{2}(?:\))\s?[0-9]{4,5}(?:-)[0-9]{4}$/;
  patternNumber = /\d+/g;
  public flag: boolean
  constructor(private dialog: MatDialog, private bancoService: BancoService, public dialogRef: MatDialogRef<IncluirResselersComponent>, @Inject(MAT_DIALOG_DATA) public data: any ,private loader: LoaderService) { }

  ngOnInit() {
    this.flag = this.data.editing;
    this.btnRadioModel = (this.data.linxResellers.ativo == '0') ? 'inativo' : 'ativo';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  editarResellers = async (linxResellers: LinxResellers) => {
    if (this.validateFields()) {
      try {
        this.loader.show();
        this.btnRadioModel === 'ativo' ? linxResellers.ativo = '1' : linxResellers.ativo = '0';
        await this.bancoService.putResellers(linxResellers);
        this.modalMensagem('Varejista Atualizado com Sucesso!');
        this.loader.hide();
        this.onNoClick();
        } catch (error) {
          this.loader.hide();
          this.modalMensagem('Erro ao tentar editar o varejista , por favor tente novamente.');
        }
    }
  }

  cadastrarResellers = async (linxResellers: LinxResellers) => {
    if (this.validateFields()) {
      try {
        this.loader.show();
        this.btnRadioModel === 'ativo' ? linxResellers.ativo = '1' : linxResellers.ativo = '0';
        await this.bancoService.postResellers(linxResellers);
        this.modalMensagem('Varejista criado com sucesso!');
        this.loader.hide();
        this.onNoClick();
      } catch (error) {
        this.loader.hide();
        this.modalMensagem('Erro ao tentar criar um novo varejista , por favor tente novamente.');
      }
    }
  }

  // onlyNumberTel = () => {
  //   if (String(this.linxResellers.telefone) !== "") {
  //     var newValue = String(this.linxResellers.telefone).match(this.pattern)[0];
  //     this.linxResellers.telefone = String(newValue);
  //   } else {
  //     this.linxResellers.telefone = '(';
  //   }
  // }

  onlyNumberCNPJ = () => {
    if (String(this.linxResellers.cnpj) !== "") {
      var newValue = (String(this.linxResellers.cnpj).match(this.patternNumber) !== null) ? String(this.linxResellers.cnpj).match(this.patternNumber)[0] : '';
      this.linxResellers.cnpj = String(newValue);
    } else {
      this.linxResellers.cnpj = '0';
    }
  }


  validateFields = (): boolean => {
    const param = this.data.linxResellers;

    // for (const key in param) {
    //   if (param.hasOwnProperty(key)) {
    //     if (!param.key) {
    //       this.modalMensagem(`Campo ${key} é obrigatório!`);
    //       return false;
    //     }
    //     return true;
    //   }
    // }

    if (!param.nome) {
      this.modalMensagem('Campo Nome é obrigatorio!');
      return false;
    }
    if (!param.email ) {
      this.modalMensagem('Campo Email é obrigatorio!');
      return false;
    }
    if (!param.razao_social ) {
      this.modalMensagem('Campo Razão Social é obrigatorio!');
      return false;
    }
    if (!param.cnpj) {
      this.modalMensagem('Campo CNPJ é obrigatorio!');
      return false;
    }
    if (!param.nome_fantasia) {
      this.modalMensagem('Campo Loja é obrigatorio!');
      return false;
    }
    if (!param.telefone)  {
      this.modalMensagem('Campo Telefone é obrigatorio!');
      return false;
    }
    if (!param.idempresa || param.idempresa === '' || param.idempresa === 0) {
      this.modalMensagem('Campo Id Empresa é obrigatorio!');
      return false;
    }
    return true;
  }

   modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }


}
