import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { IncluirUsuarioComponent } from '../../modal/incluir-usuario/incluir-usuario.component';
import { User } from 'src/class/user';
import { AuthorizationService } from 'src/services/authorization.service';
import { EscolhaComponent } from '../escolha/escolha.component';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';

@Component({
  selector: 'app-usuarios-modal',
  templateUrl: './usuarios-modal.component.html',
  styleUrls: ['./usuarios-modal.component.css']
})
export class UsuariosModalComponent implements OnInit {

  public listUser: Array<User> = new Array<User>();
  public listFindUser: Array<User> = new Array<User>();
  public listGroupUser: Array<any> = new Array<any>();

  constructor(public dialog: MatDialog,
              public authorizationService: AuthorizationService,
              private loader: LoaderService,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(UsuariosModalComponent, {
      width: '750px',
      height: '500px'
    });
  }

  async ngOnInit() {
    await this.listUsersByGrupo();
  }

  incluirUsuario = () => {
    const dialogRef = this.dialog.open(IncluirUsuarioComponent, {
      width: '800px',
      height: '500px',
    });
  }

  Cancelar(): void {
    this.dialog.closeAll();
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  listUsers = async () => {
    try {
      this.listUser = await this.authorizationService.listUsers();
      this.listFindUser = this.listUser;
    } catch (error) {
      console.log(error);
    }
  }

  listUsersByGrupo = async () => {
    try {
      this.listGroupUser = await this.authorizationService.listUsersByGrupo(this.data.groupId);
      console.log('listGroupUser: '); console.log(this.listGroupUser);
    } catch (error) {
      console.log(error);
    }
  }


  desassociar = async (idUser: number) => {
    const userId = idUser;
    const groupId = this.data.groupId;
    console.log('DATA: '); console.log(this.data);
    console.log('userID: '); console.log(userId);
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: {escolha: false, mensagem: 'Deseja realmente desassociar esse usuário?'}
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
            this.loader.show();
            const deleteUser = await this.authorizationService.disassociateUser(userId, groupId);
            await this.listUsers();
            this.modalMensagem('Usuário desassociado com Sucesso');
            await this.listUsersByGrupo();
            this.loader.hide();
            return deleteUser;
        } catch (err) {
          console.log(err);
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar desassociar usuário.');
        }
      }
    });
  }

}
