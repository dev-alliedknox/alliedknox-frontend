import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { cadastroEmails } from '../../../class/cadastroEmails';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-editaremail',
  templateUrl: './editaremail.component.html',
  styleUrls: ['./editaremail.component.css']
})
export class EditaremailComponent implements OnInit {

  cadastro: cadastroEmails = new cadastroEmails();
  varejistaOption: [{}];
  varejistaEscolhido = 0;

  constructor( private cookieService: CookieService,
    public dialogRef: MatDialogRef<EditaremailComponent>, private service: BancoService, private loader: LoaderService, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async ngOnInit() {
    // const IDEMPRESA = [];
    // IDEMPRESA[0] = this.cookieService.get('idempresa');
    // let IdEmpresa = IDEMPRESA[0].split(',').map(Number);
    await this.getAllLinxResellers();
    this.cadastro.id = this.data.id;
    this.cadastro.nome = this.data.nome;
    this.cadastro.email = this.data.email;
    this.varejistaEscolhido = this.data.idempresa;
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  getAllLinxResellers = async () => {
    const result = await this.service.getAllLinxResellers();
    // console.log('varejistas: ', result);
    const varejistas = JSON.parse(result._body);
    // console.log('varejistas2: ', varejistas);
    this.varejistaOption = varejistas.map((item) => 
        { return {ID: item.idempresa, Nome: item.nome_fantasia }; });
    //console.log('Option: ', this.varejistaOption);
  }


  editarEmail = async () => {
    // console.log('varejistaEscolhido: ', this.varejistaEscolhido);
    this.cadastro.idempresa = Number(this.varejistaEscolhido);
    // console.log('cadastro: ', this.cadastro); return;
    if (this.validateFields()) {
      try {
        this.loader.show();
        var response = await this.service.putCadastroEmail(this.cadastro);
        var responseCadastro = response.json();
        this.loader.hide();
        this.modalMensagem('Email atualizado com sucesso!');
        this.onNoClick();
        (responseCadastro.affectedRows > 0) ? this.dialogRef.close() : null ;
      } catch (error) {
        // console.log(error);
        const msg = JSON.parse(error._body);
        this.modalMensagem('Erro ao atualizar email: ' + msg.message);
        this.loader.hide();
      }
    }
  }

  validateFields = (): boolean => {
    if (this.cadastro.nome === '') {
      this.modalMensagem('Campo nome é obrigatório.');
      return false;
    }

    if (this.cadastro.email === '') {
      this.modalMensagem('Campo email é obrigatório');
      return false;
    }

    if (this.cadastro.idempresa === 0) {
      this.modalMensagem('Campo idEmpresa é obrigatório');
      return false;
    }

    return true;
  }

}
