import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaremailComponent } from './editaremail.component';

describe('EditaremailComponent', () => {
  let component: EditaremailComponent;
  let fixture: ComponentFixture<EditaremailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditaremailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaremailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
