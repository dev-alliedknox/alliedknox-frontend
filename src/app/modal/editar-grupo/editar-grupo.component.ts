import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import {AuthorizationService} from '../../../services/authorization.service';
import { Group } from 'src/class/group';
import { User } from 'src/class/user';
import { filterDateBloqueados } from 'src/class/lista-bloqueados';
import { EscolhaComponent } from '../escolha/escolha.component';

@Component({
  selector: 'app-editar-grupo',
  templateUrl: './editar-grupo.component.html',
  styleUrls: ['./editar-grupo.component.css']
})
export class EditarGrupoComponent implements OnInit {
  public name: string;
  public description: string;
  public newGrupo: Group = new Group();
  public listUser: Array<any> = new Array<any>();
  public listFilterUser: Array<User> = new Array<User>();
  public groupUserList: Array<number> = new Array<number>();
  public listGroupUser: Array<any> = new Array<any>();
  // public associado: boolean;

  constructor(
            public authorizationService: AuthorizationService,
            private service: BancoService,
            private loader: LoaderService,
            private dialog: MatDialog,
            @Inject(MAT_DIALOG_DATA) public data: any) {}

  async ngOnInit() {
    // this.listFilterUser = [];
    await this.getThisGroup();
    await this.listUsersByGrupo();
    await this.listUsers();
   }

  Cancelar() {
    this.dialog.closeAll();
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }


  //updateGrupo = async (groupUserList: number[]) => {
  updateGrupo = async ( ) => {
    try {
      // console.log('updateGrupo');
      if (!this.newGrupo.name || !this.newGrupo.description ) {
        this.modalMensagem('Nome do grupo ou descrição estão em branco');
        return;
      }
      this.loader.show();
      const grupoNew = await this.authorizationService.updateGrupo(this.newGrupo, this.data.groupId);
      //  console.log('grupoNew: '); console.log(grupoNew);
      // const grupoId = JSON.parse(grupoNew._body).groupId;
      //  console.log('grupoId: '); console.log(grupoId);

      const payload = this.getAssociados(this.data.groupId);

      this.authorizationService.associateUser(payload);

      await this.listUsersByGrupo();
      await this.listUsers();

      this.modalMensagem('Grupo atualizado com sucesso!');
      this.loader.hide();
      return grupoNew;
    } catch (error) {
      console.log(error);
      this.loader.hide();
      this.modalMensagem('Ocorreu um erro ao tentar atualizar grupo.');
    }
    this.dialog.closeAll();
    this.newGrupo.name = '';
    this.newGrupo.description = '';
    // this.listFilterUser = [];
  }

  // modalMensagem = mensagem => {
  //   const dialogRef = this.dialog.open(MensagemComponent,{
  //     width: '400px',
  //     height: '200px',
  //     data: {mensagem: mensagem}
  //   });
  // }

  listUsers = async () => {
    try {
      this.listFilterUser = [];
      // console.log('listFilterUserAntes'); console.log(this.listFilterUser);
      this.listUser = await this.authorizationService.listUsers();
      // console.log('listUser: '); console.log(this.listUser);
      const listUserName = this.listUser.map(user => user.name);
      const listGroupUserName = this.listGroupUser.map(user => user.name);

      const nomesFiltrados = listUserName.filter((name) => !listGroupUserName.includes(name));
      this.listFilterUser = this.listUser.filter(user => nomesFiltrados.includes(user.name));
      // console.log('listFilterUser'); console.log(this.listFilterUser);
    } catch (error) {
      console.log(error);
    }
  }

  getThisGroup = async () => {
    try {
      this.newGrupo = await this.authorizationService.getGroupById(this.data.groupId);
      // console.log('newGrupo: '); console.log(this.newGrupo);
    } catch (error) {
      console.log(error);
    }
  }

  getChecked() {
    return this.listUser.filter((user) => user.checked);
  }

  getAssociados(groupId: number) {
    let associados = this.getChecked();
    return associados.map((item) => {return { idUser: item.id, idGroup: groupId };
    });
  }

  listUsersByGrupo = async () => {
    try {
      this.listGroupUser = await this.authorizationService.listUsersByGrupo(this.data.groupId);
      // console.log('listGroupUser: '); console.log(this.listGroupUser);
    } catch (error) {
      console.log(error);
    }
  }

  desassociar = async (idUser: number) => {
    const userId = idUser;
    const groupId = this.data.groupId;
    // console.log('DATA: '); console.log(this.data);
    // console.log('userID: '); console.log(userId);
    const dialogRef = this.dialog.open(EscolhaComponent, {
      width: '300px',
      data: {escolha: false, mensagem: 'Deseja realmente desassociar esse usuário?'}
    });

    dialogRef.afterClosed().subscribe(async (result: any) => {
      if (result.escolha) {
        try {
            this.loader.show();
            const deleteUser = await this.authorizationService.disassociateUser(userId, groupId);
            await this.listUsers();
            this.modalMensagem('Usuário desassociado com Sucesso');
            await this.listUsersByGrupo();
            await this.listUsers();
            this.loader.hide();
            return deleteUser;
        } catch (err) {
          console.log(err);
          this.loader.hide();
          this.modalMensagem('Ocorreu um erro ao tentar desassociar usuário.');
        }
      }
    });
  }


}
