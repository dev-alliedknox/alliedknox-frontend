import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { User } from 'src/class/user';
import {AuthorizationService} from '../../../services/authorization.service'
import { EscolhaService } from '../escolha/escolha.service';

@Component({
  selector: 'app-incluir-usuario',
  templateUrl: './incluir-usuario.component.html',
  styleUrls: ['./incluir-usuario.component.css']
})
export class IncluirUsuarioComponent implements OnInit {

  // cadastro: cadastroUsuarios = new cadastroUsuarios();
  public name: string
  public email: string
  public senha: string
  public flag: boolean
  public statusModel = [{ label: 'Ativo', value: 1 }, {  label: 'Inativo', value: 0 }];
  public resellers: Array<any> = new Array<any>();
  public resellersAssoc: Array<any> = new Array<any>();

  constructor(
    public authorizationService: AuthorizationService,
    private service: BancoService,
    private loader: LoaderService,
    private escolhaService: EscolhaService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  // onNoClick(): void {
  //   this.dialogRef.close();
  // }

  async ngOnInit() {

    this.flag = this.data.editing;
    this.data.user.isActive = this.flag ? this.data.user.isActive : this.statusModel[0].value;

    await this.listResellers();
    if (this.data.user.id) await this.listResellersNotAssociates();
  }

  fechar = () => {
    this.dialog.closeAll();
  }

  atualizarUser = async (user: User, id: number) => {
    try{
      this.loader.show();
      await this.authorizationService.updateUser(id, user, this.getResellers());
      this.fechar();
      this.modalMensagem('Usuário atualizado com sucesso');
    }catch(err){
      this.modalMensagem('Ocorreu um erro durante a atualização de usuário');
    }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  getResellers = (): number[]=> {

    return this.resellers.filter((reseller) => reseller.selected)
      .map((reseller) => reseller.idempresa);
  }

  createUser = async () => {
    try {
      if (!this.data.user.email || !this.data.user.name || !this.data.user.password) {
        this.modalMensagem('Usuário/senha ou email estão em branco');
        return;
      }

      this.loader.show();
      await this.authorizationService.createUser(this.data.user, this.getResellers());

      this.fechar();
      this.modalMensagem('Usuário incluido com sucesso');
    } catch (error) {
      this.modalMensagem('Ocorreu um erro durante a criação de usuário!');
    }
    this.loader.hide();
  }

  listResellers = async () => {

    this.resellers = await this.authorizationService.listResellers();
  }

  listResellersNotAssociates = async () => {

    this.resellersAssoc = await this.authorizationService.listResellersByUserId(this.data.user.id);

    const listUsersAssoc = this.resellersAssoc.map(reseller => reseller.nomeFantasia);
    const allResellers = this.resellers.map(reseller => reseller.nomeFantasia);
    const nomesFiltrados = allResellers.filter((nomeFantasia) => !listUsersAssoc.includes(nomeFantasia));

    const listValid = this.resellers.filter(reseller => nomesFiltrados.includes(reseller.nomeFantasia));
    console.log(listValid);
    this.resellers = listValid;
   }

   async disassociateResellers(idReseller: number) {

    const dialogRef = this.escolhaService.openDialogMensagem();

    dialogRef.afterClosed().subscribe(async (result: any) => {

      if (result.escolha) {

        this.loader.show();
        const userResellers = [{ idUser: this.data.user.id, idReseller: idReseller }];
        await this.authorizationService.disassociateResellers(userResellers);
        await this.listResellers();
        await this.listResellersNotAssociates();

        this.loader.hide();
        this.modalMensagem('Varejista desassociado com Sucesso');
      }
    });
  }
}
