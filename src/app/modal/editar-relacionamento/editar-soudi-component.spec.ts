import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSoudiComponent } from './editar-soudi-component';

describe('EditarSoudiComponent', () => {
  let component: EditarSoudiComponent;
  let fixture: ComponentFixture<EditarSoudiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarSoudiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarSoudiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
