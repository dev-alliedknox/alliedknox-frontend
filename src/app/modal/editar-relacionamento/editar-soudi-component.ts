import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { RelacionamentoSoudi } from '../../../class/relacionamento-soudi';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';

@Component({
  selector: 'app-editar-soudi',
  templateUrl: './editar-soudi-component.html',
  styleUrls: ['./editar-soudi-component.css']
})
export class EditarSoudiComponent implements OnInit {

  soudi: RelacionamentoSoudi = new RelacionamentoSoudi();
  emailOption = '';
  telefoneOption = '';

  constructor( public dialogRef: MatDialogRef<EditarSoudiComponent>, private service: BancoService, private loader: LoaderService, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async ngOnInit() {
    this.soudi.id = this.data.id;
    this.soudi.email_soudi = this.data.email_soudi;
    this.soudi.telefone_soudi = this.data.telefone_soudi;
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }


  editarSoudi = async () => {
    // console.log('SOUDI: ', this.soudi);
    if (this.validateFields()) {
      try {
        this.loader.show();
        await this.service.putRelacionamentoSoudi(this.soudi);
        this.loader.hide();
        this.modalMensagem('Relacionamento atualizado com sucesso!');
        this.onNoClick();
      } catch (error) {
        // console.log(error);
        const msg = JSON.parse(error._body);
        this.modalMensagem('Erro ao atualizar relacionamento: ' + msg.message);
        this.loader.hide();
      }
    }
  }

  validateFields = (): boolean => {
    if (this.soudi.email_soudi === '') {
      this.modalMensagem('Campo email é obrigatório.');
      return false;
    }

    if (this.soudi.telefone_soudi === '') {
      this.modalMensagem('Campo telefone é obrigatório');
      return false;
    }

    return true;
  }

}
