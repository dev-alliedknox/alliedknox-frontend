import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncluirPerfilComponent } from './incluir-perfil.component';

describe('IncluirPerfilComponent', () => {
  let component: IncluirPerfilComponent;
  let fixture: ComponentFixture<IncluirPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncluirPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncluirPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
