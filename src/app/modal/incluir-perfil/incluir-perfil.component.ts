import { Component, OnInit, Inject } from '@angular/core';
import { AuthorizationService } from 'src/services/authorization.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { LoaderService } from 'src/app/loading/loader.service';
import { treatError } from 'src/utils/error-utils';
import { EscolhaService } from '../escolha/escolha.service';

@Component({
  selector: 'app-incluir-perfil',
  templateUrl: './incluir-perfil.component.html',
  styleUrls: ['./incluir-perfil.component.css']
})
export class IncluirPerfilComponent implements OnInit {

  public profile: any = {};
  public editing: boolean;
  public permissions: Array<any> = new Array<any>();
  public groups: Array<any> = new Array<any>();
  public permissionsAssoc: Array<any> = new Array<any>();
  public groupsAssoc: Array<any> = new Array<any>();

  constructor(public authorizationService: AuthorizationService,  private dialog: MatDialog,
    private loader: LoaderService,
    private escolhaService: EscolhaService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  async ngOnInit() {
    this.profile = this.data.profile;
    this.editing = this.data.editing;
    if (!this.editing) this.listGroups();
    await Promise.all([ this.listGroupsAssociates(), this.listGroupsNotAssociates(), this.listPermissions(), this.listPermissionsAssociados() ]);
  }

  modalMensagem(mensagem) {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  clearFields() {

    this.profile = {};
  }

  closeDialog() {

    this.loader.hide();
    this.dialog.closeAll();
    this.clearFields();
  }

  getPermissions() {

    return this.permissions.filter((permission) => permission.canRead || permission.canWrite);
  }

  getPermissionAssociations(idProfile: number) {

    return this.getPermissions().map((permission) => {

      permission.idPermission = permission.id;
      permission.idProfile = idProfile;
      delete permission.id;
      delete permission.resource;

      return permission;
    });
  }

  getGroups() {

    return this.groups.filter((group) => group.selected);
  }

  getGroupAssociations(idProfile: number) {

    return this.getGroups().map((group) => {

      return {

        idGroup: group.id,
        idProfile: idProfile
      };
    });
  }

  async createProfile() {

    try {

      this.loader.show();
      const idProfile = await this.authorizationService.createProfile(this.profile)
      const permissionAssociations = this.getPermissionAssociations(idProfile);
      const permissionGroups = this.getGroupAssociations(idProfile);

      await this.authorizationService.associatePermissions(permissionAssociations);
      await this.authorizationService.associateGroups(permissionGroups);

      this.closeDialog();
      this.modalMensagem('Perfil incluído com sucesso!');
    } catch (err) {
      treatError(err, this.dialog, this.loader)
    }
  }

  async updateProfile(profile) {
    try{
      this.loader.show();
      await this.authorizationService.updateProfile(profile.id, { name: profile.name })
        .then((resp) => { this.closeDialog(); this.modalMensagem('Perfil atualizado com sucesso!') })
        .catch((err) => treatError(err, this.dialog, this.loader));
        const permissionAssociations = this.getPermissionAssociations(profile.id);
        await this.authorizationService.associatePermissions(permissionAssociations);
        const permissionGroups = this.getGroupAssociations(profile.id);
        await this.authorizationService.associateGroups(permissionGroups);
    }catch(err){
      this.modalMensagem('Ocorreu um erro durante a atualização de perfil!');
    }
  }

  async listGroups () {
    this.groups = await this.authorizationService.listGroups();
  }

  async listPermissions() {
    this.permissions = await this.authorizationService.listPermissions();
    const permissionsAssoc = await this.authorizationService.listPermissionsByProfile(this.profile.id);
    this.permissions.map((item) => {

      permissionsAssoc.forEach((item2) => {

        if (item.resource === item2.resource) {

          item.canRead = item2.canRead;
          item.canWrite = item2.canWrite;
        }
      });
    });
  }

  async listGroupsAssociates() {
    this.groupsAssoc = await this.authorizationService.listGroupsByProfile(this.profile.id);
  }

  async listPermissionsAssociados() {
    this.permissionsAssoc = await this.authorizationService.listPermissionsByProfile(this.profile.id);
  }

  async associatePermission(permissionAssociations) {

    return await this.authorizationService.associatePermissions(permissionAssociations);
  }


  async listGroupsNotAssociates() {
   this.groupsAssoc = await this.authorizationService.listGroupsByProfile(this.profile.id);
   this.groups = await this.authorizationService.listGroups();
   const listGroupsAssoc = this.groupsAssoc.map(group => group.name);
   const allGroups = this.groups.map(group => group.name)
   const nomesFiltrados = allGroups.filter((name) => !listGroupsAssoc.includes(name));
   const listValid = this.groups.filter(group => {
     return nomesFiltrados.includes(group.name);
   });

   this.groups = listValid;
  }

  async disassociateGroups(idGroup: number) {

    const dialogRef = this.escolhaService.openDialogMensagem();

    dialogRef.afterClosed().subscribe(async (result: any) => {

      if (result.escolha) {

        this.loader.show();
        const profileGroups = [{ idGroup: idGroup, idProfile: this.profile.id }];

        await this.authorizationService.disassociateGroups(profileGroups);
        await this.listGroupsNotAssociates();
        this.loader.hide();
        this.modalMensagem('Grupo desassociado com Sucesso');
      }
    });
  }
}
