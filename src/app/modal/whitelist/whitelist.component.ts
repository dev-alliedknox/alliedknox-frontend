import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { serialWhitelist } from '../../../class/serialWhitelist';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { EscolhaComponent } from '../escolha/escolha.component';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { LoaderService } from 'src/app/loading/loader.service';

@Component({
  selector: 'app-whitelist',
  templateUrl: './whitelist.component.html',
  styleUrls: ['./whitelist.component.css']
})
export class WhitelistComponent implements OnInit {
  faTrashAlt = faTrashAlt;
  whitelist: Array<serialWhitelist> = new Array<serialWhitelist>();


  constructor(
    public dialogRef: MatDialogRef<WhitelistComponent>, private service: BancoService, private dialog: MatDialog, private loader: LoaderService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async ngOnInit() {
    this.loader.show();
    await this.fillWhiteList();
    this.loader.hide();
  }

  deleteWhiteList = (white: serialWhitelist) => {
    const id = white.id;
      const dialogRef = this.dialog.open(EscolhaComponent, {
        width: '300px',
        data: {escolha: false}
      });

      dialogRef.afterClosed().subscribe(async (result:any) => {
        if(result.escolha) {
          this.loader.show();
          await this.deleteItem(Number(id));
          await this.fillWhiteList();
          this.loader.hide();
        }
      });

  }

  deleteItem = async (id: Number) => {
    try {
      var response = await this.service.deleteWhiteList(Number(id));
      console.log(response);
      this.modalMensagem('Item deletado com sucesso!');
    } catch (error) {
      console.log(error);
      this.modalMensagem('Ocorreu um erro ao tentar deletar o item.');
    }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent,{
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  fillWhiteList = async () => {
    try {
      var response = await this.service.getAllSerialWhitelist();
      this.whitelist = response.json();
    } catch (error) {
      console.log(error);
    }
  }

}

