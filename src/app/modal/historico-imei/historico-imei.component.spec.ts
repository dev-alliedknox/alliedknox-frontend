import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoImeiComponent } from './historico-imei.component';

describe('HistoricoImeiComponent', () => {
  let component: HistoricoImeiComponent;
  let fixture: ComponentFixture<HistoricoImeiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoImeiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoImeiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
