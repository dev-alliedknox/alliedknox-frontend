import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { cadastroEmails } from '../../../class/cadastroEmails';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { historicoImei, filterDateTimeHistoricoImei } from 'src/class/historicoImei';

export interface DialogData {
  imei: boolean;
}


@Component({
  selector: 'app-historico-imei',
  templateUrl: './historico-imei.component.html',
  styleUrls: ['./historico-imei.component.css']
})

export class HistoricoImeiComponent implements OnInit {
  listaHistoricoImei: Array<historicoImei> = new Array<historicoImei>();
  currentPageHistoricoImei: number = 1;
  dropdownPagesHistoricoImei = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageHistoricoImei = this.dropdownPagesHistoricoImei[0].itemsPerPage;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, public dialogRef: MatDialogRef<HistoricoImeiComponent>, private bancoService: BancoService, private loader: LoaderService, private dialog: MatDialog) { }

  async ngOnInit() {
    this.loader.show();
      try {
        let responseImeiMsg = await this.bancoService.postImeiMensagens({imei: this.data.imei});
        this.listaHistoricoImei = responseImeiMsg.json();
        this.listaHistoricoImei = filterDateTimeHistoricoImei(this.listaHistoricoImei);
        this.loader.hide();
      } catch (error) {
        this.modalMensagem(error);
        this.loader.hide();
      }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '100vh',
      data: { mensagem: mensagem }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
