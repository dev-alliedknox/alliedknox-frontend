import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroRegistroManualComponent } from './cadastro-registro-manual.component';

describe('CadastroRegistroManualComponent', () => {
  let component: CadastroRegistroManualComponent;
  let fixture: ComponentFixture<CadastroRegistroManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroRegistroManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroRegistroManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
