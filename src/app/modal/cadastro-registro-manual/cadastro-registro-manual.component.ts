import { Component, OnInit, Inject } from '@angular/core';
import { RegistroManual } from 'src/class/registro-manual';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { LoaderService } from 'src/app/loading/loader.service';
import { BancoService } from 'src/services/banco.service';
import { MensagemComponent } from '../mensagem/mensagem.component';

@Component({
  selector: 'app-cadastro-registro-manual',
  templateUrl: './cadastro-registro-manual.component.html',
  styleUrls: ['./cadastro-registro-manual.component.css']
})
export class CadastroRegistroManualComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CadastroRegistroManualComponent>,
    private service: BancoService,
    private loader: LoaderService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public registroManual: RegistroManual
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    // edit não permite alterar transação
    let edit: boolean = false;
    this.registroManual.transacao ? edit = true : edit = false;
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  validateFields = () => {
    if (!this.registroManual.cnpj) { throw new Error('O campo CNPJ é obrigatório.'); }
    if (!this.registroManual.razaoSocial) { throw new Error('O campo Razão Social é obrigatório.'); }
    if(!this.registroManual.nomeLoja)  { throw new Error('O campo Nome Loja é obrigatório.'); }
    if(!this.registroManual.idempresa)  { throw new Error('O campo Numero Loja é obrigatório.'); }
    if (!this.registroManual.nota) { throw new Error('O campo Nota é obrigatório.'); }
    if (!this.registroManual.modeloDocumento) { throw new Error('O campo Modelo Documento é obrigatório.'); }
    if (!this.registroManual.cpfCliente) { throw new Error('O campo CPF Cliente é obrigatório.'); }
    if (!this.registroManual.dataEmissao) { throw new Error('O campo Data emissão é obrigatório.'); }
    if (!this.registroManual.valor) { throw new Error('O campo Valor é obrigatório.'); }
    if (!this.registroManual.formaPagamento) { throw new Error('O campo Forma Pagamento é obrigatório.'); }
    if (!this.registroManual.parcelas) { throw new Error('O campo Parcelas é obrigatório.'); }
    if (!this.registroManual.tipoDispositivo) { throw new Error('O campo Tipo Dispositivo é obrigatório.'); }
  }

  salvarRegistroManual = async () => {
    try {
      this.validateFields();
      this.loader.show();
      if (!this.registroManual.transacao) {
        const response = await this.service.postVendaManual([this.registroManual]);
        const resultlist = JSON.parse(response._body).resultlist;
        // if (resultlist.length > 0) { throw new Error('Erro ao tentar cadastrar registro manual, tente novamente.'); }
        if (resultlist.length > 0) { throw new Error(`Erro ao tentar cadastrar registro manual: ${resultlist[0].error.message}`); }
      } else {
        this.registroManual.numeroLoja = this.registroManual.idempresa;
        this.registroManual.numeroVenda = this.registroManual.transacao.toString();
        const response = await this.service.putVendaManual([this.registroManual]);
        const resultlist = JSON.parse(response._body).resultlist;
        // if (resultlist.length > 0) { throw new Error('Erro ao tentar atualizar registro manual, tente novamente.'); }
        if (resultlist.length > 0) { throw new Error(`Erro ao tentar cadastrar registro manual: ${resultlist[0].error.message}`); }
      }
      this.loader.hide();
      this.modalMensagem('Registro cadastrado com sucesso.');
      this.onNoClick();
    } catch (error) {
        let success = true;
        try {
          const msg = JSON.parse(error._body);
          success = false;
          this.modalMensagem(msg.message);
          this.loader.hide();
        } catch (e) {
          // console.log(e);
        }
        if (success) {
          this.modalMensagem(error.message);
          this.loader.hide();
        }

    }
  }

}
