import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  escolha: boolean;
  mensagem?: string;
}

@Component({
  selector: 'app-escolha',
  templateUrl: './escolha.component.html',
  styleUrls: ['./escolha.component.css']
})
export class EscolhaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EscolhaComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    if(!this.data.mensagem) {
      this.data.mensagem = 'Deseja realmente deletar esse item?';
    }
    console.log(data);
   }

  ngOnInit() {
  }

  onNoClick(): void {
    this.data.escolha = false;
    this.dialogRef.close(this.data);
  }

  onYesClick(): void {
    this.data.escolha = true;
    this.dialogRef.close(this.data);
  }

}
