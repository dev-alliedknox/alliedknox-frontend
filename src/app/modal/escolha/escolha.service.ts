import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EscolhaComponent } from 'src/app/modal/escolha/escolha.component';
 
@Injectable({providedIn: 'root'})

export class EscolhaService {

    constructor(public dialog: MatDialog) { }

    openDialogMensagem(): MatDialogRef<EscolhaComponent> {

        const dialogRef = this.dialog.open(EscolhaComponent, {
            width: '300px',
            data: {escolha: false},
            disableClose: true
        });

        return dialogRef;
    }
}
