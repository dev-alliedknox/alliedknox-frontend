import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroemailComponent } from './cadastroemail.component';

describe('CadastroemailComponent', () => {
  let component: CadastroemailComponent;
  let fixture: ComponentFixture<CadastroemailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroemailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
