import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { BancoService } from '../../../services/banco.service';
import { cadastroEmails } from '../../../class/cadastroEmails';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from '../mensagem/mensagem.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-cadastroemail',
  templateUrl: './cadastroemail.component.html',
  styleUrls: ['./cadastroemail.component.css']
})
export class CadastroemailComponent implements OnInit {

  cadastro: cadastroEmails = new cadastroEmails();
  varejistaOption: [{}];
  varejistaEscolhido = 0;

  constructor( private cookieService: CookieService,
    public dialogRef: MatDialogRef<CadastroemailComponent>, private service: BancoService, private loader: LoaderService, private dialog: MatDialog) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async ngOnInit() {
    // const IDEMPRESA = [];
    // IDEMPRESA[0] = this.cookieService.get('idempresa');
    // let IdEmpresa = IDEMPRESA[0].split(',').map(Number);
    await this.getAllLinxResellers();
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }

  getAllLinxResellers = async () => {
    const result = await this.service.getAllLinxResellers();
    // console.log('varejistas: ', result);
    const varejistas = JSON.parse(result._body);
    // console.log('varejistas2: ', varejistas);
    this.varejistaOption = varejistas.map((item) => {
      return {ID: item.idempresa, Nome: item.nome_fantasia };
    });
    // console.log('Option: ', this.varejistaOption);
  }


  cadastrarEmail = async () => {
    // console.log('varejistaEscolhido: ', this.varejistaEscolhido);
    this.cadastro.idempresa = Number(this.varejistaEscolhido);
    // console.log('cadastro: ', this.cadastro);
    if (this.validateFields()) {
      try {
        this.loader.show();
        const response = await this.service.postCadastroEmail(this.cadastro);
        const responseCadastro = response.json();
        this.loader.hide();
        this.modalMensagem('Email registrado com sucesso!');
        this.onNoClick();
        (responseCadastro.affectedRows > 0) ? this.dialogRef.close() : null ;
      } catch (error) {
        this.loader.hide();
        console.log(error);
        this.modalMensagem('Erro ao tentar cadastrar um email, tente novamente.');
      }
    }
  }

  validateFields = (): boolean => {
    if (this.cadastro.nome === '') {
      this.modalMensagem('Campo nome é obrigatório.');
      return false;
    }

    if (this.cadastro.email === '') {
      this.modalMensagem('Campo email é obrigatório');
      return false;
    }

    if (this.cadastro.idempresa === 0) {
      this.modalMensagem('Campo idEmpresa é obrigatório');
      return false;
    }

    return true;
  }

}
