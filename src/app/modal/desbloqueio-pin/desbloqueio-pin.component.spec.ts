import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesbloqueioPinComponent } from './desbloqueio-pin.component';

describe('DesbloqueioPinComponent', () => {
  let component: DesbloqueioPinComponent;
  let fixture: ComponentFixture<DesbloqueioPinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesbloqueioPinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesbloqueioPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
