import { Component, OnInit, Inject } from '@angular/core';
import { ObjectPin } from 'src/class/objectPin';
import { LoaderService } from '../../loading/loader.service';
import { CrmService } from 'src/services/crm.service';
import { BancoService } from '../../../services/banco.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MensagemComponent } from '../../modal/mensagem/mensagem.component';

export interface DialogData {
  dtHota: string;
  nome: string;
  cpf: string;
  imei: string;
}

@Component({
  selector: 'app-desbloqueio-pin',
  templateUrl: './desbloqueio-pin.component.html',
  styleUrls: ['./desbloqueio-pin.component.css']
})
export class DesbloqueioPinComponent implements OnInit {
  

  objectPin: ObjectPin = new ObjectPin();


  telaPin: string = '';
  telaPin2: string = '';
  serial: string = '';
  passKey: string = '';
  flagAndroid: boolean = true;
  androidOption: string = 'android9';
 

  constructor(public dialogRef: MatDialogRef<DesbloqueioPinComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData, private crm: CrmService, private bancoService: BancoService, private dialog: MatDialog, private loader: LoaderService) { }

  

  async ngOnInit() {
    this.serial = this.data.imei;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  checkAndroid = () => {
    this.flagAndroid = true;
    if (this.androidOption === 'android9') {
      console.log('option: ', this.androidOption);
      this.flagAndroid = true;
    }
    if (this.androidOption === 'android8') {
      console.log('option: ', this.androidOption);
      this.flagAndroid = false;
    }
  }



  obterPin = async () => {
    if (this.serial === '') {
      this.modalMensagem('Adicione um IMEI bloqueado para resgatar o PIN.');
    } else {
      this.loader.show();

      this.objectPin.passKey = '';
      this.objectPin.serial = '';
      this.telaPin = '';
      this.telaPin2 = '';

      try {
        // await this.crm.postAccessToken();

        this.objectPin.serial = this.serial;
        this.objectPin.passKey = this.passKey;

        console.log('==objectPin=='); console.log(this.objectPin);

        let response = await this.crm.postGetPin(this.objectPin);
        console.log('response***'); console.log(response);

        let result = response.json();
        console.log('==GetPin-Response== : '); console.log(result);

        this.telaPin  = 'IMEI/SN: ' + result.requestedId;

        this.telaPin2 = 'Chave de desbloqueio: ' + result.pinNumber[0];

        this.loader.hide();
      } catch (error) {
        console.log(error);
        const msg = JSON.parse(error._body);
        this.modalMensagem('Erro ao resgatar chave PIN: ' + msg.message);
        this.loader.hide();
      }
      // this.dialogRef.close();
      this.objectPin.passKey = '';
      this.objectPin.serial = '';
      this.serial = '';
    }
  }

  modalMensagem = mensagem => {
    const dialogRef = this.dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: { mensagem: mensagem }
    });
  }


}
