import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogNotificacoesComponent } from './log-notificacao.component';

describe('LogNotificacoesComponent', () => {
  let component: LogNotificacoesComponent;
  let fixture: ComponentFixture<LogNotificacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogNotificacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogNotificacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
