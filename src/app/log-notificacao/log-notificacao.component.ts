import { Component, OnInit } from '@angular/core';
import { BancoService } from '../../services/banco.service';
import { LoaderService } from '../loading/loader.service';
import { MatDialog } from '@angular/material';
import { MensagemComponent } from '../modal/mensagem/mensagem.component';
import { logNotificacoes, filterDateTimeLogNotificacoes } from 'src/class/logNotificacoes';
import { HistoricoImeiComponent } from '../modal/historico-imei/historico-imei.component';
import * as moment from 'moment';
import * as localization from 'moment/locale/pt';
moment.locale('pt', localization);


@Component({
  selector: 'app-log-notificacao',
  templateUrl: './log-notificacao.component.html',
  styleUrls: ['./log-notificacao.component.css']
})
export class LogNotificacoesComponent implements OnInit {

  minDate = moment();

  //Tabela Notificacoes
  listaLogNotificacoes: Array<logNotificacoes> = new Array<logNotificacoes>();
  listaLogNotificacoesTodos: Array<logNotificacoes> = new Array<logNotificacoes>();
  flagOrderByDtHrLogNotificacoes: boolean = false;
  currentPageListaLogNotificacoes: number = 1;
  dropdownPagesListaLogNotificacoes = [{itemsPerPage: 10}, {itemsPerPage: 25}, {itemsPerPage: 50}];
  currentItemsPerPageListaLogNotificacoes = this.dropdownPagesListaLogNotificacoes[0].itemsPerPage;
  searchIMEI: string = '';

  comboBoxLogNotificacoesTipo = [{id: 0, name: 'Tipo'},
                                 {id: 1, name: 'Mensagem'},
                                 {id: 2, name: 'Blink'},
                                 {id: 3, name: 'Bloqueio'},
                                 {id: 4, name: 'Desbloqueio'}];
  comboBoxLogNotificacaoTipoModel = this.comboBoxLogNotificacoesTipo[0];

  comboBoxLogNotificacoesStatus = [{id: 0, name: 'Status KG'},
                                   {id: 1, name: 'Pending'},
                                   {id: 2, name: 'Rejected'},
                                   {id: 3, name: 'Accepted'},
                                   {id: 4, name: 'Enrolled'},
                                   {id: 5, name: 'Exchanging'},
                                   {id: 6, name: 'Resetting'},
                                   {id: 7, name: 'Overdue'},
                                   {id: 8, name: 'StartingReminder'},
                                   {id: 9, name: 'StoppingReminder'},
                                   {id: 10, name: 'Blinked'},
                                   {id: 11, name: 'Locking'},
                                   {id: 12, name: 'Locked'},
                                   {id: 13, name: 'Unlocking'},
                                   {id: 14, name: 'Completing'},
                                   {id: 15, name: 'Completed'},
                                   {id: 16, name: 'Desassociado'}];
  comboBoxLogNotificacaoStatusModel = this.comboBoxLogNotificacoesStatus[0];

  comboBoxStatusExecucao = [{id: 0, name: 'Status Execução'},
                            {id: 1, name: 'Sucesso'},
                            {id: 2, name: 'Falha'}];
  comboStatusExecucaoModel = this.comboBoxStatusExecucao[0];


  selected = {startDate: moment(), endDate: moment()};

 constructor(private bancoService: BancoService, private dialog: MatDialog , private loader: LoaderService) { }

 async ngOnInit() {
  this.loader.show();
  await this.fillLogNotificacoes();
  await this.fillStatusExecucao();
  this.loader.hide();
 }

 fillLogNotificacoes = async () => {
  try {
    const responseLogNotificacoes = await this.bancoService.getLogNotificacoes();
    this.listaLogNotificacoes = responseLogNotificacoes.json();
    // this.listaLogNotificacoesTodos = responseLogNotificacoes.json();
    this.listaLogNotificacoesTodos = this.listaLogNotificacoes;
    this.listaLogNotificacoes = filterDateTimeLogNotificacoes(this.listaLogNotificacoes);
    this.listaLogNotificacoesTodos = filterDateTimeLogNotificacoes(this.listaLogNotificacoesTodos);
  } catch (error) {
    console.log(error);
  }
}


fillStatusExecucao = async () => {
  this.listaLogNotificacoes.forEach(element => {

    const response = JSON.parse(element.response);
    if (response) {
      if (response.fail === 0) {
        element.statusExecucao = 'Sucesso';
      } else {
          if (response.fail === response.count) {
            element.statusExecucao = 'Falha';
          } else {
              response.resultList.filter((item: any) => {
                return item.requestedId === element.codigoImei ?  element.statusExecucao = 'Falha' : element.statusExecucao = 'Sucesso';
              });
          }
      }
    }
  });
}

 orderByDtHLogNotificacoes = () => {
   if (this.flagOrderByDtHrLogNotificacoes) {
     this.flagOrderByDtHrLogNotificacoes = false;
     this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
       if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
         return 1;
       }
       if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
         return -1;
       }
       return 0;
     });
   } else {
     this.flagOrderByDtHrLogNotificacoes = true;
     this.listaLogNotificacoes = this.listaLogNotificacoes.sort((a, b) => {
       if (a.timeSpan.getTime() > b.timeSpan.getTime()) {
         return 1;
       }
       if (a.timeSpan.getTime() < b.timeSpan.getTime()) {
         return -1;
       }
       return 0;
     });
   }
 }

 filterByInputIMEI = () => {
  if (this.searchIMEI === '') {
    this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
  } else {
    const filterImei = this.listaLogNotificacoesTodos.filter((item:logNotificacoes) =>{
      return item.codigoImei.includes(this.searchIMEI)
    })
    this.listaLogNotificacoes = filterImei
  }
}

// filterBycomboBoxLogNotificacoesTipo = () => {
//   if (this.comboBoxLogNotificacaoTipoModel.id === 0) {
//     this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
//   } else {
//     this.listaLogNotificacoes = (this.listaLogNotificacoesTodos.filter( x => x.tipo.match(new RegExp(this.comboBoxLogNotificacaoTipoModel.name + '$')) !== null).length > 0) ? this.listaLogNotificacoesTodos.filter( x => x.tipo.match(new RegExp(this.comboBoxLogNotificacaoTipoModel.name + '$')) !== null) : new Array<logNotificacoes>();
//   }
// }

// filterByStatus  = () => {
//   if(this.comboStatusExecucaoModel.id === 0){
//     this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
//   }else{
//     const filterStatus = this.listaLogNotificacoesTodos.filter((item:logNotificacoes) =>{
//       if(item.statusExecucao){
//         return item.statusExecucao.includes(this.comboStatusExecucaoModel.name)
//       }
//     })
//     this.listaLogNotificacoes = filterStatus;
//   }
// }

filterAll  = () => {
  if ((this.comboBoxLogNotificacaoTipoModel.id === 0) &&
      (this.comboStatusExecucaoModel.id === 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id === 0)) {
    this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id !== 0) &&
      (this.comboStatusExecucaoModel.id === 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id === 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.tipo.includes(this.comboBoxLogNotificacaoTipoModel.name)) {
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id !== 0) &&
      (this.comboStatusExecucaoModel.id !== 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id === 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.tipo.includes(this.comboBoxLogNotificacaoTipoModel.name) &&
              item.statusExecucao.includes(this.comboStatusExecucaoModel.name)) {
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id !== 0) &&
      (this.comboStatusExecucaoModel.id === 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id !== 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.tipo.includes(this.comboBoxLogNotificacaoTipoModel.name) &&
              item.status.includes(this.comboBoxLogNotificacaoStatusModel.name)) {
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id === 0) &&
      (this.comboStatusExecucaoModel.id !== 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id === 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.statusExecucao.includes(this.comboStatusExecucaoModel.name)) {
                // return item.statusExecucao.includes(this.comboStatusExecucaoModel.name);
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id === 0) &&
      (this.comboStatusExecucaoModel.id !== 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id !== 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.statusExecucao.includes(this.comboStatusExecucaoModel.name) &&
              item.status.includes(this.comboBoxLogNotificacaoStatusModel.name)) {
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id === 0) &&
      (this.comboStatusExecucaoModel.id === 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id !== 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.status.includes(this.comboBoxLogNotificacaoStatusModel.name)) {
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

  if ((this.comboBoxLogNotificacaoTipoModel.id !== 0) &&
      (this.comboStatusExecucaoModel.id !== 0) &&
      (this.comboBoxLogNotificacaoStatusModel.id !== 0)) {
        const filterStatus = this.listaLogNotificacoesTodos.filter((item: logNotificacoes) => {
          if (item.statusExecucao) {
            if (item.tipo.includes(this.comboBoxLogNotificacaoTipoModel.name) &&
              item.statusExecucao.includes(this.comboStatusExecucaoModel.name) &&
              item.status.includes(this.comboBoxLogNotificacaoStatusModel.name)) {
                // return item.statusExecucao.includes(this.comboStatusExecucaoModel.name);
                return item;
            }
          }
        });
        this.listaLogNotificacoes = filterStatus;
  }

}


// filterBycomboBoxLogNotificacoesStatus = () => {
//   if (this.comboBoxLogNotificacaoStatusModel.id === 0) {
//     this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
//   } else {
//     this.listaLogNotificacoes = (this.listaLogNotificacoesTodos.filter( x => x.status.match(new RegExp(this.comboBoxLogNotificacaoStatusModel.name + '$')) !== null).length > 0) ? this.listaLogNotificacoesTodos.filter( x => x.status.match(new RegExp(this.comboBoxLogNotificacaoStatusModel.name + '$')) !== null) : new Array<logNotificacoes>();
//   }
// }

pesquisarHandler = async (prLogNotificacao: logNotificacoes) => {
  const dialogRef = this.dialog.open(HistoricoImeiComponent, {
    width: '1214px',
    height: '750px',
    data: {imei: prLogNotificacao.codigoImei}
  });
}


cleanDate = () => {
  this.selected = {startDate: moment(), endDate: moment()};
  this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
}

filterByDate = event => {
  if (event.startDate && event.endDate) {
  const [yearStart, monthStart, dayStart] = event.startDate.format().split('T')[0].split('-');
  const [yearEnd, monthEnd, dayEnd] = event.endDate.format().split('T')[0].split('-');
  const dateStartPattern = new Date(Number(yearStart), Number(monthStart), Number(dayStart), 23, 60, 60);
  const dateEndPattern = new Date(Number(yearEnd), Number(monthEnd), Number(dayEnd), 23, 60, 60);

  const days = this.listaLogNotificacoesTodos.filter(x => x.timeSpan !== undefined && x.timeSpan !== null).filter(x => x.timeSpan.getTime() >= dateStartPattern.getTime() && x.timeSpan.getTime() < dateEndPattern.getTime());

  this.listaLogNotificacoes = days;
  } else {
    this.listaLogNotificacoes = this.listaLogNotificacoesTodos;
  }
}





}