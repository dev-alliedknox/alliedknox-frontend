import { MatDialog } from '@angular/material';
import { LoaderService } from 'src/app/loading/loader.service';
import { MensagemComponent } from 'src/app/modal/mensagem/mensagem.component';

const modalMensagem = (dialog: MatDialog, mensagem) => {
    const dialogRef = dialog.open(MensagemComponent, {
      width: '400px',
      height: '200px',
      data: {mensagem: mensagem}
    });
  }
export const getErrorBody = (err) => {

    let errorBody;

    try {

        errorBody = JSON.parse(err._body);
    } catch (error) {

        throw new Error('Ocorreu um erro, tente novamente.');
    }

    throw errorBody;
};

export const treatError = (err, dialog: MatDialog, loader: LoaderService) => {

    loader.hide();
    modalMensagem(dialog, err.message);
};