export const environment = {
  production: false,
  access_token: '',
  envName: 'hom',
  API_URL:          'https://5wgm46o2gk.execute-api.us-east-1.amazonaws.com/acceptance/auth-ext',
  API_URL_AUTH_EXT: 'https://5wgm46o2gk.execute-api.us-east-1.amazonaws.com/acceptance/auth-ext',
  API_URL_LOCAL:    'https://quymaoa6f2.execute-api.us-east-1.amazonaws.com/acceptance/backend-portal',
  API_URL_SLM:      'https://ysu65374ee.execute-api.us-east-1.amazonaws.com/acceptance/slm',
  API_URL_CRM:      'https://vzsuly4qxj.execute-api.us-east-1.amazonaws.com/acceptance/crm',
  API_URL_KG:       'https://stnu67hm7i.execute-api.us-east-1.amazonaws.com/acceptance/kg',
  API_URL_KDP:      'https://crpxx99es2.execute-api.us-east-1.amazonaws.com/acceptance/kdp',
  API_URL_AUTHORIZATION: 'https://h74rbm7x13.execute-api.us-east-1.amazonaws.com/acceptance/authorization',
  USER: '',
  PASSWORD: ''
};
